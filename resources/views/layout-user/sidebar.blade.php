@inject('notif', 'App\Booking')
@inject('userid', 'App\UserDetail')
@if (Auth::user() && !empty($userid->where('user_id', Auth::id())->first()))
	<?php $id = $userid->where('user_id', Auth::id())->first()->user_id; ?>
@endif
<!-- Sidebar
================================================== -->
<div class="col-lg-4 col-md-4 margin-top-0">
		
	<div class="dashboard-list-box with-icons margin-top-0">
		<h4>Informasi Akun</h4>
		<ul>
			<li class="current">
				<i class="list-box-icon sl sl-icon-layers"></i> <a href="{{url('/profil')}}">Pemesanan</a>
				@if(!empty($userid->where('user_id', Auth::id())->first()->id))
				<span class="badge badge-danger">
	   				{{ 
	   					count($notif->where('payment_status', '0')
	   					->where('user_detail_id', $id)
	   					->get()) 
	   				}}
   				</span>
   				@endif
			</li>

			<li>
				<i class="list-box-icon sl sl-icon-star"></i> <a href="{{url('/profil/ulasan')}}">Ulasan</a>
			</li>

			<li>
				<i class="list-box-icon sl sl-icon-settings"></i> <a href="{{url('/profil/pengaturan')}}">Pengaturan</a>
			</li>

			<li>
				<i class="list-box-icon sl sl-icon-user"></i> <a href="{{url('/profil/pasien')}}">Data Pasien</a>
			</li>
		</ul>
	</div>

</div>