<div id="footer" class="sticky-footer">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<img class="footer-logo" src="{{asset('images/logo/logo-full.png')}}" alt="Logo Perawatku">
				<div class="clearfix"></div>
				<span>Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.</span>
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<h4>Halaman</h4>
				<ul class="footer-links">
					<li><a href="{{url('/layanan')}}">Layanan</a></li>
					<li><a href="{{url('/perawat')}}">Perawat</a></li>
					<li><a href="{{url('/blog')}}">Blog</a></li>
				</ul>
				<ul class="footer-links">
					<li><a href="{{url('/faq')}}">FAQ</a></li>
					<li><a href="{{url('/kontak')}}">Kontak</a></li>
					<li><a href="{{url('/syarat-ketentuan')}}">Syarat & Ketentuan</a></li>
				</ul>

				<div class="clearfix"></div>
			</div>		

			<div class="col-md-2 col-sm-6 col-xs-12">
				<h4>Hubungi</h4>
				<div class="text-widget">
					<span>Jakarta</span><br>
					<span>0857-2532-6342</span><br>
					<span><a href="mailto:info@perawatku.id">info@perawatku.id</a></span>
				</div>

			</div>

			<div class="col-md-3 col-sm-6 col-xs-12">
				<h4>Subscribe</h4>
				<div class="text-widget">
					<form name="subscribe" method="post" action="{{url('/subscribe-footer')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<span class="clearfix">Dapatkan berbagai informasi terbaru dan menarik dari Perawatku.</span>
						<div class="col-md-8 nopadding margin-top-15 fleft">
							<input type="text" placeholder="Email" name="email" required />
						</div>
						<div class="col-md-4 margin-top-15 fleft">
							<button type="submit" name="submit" class="button">Subscribe!</button>	
						</div>
						<div class="clearfix"></div>
						@if(Session::has('success'))
						   	<div class="alert alert-success">
						    	{{ Session::get('success') }}
						   	</div>
						@endif
					</form>
				</div>
			</div>

		</div>
		
		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>

</div>