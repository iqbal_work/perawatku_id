@inject('notif', 'App\Booking')
@inject('userid', 'App\UserDetail')
@inject('profpic', 'App\UserDetail')
@if (Auth::user() && !empty($userid->where('user_id', Auth::id())->first()))
	<?php $id = $userid->where('user_id', Auth::id())->first()->user_id; ?>
	<?php $foto = $profpic->where('user_id', Auth::id())->first()->foto; ?>
@endif

<header id="header-container" class="@yield('header1') fullwidth">

	<div id="header" @yield('header2')>
		<div class="container">
			
			<div class="left-side">
				
				<!-- Logo -->
				<div id="logo">
					<a href="{{url('/')}}"><img src="{{asset('images/logo/logo-full.png')}}" alt="Logo Perawatku.id"></a>
				</div>

				<!-- Mobile Navigation -->
				<div class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>


				<!-- Main Navigation -->
				<nav id="navigation" class="style-1">
					<ul id="responsive">

						{{--<li><a @yield('menu1') href="{{url('/')}}">Home</a></li>--}}
						<li><a @yield('menu2') href="{{url('/layanan')}}" id="btn-layanan-header">Layanan</a></li>
                        <li><a @yield('menu5') href="https://goo.gl/forms/U4VPoat8ecqF2SoQ2" target="_blank">Gabung Perawatku</a></li>
						<li><a @yield('menu3') href="{{url('/perawat')}}">Perawat</a></li>
						<li><a @yield('menu4') href="{{url('/blog')}}">Artikel</a></li>
						
					</ul>
				</nav>
				<div class="clearfix"></div>
				<!-- Main Navigation / End -->
				
			</div>
			<!-- Left Side Content / End -->


			<!-- Right Side Content / End -->
			<div class="right-side">
				<div class="header-widget">
					@if (!Auth::check())
					<a href="#sign-in-dialog" id="modal-login" class="sign-in popup-with-zoom-anim"><i class="sl sl-icon-login"></i> Daftar/Masuk</a>			    

				    @elseif (Auth::check())
				    	@if (Auth::user()->role == 'user')
							@if (!empty($userid->where('user_id', Auth::id())->first()))
								@if ($notif
								->where('user_detail_id', $userid->where('user_id', Auth::id())->first()->id)
								->where('payment_status', '0')
								->first() != NULL)
								<span class="badge badge-danger">
									<a href="{{url('/profil')}}">
										{{
											count($notif->where('payment_status', '0')
											->where('user_detail_id', $id)
											->get())
										}}
									</a>
								</span>
								@endif
							@endif
				    	
					    <div class="user-menu">
							<div class="user-name"><span><img src="
								@if(empty($foto))
									{{asset('/images/icons/avatar.jpg')}}
								@else
									{{asset('/images/'.$foto)}}
								@endif" alt=""></span></div>
							<ul>
								<li><a href="{{url('/profil')}}"><i class="fa fa-user"></i> Profil</a></li>
								<li><a href="#small-dialog" class="popup-with-zoom-anim"><i class="sl sl-icon-power"></i> Keluar</a></li>
							</ul>
						</div>
						@endif
				    @endif
				    <br>

				</div>
			</div>

			<div id="small-dialog" class="zoom-anim-dialog mfp-hide">
				<div class="small-dialog-header">
					<h3 class="center">Ingin keluar?</h3>
				</div>

				<div class="margin-top-0 divcenter center">
					<a href="{{url('/logout')}}"><button class="button edit">Iya</button></a>
					<a href="."><button class="button delete">Tidak</button></a>
				</div>
				
			</div>
			<!-- Right Side Content / End -->

			<!-- Sign In Popup -->
			<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">

				<div class="small-dialog-header center">
					<h3>Akun Perawatku.id</h3>
				</div>

				<!--Tabs -->
				<div class="sign-in-form style-1">

					<ul class="tabs-nav">
						<li class=""><a href="#tab1">Daftar Akun</a></li>
						<li><a href="#tab2">Masuk Akun</a></li>
					</ul>

					<div class="tabs-container alt">

						<!-- Login -->
						<div class="tab-content" id="tab1" style="display: none;">

							<form method="POST" action="{{url('/register')}}">
								<div class="form-group login-socmed">
			                        <div class="col-md-6">
			                            <button type ="button" class="btn btn-info btn-block btn-facebook" onclick="window.location.href='{{ url('/auth/facebook') }}';"><span class="fa fa-facebook"></span> Facebook</button>
			                        </div>
			                        <div class="col-md-6">
			                            <button  type ="button" class="btn btn-info btn-block btn-google" onclick="window.location.href='{{ url('/auth/google') }}';"><span class="fa fa-google-plus"></span> Google</button>
			                        </div>
			                    </div>

			                    <div class="divider divider-rounded divider-center"><span>atau</span></div>

								@csrf
								
								<p class="form-row form-row-wide">
									<label for="username2">
										<i class="sl sl-icon-user"></i>
										<input type="text" required="required" class="input-text" name="name" id="name" placeholder="Nama Lengkap" value="" />
									</label>
								</p>
									
								<p class="form-row form-row-wide">
									<label for="email2">
										<i class="sl sl-icon-paper-plane "></i>
										<input type="email" class="input-text" required="required" name="email" id="email2" placeholder="Email" value="" />
									</label>
								</p>

								<p class="form-row form-row-wide">
									<label for="hp">
										<i class="sl sl-icon-phone"></i>
										<input type="text" class="input-text" required="required" name="hp" id="hp" placeholder="Nomor HP" value="" />
									</label>
								</p>

								<p class="form-row form-row-wide">
									<label for="password1">
										<i class="sl sl-icon-key"></i>
										<input class="input-text" required="required" type="password" name="password" id="password" placeholder="Password" value="" />
									</label>
								</p>

								<p class="form-row form-row-wide">
									<label for="password2">
										<i class="sl sl-icon-key"></i>
										<input class="input-text" required="required" type="password" name="password_confirmation" id="password_confirmation" placeholder="Ulangi password"/>
									</label>
								</p>

								<div class="form-row margin-top-10 center">
									<input type="submit" class="button border fw" name="register" value="Daftar" />
								</div>
	
							</form>
							
						</div>

						<!-- Register -->
						<div class="tab-content" id="tab2" style="display: none;">

							<form method="POST" action="{{url('/login')}}">
								@csrf

								@include('user.errors')

								<p class="form-row form-row-wide">
									<label for="email2">
										<i class="sl sl-icon-paper-plane "></i>
										<input type="email" class="form-control" required name="email" placeholder="Email" />
									</label>
								</p>

								<p class="form-row form-row-wide">
									<label for="password">
										<i class="sl sl-icon-key"></i>
										<input class="form-control" type="password" required name="password" placeholder="Password" />
									</label>
									<span class="lost_password" style="float:right">
										<a href="#">Lupa password?</a>
									</span>
								</p>

								<div class="form-row margin-top-60 center">
									<input type="submit" class="button border" value="Masuk"/>
								</div>
								
							</form>
						</div>

					</div>
				</div>
			</div>
			<!-- Sign In Popup / End -->

		</div>
	</div>
	<!-- Header / End -->

</header>