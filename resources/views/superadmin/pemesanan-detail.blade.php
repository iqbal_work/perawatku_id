@extends('layout-admin.layout-superadmin')

@section('menu2')
    class="active"
@stop

@section('title')
Detail Pemesanan
@stop
    
@section('content')

<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Detail Pemesanan</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">

				<div id="add-listing">

					<!-- Section -->
					<div class="add-listing-section">

						<!-- Row -->
						<div class="row with-forms">

							<div class="col-md-6">

								<h3 class="margin-bottom-30">Data Pemesan</h3>

								<div class="col-md-12 margin-bottom-10">
									<h5>Nama Lengkap</h5>
									<span>{{$join->userDetail->nama}}</span>
								</div>

								<div class="col-md-12 margin-bottom-10">
									<h5>Alamat</span></h5>
									<span>{{ $join->userDetail->alamat }}</span>
								</div>

								<div class="col-md-12 margin-bottom-10">
									<h5>Nomor Telepon</h5>
									<span>{{ $join->userDetail->nomor_hp }}</span>
								</div>

							</div>

							<div class="col-md-6">

								@if($join->pasien->tipe_pasien == 3)
								<h3 class="margin-bottom-30">Data Pasien Bayi</h3>

								<div class="col-md-6 margin-bottom-10">
									<h5>Nama Bayi</h5>
									<span>{{ $join->pasien->nama_pasien }}</span>
								</div>

								<div class="col-md-6 margin-bottom-10">
									<h5>Alamat Bayi</span></h5>
									<span>{{ $join->pasien->alamat_pasien }}</span>
								</div>

								<div class="col-md-6 margin-bottom-10">
									<h5>Berat Badan</h5>
									<span>{{ $join->pasien->berat_badan }} kg</span>
								</div>

								<div class="col-md-6 margin-bottom-10">
									<h5>Tinggi Badan</h5>
									<span>{{ $join->pasien->tinggi_badan }} cm</span>
								</div>

								<div class="col-md-6 margin-bottom-10">
									<h5>Umur</h5>
									<span>{{ $join->pasien->umur }} Tahun</span>
								</div>

								<div class="col-md-6 margin-bottom-10">
									<h5>Suku</h5>
									<span>{{ $join->pasien->suku }}</span>
								</div>

								<div class="col-md-6 margin-bottom-10">
									<h5>Agama</h5>
									<span>{{ $join->pasien->agama }}</span>
								</div>

								<div class="col-md-6 margin-bottom-10">
									<h5>Kondisi Medis</h5>
									<span>{{ $join->pasien->kondisi_medis }}</span>
								</div>
								@else
								<h3 class="margin-bottom-30">Data Pasien Manula/OrangLain</h3>

								<div class="col-md-6 margin-bottom-10">
									<h5>Nama Lengkap</h5>
									<span>{{ $join->pasien->nama_pasien }}</span>
								</div>

								<div class="col-md-6 margin-bottom-10">
									<h5>Alamat Pasien</span></h5>
									<span>{{ $join->pasien->alamat_pasien }}</span>
								</div>

								<div class="col-md-6 margin-bottom-10">
									<h5>Email</h5>
									<span>{{ $join->pasien->email_pasien }}</span>
								</div>

								<div class="col-md-6 margin-bottom-10">
									<h5>Nomor Telepon</h5>
									<span>{{ $join->pasien->nomor_hp }} </span>
								</div>
								@endif

								

								
							</div>

						</div>

						<div class="row with-forms margin-top-30">

							<div class="col-md-12">

								<h3 class="margin-bottom-30">Kebutuhan Pasien</h3>

								<div class="col-md-2 margin-bottom-10">
									<h5>Hari Kunjungan</h5>
									<span>{{ $join->tanggal_order->format('d-m-Y') }}</span>
								</div>

								<div class="col-md-7">
									<h5>Layanan</h5>
									<span>{{ $join->fee->layanan }}</span>
								</div>

								<div class="col-md-3">
									<h5>Total Biaya</h5>
									<span>Rp {{ number_format($join->fee->biaya, '0') }} </span>
								</div>

							</div>
						</div>

					</div>

					<div class="add-listing-section margin-top-30">

						<!-- Headline -->
						<div class="add-listing-headline">
							<h3>Data Perawat</h3>
						</div>

						<!-- Row -->
						<div class="row with-forms">

							<div class="col-md-4">
								<div class="edit-profile-photo">
									<img src="@if(!file_exists(public_path().('/images/'.$join->perawat->foto))) {{asset('/images/icons/avatar.jpg')}} @else {{asset('/images/'.$join->perawat->foto)}} @endif" alt="">
								</div>
							</div>

							<div class="col-md-8">

								<div class="col-md-6">

									<div class="margin-bottom-10">
										<h5>Nama Lengkap</h5>
										<span> {{ $join->perawat->nama }} </span>
									</div>

									<div class="margin-bottom-10">
										<h5>Alamat</span></h5>
										<span> {{ $join->perawat->alamat }} </span>
									</div>

								</div>

								<div class="col-md-6">

									<div>
										<h5>Nomor Telepon</h5>
										<span> {{ $join->perawat->telepon }} </span>
									</div>

									<div>
										<h5>Email</h5>
										<span> {{ $join->perawat->email }} </span>
									</div>

								</div>

							</div>

						</div>
						<!-- Row / End -->

					</div>
					<!-- Section / End -->
					 @if($join->booking_status == '0') 
						<div class="form-row center margin-top-30">
							<a href="{{url('/superadmin/pemesanan/tolak/'.$join->id)}}" class="button gray delete"><i class="sl sl-icon-close"></i> Tolak</a>
							<a href="{{url('/superadmin/pemesanan/terima/'.$join->id)}}" class="button edit"><i class="sl sl-icon-check"></i> Terima</a>
						</div>
					@elseif($join->booking_status == '1')
						<div class="form-row center margin-top-30">
							 @if($join->payment_status == '2')
							<a href="{{url('/superadmin/pembayaran/terima/'.$join->id)}}" class="button edit"></i> Konfirmasi Terima Pembayaran</a>
							 @endif
							<a href="{{url('/superadmin/pemesanan/')}}" class="button gray"></i> Kembali</a>
						</div>
					@else 
						<div class="form-row center margin-top-30">
							<a href="{{url('/superadmin/pemesanan/')}}" class="button gray"></i> Kembali</a>
						</div>
					@endif
					

				</div>
			</div>

			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop