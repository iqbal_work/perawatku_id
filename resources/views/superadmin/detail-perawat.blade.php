@extends('layout-admin.layout-superadmin')

@section('menu3')
    class="active"
@stop

@section('title')
Detail Perawat
@stop
    
@section('content')

@inject('fee', 'App\Fee')
<?php 
	$biaya = $fee->where('perawat_id', $perawat->id)->get();
?>
<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Detail Perawat</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">

				<div id="add-listing">

					<!-- Section -->
					<div class="add-listing-section">

						<!-- Headline -->
						<div class="add-listing-headline">
							<h3>Detail Data Perawat</h3>
						</div>

						<!-- Row -->
						<div class="row with-forms">

							<!-- Avatar -->
							<div class="col-md-12">
								<div class="edit-profile-photo">
									<img src="@if(!file_exists(public_path().('/images/'.$perawat->foto))) {{asset('/images/icons/avatar.jpg')}} @else {{asset('/images/'.$perawat->foto)}} @endif" alt="">
								</div>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Nama Lengkap</h5>
								<span>{{ $perawat->nama }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Alamat</h5>
								<span>{{ $perawat->alamat }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Domisili</h5>
								<span>{{ $perawat->domisili }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Jenis Kelamin</h5>
								<span>{{ $perawat->jenis_kelamin }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Agama</h5>
								<span>{{ $perawat->agama }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Umur</h5>
								<span>{{ $perawat->umur }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Nomor Telepon</h5>
								<span>{{ $perawat->telepon }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Email</h5>
								<span>{{ $perawat->email }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Pendidikan</h5>
								<span>{{ $perawat->pendidikan }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Institusi</h5>
								<span>{{ $perawat->institusi }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Sertifikasi yang Dimiliki</h5>
								<span>{{ $perawat->sertifikasi }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Mulai Aktif</h5>
								<span>{{ $perawat->mulai_aktif }}</span>
							</div>

							<div class="col-md-12 margin-bottom-10">
								<h5>Pengalaman</h5>
								<span>{{ $perawat->pengalaman }}</span>
							</div>

							<div class="col-md-12 margin-bottom-10">
								<h5>Deskripsi Perawat</h5>
								<span>{{ $perawat->deskripsi }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Jenis Perawatan</h5>
								<span>{{ $perawat->jenis_perawatan }}</span>
							</div>

							<div class="col-md-6 margin-bottom-10">
								<h5>Bersedia tinggal dengan pasien</h5>
								<span>{{ $perawat->bersedia }}</span>
							</div>

						</div>

					</div>

					<div class="add-listing-section margin-top-30">

						<!-- Headline -->
						<div class="add-listing-headline">
							<h4>Detail Fee Perawat</h4>
						</div>

						<!-- Row -->
						<div class="row with-forms">

							<div class="row">
								<div class="col-md-12">
									<form action="{{url('/superadmin/perawat/tambah-fee/')}}" method="POST" enctype="multipart/form-data">
										<!-- @if(!empty($biaya[0]))
										 	@method('PATCH')
										@endif -->
										@csrf
										<input type="hidden" name="perawat_id" value="{{$perawat->id}}">
										<table id="pricing-list-container">
											<tr class="pricing-list-item pattern">
												@if(empty($biaya[0]))
													<td>
														<div class="fm-input pricing-ingredients"><input type="text" name="layanan[]" value="Paket - Homecare 8 Jam Kerja Per Hari"  readonly /></div>
														<div class="fm-input pricing-price">
															<input type="text" data-unit="RP" name="biaya[]" placeholder="0" value="0"/>
														</div>
													</td>
													<td>
														<div class="fm-input pricing-ingredients"><input type="text" name="layanan[]" value="Paket - Homecare 12 Jam Kerja Per Hari" readonly /></div>
														<div class="fm-input pricing-price">
															<input type="text" data-unit="RP" name="biaya[]" placeholder="0" value="0"/>
														</div>
													</td>
													<td>
														<div class="fm-input pricing-ingredients"><input type="text" name="layanan[]" value="Paket - Homecare Standby 24 Jam Kerja" readonly /></div>
														<div class="fm-input pricing-price">
															<input type="text" data-unit="RP" name="biaya[]" value="0"/>
														</div>
													</td>
													<td>
														<div class="fm-input pricing-ingredients"><input type="text" name="layanan[]" value="Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi" readonly /></div>
														<div class="fm-input pricing-price">
															<input type="text" data-unit="RP" name="biaya[]" value="0"/>
														</div>
													</td>
													<td>
														<div class="fm-input pricing-ingredients"><input type="text" name="layanan[]" value="Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi" readonly /></div>
														<div class="fm-input pricing-price">
															<input type="text" data-unit="RP" name="biaya[]" value="0" />
														</div>
													</td>
													<td>
														<div class="fm-input pricing-ingredients"><input type="text" name="layanan[]" value="Perawatan Pasien Lansia" readonly /></div>
														<div class="fm-input pricing-price">
															<input type="text" data-unit="RP" name="biaya[]" value="0" />
														</div>
													</td>
												@else
													@foreach($biaya as $fee)
														<td>
															<input type="hidden" name="id_fee[]" value="{{$fee->id}}">
															<div class="fm-input pricing-ingredients"><input type="text" name="layanan[]" value="{{$fee->layanan}}" readonly /></div>
															<div class="fm-input pricing-price">
																<input type="text" data-unit="RP" name="biaya[]" value="{{ $fee->biaya }}" />
															</div>
														</td>
													@endforeach
												@endif

												
											</tr>
										</table>
										<!-- <a href="#" class="button add-pricing-list-item">Tambah Fee</a> -->
										<div class="form-row center">
											<button type="submit" class="button margin-top-15">Simpan Data</button>
										</div>
									</form>
								</div>
							</div>

						</div>

					</div>
					<!-- Row / End -->

					<div class="form-row center">
						<a href="{{url('/superadmin/perawat/edit-perawat/'.$perawat->id )}}" class="button preview">Edit Perawat <i class="fa fa-arrow-circle-right"></i></a>
					</div>

				</div>
			</div>

			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop