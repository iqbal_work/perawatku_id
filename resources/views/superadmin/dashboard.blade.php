@extends('layout-admin.layout-superadmin')

@section('menu1')
    class="active"
@stop

@section('title')
Dashboard
@stop
    
@section('content')

<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<!-- Titlebar -->
		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Super Admin</h2>
				</div>
			</div>
		</div>

		<!-- Notice -->
		<!-- <div class="row">
			<div class="col-md-12">
				<div class="notification success closeable margin-bottom-30">
					<p>Your listing <strong>Hotel Govendor</strong> has been approved!</p>
					<a class="close" href="#"></a>
				</div>
			</div>
		</div> -->

		<!-- Content -->
		<div class="row">

			<!-- Item -->
			<div class="col-lg-4 col-md-6">
				<div class="dashboard-stat color-2">
					<div class="dashboard-stat-content"><h4>{{ $perawat }}</h4> <span>Total Perawat</span></div>
					<div class="dashboard-stat-icon"><i class="im im-icon-Line-Chart"></i></div>
				</div>
			</div>

			<!-- Item -->
			<div class="col-lg-4 col-md-6">
				<div class="dashboard-stat color-3">
					<div class="dashboard-stat-content"><h4>{{ $review }}</h4> <span>Total Ulasan</span></div>
					<div class="dashboard-stat-icon"><i class="im im-icon-Add-UserStar"></i></div>
				</div>
			</div>

			<!-- Item -->
			<div class="col-lg-4 col-md-6">
				<div class="dashboard-stat color-4">
					<div class="dashboard-stat-content"><h4>{{$booking}}</h4> <span>Total Pemesanan</span></div>
					<div class="dashboard-stat-icon"><i class="im im-icon-Heart"></i></div>
				</div>
			</div>
		</div>

		<div class="row">
			
			<!-- Recent Activity -->
			<div class="col-lg-6 col-md-12">
				<div class="dashboard-list-box with-icons margin-top-20">
					<h4>Aktivitas Terakhir</h4>
					<ul>
						@foreach($activity as $log)
							<li>
								<i class="list-box-icon sl sl-icon-star"></i>{!! $log->activity !!}
							</li>
						@endforeach
					</ul>
				</div>
			</div>
			
			<!-- Invoices -->
			<div class="col-lg-6 col-md-12">
				<div class="dashboard-list-box invoices with-icons margin-top-20">
					<h4>Bukti Pembayaran</h4>
					<ul>

						@foreach($join as $joins)
							<li><i class="list-box-icon sl sl-icon-doc"></i>
								@include('superadmin.booking-list-dashboard')
							</li>
						@endforeach
				
						<!-- 						
						<li><i class="list-box-icon sl sl-icon-doc"></i>
							<strong>Benarivo Putra</strong>
							<ul>
								<li class="paid">Lunas</li>
								<li>Kode: #00003</li>
								<li>Tanggal: 19/04/2018</li>
							</ul>
							<div class="buttons-to-right">
								<a href="{{url('/pemesanan/bukti-pembayaran')}}" class="button gray">Lihat Bukti</a>
							</div>
						</li> -->

					</ul>
				</div>
			</div>

			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop