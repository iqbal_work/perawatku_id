@switch($rumahsakits->membership_status)
    @case(0)
        <?php 
        	$status = 'Free Account'
        ?>
        @break
    @case(1)
        <?php 
        	$status = 'Waiting for Approval'
        ?>
        @break
    @default
        <?php 
        	$status = 'Premium Account'
        ?>
@endswitch

<div class="list-box-listing bookings">
	<div class="list-box-listing-img"><img src="{{asset($rumahsakits->foto)}}" alt=""></div>
	<div class="list-box-listing-content">
		<div class="inner">
			<h3>{{$rumahsakits->nama}}
				<span class="booking-status">{{$status}}</span>
			</h3>

			<div class="inner-booking-list">
				<h5>Kontak:</h5>
				<ul class="booking-list">
					<li>{{$rumahsakits->penanggungjawab}}</li>
					<li>{{$rumahsakits->telepon}}</li>
					<li>{{$rumahsakits->email}}</li>
				</ul>
			</div>

		</div>
	</div>
</div>
@if($rumahsakits->membership_status >= 1)
	<div class="buttons-to-right">
		<a href="{{url('superadmin/rumah-sakit/tolak/'.$rumahsakits->id)}}" class="button delete"><i class="sl sl-icon-close"></i> Tolak</a>
		@if($rumahsakits->membership_status == 1)
		<a href="{{url('superadmin/rumah-sakit/terima/'.$rumahsakits->id)}}" class="button detail"><i class="sl sl-icon-check"></i> Terima</a>	
		@endif
	</div>
@endif
