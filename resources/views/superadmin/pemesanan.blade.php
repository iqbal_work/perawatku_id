@extends('layout-admin.layout-superadmin')

@section('menu2')
    class="active"
@stop

@section('title')
Pemesanan
@stop
    
@section('content')

<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<!-- Titlebar -->
		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Pemesanan Perawat</h2>
				</div>
			</div>
		</div>

		<!-- Content -->
		<div class="row">
			
			<div class="col-lg-12 col-md-12">

				<form action="{{url('superadmin/pemesanan/cari')}} " method="GET">
				<div class="row with-forms margin-bottom-10">
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pull-right" id="search-code">
						<div class="col-md-9">
						@csrf
						<input type="text" name="search" placeholder="Kode transaksi atau nama" value=""/>
						</div>
						<button type="submit" class="button">Cari</button>
					</div>
				</div>
				</form>

				<div class="dashboard-list-box margin-top-0">

					<!-- Sort by -->
					<div class="sort-by">
						<div class="sort-by-select">
							<select data-placeholder="Booking Status" class="chosen-select-no-single" onChange="window.location.href=this.value">
								<option disabled selected></option>
								<option value="{{url('/superadmin/pemesanan/')}}" @if($status == 0) selected @endif >Semua Status</option>	
								<option value="{{url('/superadmin/pemesanan/diterima')}}" @if($status == 1) selected @endif>Booking Diterima</option>
								<option value="{{url('/superadmin/pemesanan/ditolak/')}}" @if($status == 2) selected @endif >Booking Ditolak</option>
								<option value="{{url('/superadmin/pemesanan/belum-diproses/')}}" @if($status == 3) selected @endif>Belum Diproses</option>
							</select>
						</div>
					</div>

					<h4>Daftar Pemesanan Perawat</h4>
					<ul>
						@foreach ($join as $joins)
							@if ($joins->booking_status == '1')
								<li class="approved-booking">
									@include ('superadmin.list-pesanan')
								</li>
							@elseif ($joins->booking_status == '2')
								<li class="canceled-booking">
									@include ('superadmin.list-pesanan')
								</li>
							@else
								<li class="pending-booking">
									@include ('superadmin.list-pesanan')
								</li>
							@endif			        		
				        @endforeach
					</ul>
				</div>

				<div class="clearfix"></div>
				<div class="pagination-container margin-top-30 margin-bottom-0">
					<nav class="pagination">
						{{$join->links()}}
					</nav>
				</div>
			</div>


			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop