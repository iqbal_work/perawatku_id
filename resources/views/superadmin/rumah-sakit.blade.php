@extends('layout-admin.layout-superadmin')

@section('menu7')
    class="active"
@stop

@section('title')
Rumah Sakit Terdaftar
@stop
    
@section('content')


<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<!-- Titlebar -->
		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Rumah Sakit Terdaftar</h2>
				</div>
			</div>
		</div>

		<!-- Content -->
		<div class="row">
			
			<!-- Listings -->
			<div class="col-lg-12 col-md-12">
				<div class="dashboard-list-box margin-top-0">

					<!-- Sort by -->
					<div class="sort-by">
						<div class="sort-by-select">
							<select data-placeholder="Account Status" class="chosen-select-no-single" onChange="window.location.href=this.value">
								<option disabled selected></option>
								<option value="{{url('/superadmin/rumah-sakit/')}}" @if($status == 0) selected @endif >Semua Status</option>	
								<option value="{{url('/superadmin/rumah-sakit/free-account')}}" @if($status == 1) selected @endif >Free Account</option>
								<option value="{{url('/superadmin/rumah-sakit/premium-account')}}" @if($status == 2) selected @endif >Premium Account</option>
								<option value="{{url('/superadmin/rumah-sakit/mengajukan-premium')}}" @if($status == 3) selected @endif>Mengajukan Premium</option>
							</select>
						</div>
					</div>

					<h4>Daftar Rumah Sakit</h4>
					<ul>
						@foreach($rumahsakit as $rumahsakits)
							@if($rumahsakits->membership_status < 2)
							<li class="pending-booking">
								@include('superadmin.list-rumah-sakit')
							</li>
							@else
							<li class="approved-booking">
								@include('superadmin.list-rumah-sakit')
							</li>
							@endif
						@endforeach		
					</ul>
				</div>

				<div class="clearfix"></div>
				<div class="pagination-container margin-top-30 margin-bottom-0">
					<nav class="pagination">
						{{$rumahsakit->links()}}	
					</nav>
				</div>
			</div>


			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop