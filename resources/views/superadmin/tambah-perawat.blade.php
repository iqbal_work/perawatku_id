@extends('layout-admin.layout-superadmin')

@section('menu5')
    class="active"
@stop

@section('title')
Tambah Perawat
@stop
    
@section('content')

<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Tambah Perawat</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">

				<div id="add-listing">

					<!-- Section -->
					<div class="add-listing-section">

						<!-- Headline -->
						<div class="add-listing-headline">
							<h4>Tambah Data Perawat</h4>
						</div>

						<!-- Row -->
						<div class="row with-forms">
							<form method="POST" action="{{url('/superadmin/perawat/tambah-perawat')}}" enctype="multipart/form-data">
								@csrf
								<div class="col-md-12">
									<div class="edit-profile-photo">
										<img src="{{asset('images/icons/avatar.jpg')}}" alt="">
										<div class="change-photo-btn">
											<div class="photoUpload">
											    <span><i class="fa fa-upload"></i> Upload Foto Perawat</span>
											    <input type="file" class="upload" name="foto" required />
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<h5>Nama Lengkap</h5>
									<input type="text" name="nama" required>
								</div>

								<div class="col-md-6">
									<h5>Alamat</h5>
									<input type="text" name="alamat" required>
								</div>

								<div class="col-md-6">
									<h5>Domisili</h5>
									<input type="text" name="domisili" required>
								</div>

								<div class="col-md-6">
									<h5>Jenis Kelamin</h5>
									<select name="jenis_kelamin" class="chosen-select" required="required">
										<option selected="true">Jenis Kelamin</option>
										<option value="Laki Laki">Laki Laki</option>
										<option value="Perempuan">Perempuan</option>
									</select>
								</div>

								<div class="col-md-6">
									<h5>Agama</h5>
									<select name="agama" class="chosen-select" required="required">
										<option selected="true">Agama</option>
										<option value="Islam">Islam</option>
										<option value="Katolik">Katolik</option>
										<option value="Protestan">Protestan</option>
										<option value="Budha">Budha</option>
										<option value="Hindu">Hindu</option>
										<option value="Lainnya">Lainnya</option>
									</select>
								</div>

								<div class="col-md-6">
									<h5>Umur</h5>
									<input type="text" name="umur" required>
								</div>

								<div class="col-md-6">
									<h5>Nomor Telepon</h5>
									<input type="text" name="telepon" required>
								</div>

								<div class="col-md-6">
									<h5>Email</h5>
									<input type="text" name="email" required>
								</div>

								<div class="col-md-6">
									<h5>Pendidikan</h5>
									<select name="pendidikan" class="chosen-select" required="required">
										<option selected="true">Tingkat Pendidikan</option>
										<option value="D4">D4</option>
										<option value="D3">D3</option>
										<option value="S1">S1</option>
										<option value="SMK">SMK</option>
										<option value="Lainnya">Lainnya</option>
									</select>
								</div>

								<div class="col-md-6">
									<h5>Institusi</h5>
									<input type="text" name="institusi" required>
								</div>

								<div class="col-md-6">
									<h5>Sertifikasi yang Dimiliki</h5>
									<input type="text" name="sertifikasi" required>
								</div>

								<div class="col-md-6">
									<h5>Mulai Aktif</h5>
									<select name="mulai_aktif" class="chosen-select" required="required">
										<option value="">Klasifikasi Perawat</option>
										<option value="1">Belum ada pengalaman</option>
										<option value="2">Pengalaman kurang dari 5 tahun</option>
										<option value="3">Pengalaman 5 - 10 tahun</option>
										<option value="4">Pengalaman lebih dari 10 tahun</option>
									</select>
								</div>

								<div class="col-md-6">
									<h5>Pengalaman menjadi Perawat</h5>
									<textarea name="pengalaman" id="notes" cols="30" rows="3" required></textarea>
								</div>

								<div class="col-md-6">
									<h5>Deskripsi/Keterangan Tambahan Perawat</h5>
									<textarea name="deskripsi" id="notes" cols="30" rows="3" required></textarea>
								</div>

								<div class="col-md-6">
									<h5>Jenis Perawatan</h5>
									<select name="jenis_perawatan" class="chosen-select" required="required">
										<option value="">Jenis Perawatan</option>
										<option value="1">Visit Perawat</option>
										<option value="2">Visit Bidan</option>
										<option value="3">Visit Fisioterapi</option>
									</select>
								</div>

								<div class="col-md-6">
									<h5>Bersedia tinggal dengan pasien?</h5>
									<select name="bersedia" class="chosen-select" required="required">
										<option value="">Bersedia?</option>
										<option value="1">Iya</option>
										<option value="2">Tidak</option>
									</select>
								</div>

								<div class="form-row center">
									<button type="submit" class="button margin-top-15">Simpan Data</button>
								</div>
							</form>
							<!-- Avatar -->
							

						</div>

					</div>

					

				</div>
			</div>

			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop