@inject('fee', 'App\Fee')
<?php 
	$biaya = $fee::where('perawat_id', $perawat->id)->first();
?>
@switch($perawat->pengalaman)
    @case(1)
        <?php $pengalaman = "Belum ada pengalaman";?>
        @break
    @case(2)
        <?php $pengalaman = "Pengalaman kurang dari 5 tahun";?>
        @break
    @case(3)
    	<?php $pengalaman = "Pengalaman 5 - 10 tahun"; ?>
    	@break
    @default
    	<?php $pengalaman = "Pengalaman lebih dari 10 tahun"?>
@endswitch

<div class="list-box-listing">
	<div class="list-box-listing-img">
		<img src="@if(!file_exists(public_path().('/images/'.$perawat->foto))) {{asset('/images/icons/avatar.jpg')}} @else {{asset('/images/'.$perawat->foto)}} @endif" alt="">
	</div>
	<div class="list-box-listing-content">
		<div class="inner">
			<h3>{{ $perawat['nama'] }} @if(empty($biaya))<span class="badge badge-danger" title="Biaya belum diisi"><span class="fa fa-exclamation"></span></span>@endif</h3>
			<span>Nomor Telepon: {{$perawat->telepon}} </span><br>
			<span>Email: {{$perawat->email}} </span><br>
			<span>Mulai Aktif: {{$perawat->mulai_aktif}}</span>
		</div>
	</div>
</div>
<div class="buttons-to-right">
	<a href="{{url('/superadmin/perawat/detail-perawat/'.$perawat->id)}}" class="button detail"><i class="sl sl-icon-book-open"></i> Detail</a>
	<a href="{{url('/superadmin/perawat/edit-perawat/'.$perawat->id)}}" class="button edit"><i class="sl sl-icon-note"></i> Edit</a>
	<a href="#confirm-dialog" class="popup-with-zoom-anim button delete"><i class="sl sl-icon-close"></i> Hapus</a>
</div>

<div id="confirm-dialog" class="zoom-anim-dialog mfp-hide">
	<div class="small-dialog-header">
		<h3 class="center">Ingin hapus?</h3>
	</div>
	<div class="margin-top-0 divcenter center">
		<a href="{{url('/superadmin/perawat/delete-perawat/'.$perawat->id)}}"><button class="button edit">Iya</button></a>
		<a href=""><button class="button delete">Tidak</button></a>
	</div>
</div>