@extends('layout-admin.layout-superadmin')

@section('menu6')
    class="active"
@stop

@section('title')
Blog Perawatku
@stop
    
@section('content')

<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<div class="row">
			
			<!-- Listings -->
			<div class="col-lg-12 col-md-12">
				<div class="dashboard-list-box margin-top-0">

					<div class="sort-by">
						<div class="sort-by-select">
							<a href="{{url('/superadmin/blog/add')}}" class="button edit"><i class="sl sl-icon-plus"></i> Tambah Blog</a>
						</div>
					</div>

					<h4>Daftar Blog</h4>

					<ul>
						@foreach($blog as $blogs)
						<li>
				        	<div id="blogAdmin" class="list-box-listing">
								<div class="list-box-listing-img"><img src="{{asset('/images/'.$blogs->gambar)}}" alt=""></div>
								<div class="list-box-listing-content">
									<div class="inner">
										<h3>{{ $blogs->judul }}</h3>
										<span>{!! str_limit($blogs->konten,250) !!}</span>
									</div>
								</div>
							</div>
							<div class="buttons-to-right">
								<a href="{{url('/superadmin/blog/edit/'.$blogs->id)}}" class="button detail"><i class="sl sl-icon-note"></i> Edit</a>
								<a href="#confirm-dialog" class="popup-with-zoom-anim button delete"><i class="sl sl-icon-close"></i> Hapus</a>
							</div>
						</li>

						<div id="confirm-dialog" class="zoom-anim-dialog mfp-hide">
							<div class="small-dialog-header">
								<h3 class="center">Ingin hapus?</h3>
							</div>
							<div class="margin-top-0 divcenter center">
								<a href="{{url('/superadmin/blog/delete/'.$blogs->id)}}"><button class="button edit">Iya</button></a>
								<a href="#"><button class="button delete">Tidak</button></a>
							</div>
						</div>
						@endforeach
					</ul>
				</div>

				<!-- Pagination -->
				<div class="clearfix"></div>
				<div class="pagination-container margin-top-30 margin-bottom-0">
					<nav class="pagination">
						{{$blog->links()}}
					</nav>
				</div>
				<!-- Pagination / End -->
			</div>


			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop