@extends('layout-admin.layout-superadmin')

@section('menu8')
    class="active"
@stop

@section('title')
Ubah Password
@stop
    
@section('content')

<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Ubah Password</h2>
				</div>
			</div>
		</div>

		<div class="row">

			<!-- Change Password -->
			<div class="col-lg-12 col-md-12">
				<div class="dashboard-list-box margin-top-0">
					<h4 class="gray">Ubah Password</h4>
					<div class="dashboard-list-box-static">

						<!-- Change Password -->
						<form action="{{url('/superadmin/ubah-password')}}" method="POST">
							@csrf
							<div class="my-profile">
								<label class="margin-top-0">Password Lama</label>
								<input type="password" name="old-pass" required>

								<label>Password Baru</label>
								<input type="password" name="password" required>

								<label>Konfirmasi Password Baru</label>
								<input type="password" name="password_confirmation" required>

								<div class="form-row center">
									<button type="submit" class="button margin-top-15">Ubah Password</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop