@extends('layout-admin.layout-superadmin')

@section('menu3')
    class="active"
@stop

@section('title')
Daftar Perawat
@stop
    
@section('content')

<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Manajemen Perawat</h2>
				</div>
			</div>
		</div>

		<div class="row">
			
			<!-- Listings -->
			<div class="col-lg-12 col-md-12">
				<div class="dashboard-list-box margin-top-0">
					<h4>Daftar Perawat</h4>

					<ul>
						@foreach ($perawats as $perawat)
							<li>
				        		@include ('superadmin.list-perawat')
							</li>
				        @endforeach
					</ul>
				</div>

				<!-- Pagination -->
				<div class="clearfix"></div>
				<div class="pagination-container margin-top-30 margin-bottom-0">
					<nav class="pagination">
						{{$perawats->links()}}
					</nav>
				</div>
				<!-- Pagination / End -->
			</div>


			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop