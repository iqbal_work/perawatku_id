@extends('layout-admin.layout-superadmin')

@section('menu4')
    class="active"
@stop

@section('title')
Ulasan Pengguna
@stop
    
@section('content')

<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Ulasan Pengguna</h2>
				</div>
			</div>
		</div>

		<div class="row">
			
			<!-- Listings -->
			<div class="col-lg-12 col-md-12">

				<div class="dashboard-list-box margin-top-0">

					<!-- Sort by -->
					<!--<div class="sort-by">
						<div class="sort-by-select">
							<select data-placeholder="Default order" class="chosen-select-no-single">
								<option>All Listings</option>	
								<option>Tom's Restaurant</option>
								<option>Sticky Band</option>
								<option>Hotel Govendor</option>
								<option>Burger House</option>
								<option>Airport</option>
								<option>Think Coffee</option>
							</select>
						</div>
					</div>-->

					<h4>Ulasan Pengguna</h4> 

					<ul>
						@foreach ($review as $reviews)
							<li>
								<div class="comments listing-reviews">
									<ul>
										<li>
											<div class="avatar"><img src="{{asset('images/profile/review-avatar.jpg')}}" alt="" /></div>
											<div class="comment-content"><div class="arrow-comment"></div>
												<div class="comment-by">{{ $reviews->nama_user }}<div class="comment-by-listing">untuk <a href="{{url('/superadmin/perawat/detail-perawat')}}">{{ $reviews->nama_perawat }}</a></div> <span class="date">{{ $reviews->tanggal_order }}</span>
													<div class="star-rating" data-rating="{{ $reviews->rating }}"></div>
												</div>
												<p>{{ $reviews->review }}</p>
											</div>
										</li>
									</ul>
								</div>
							</li>
						@endforeach

					</ul>
				</div>

				<!-- Pagination -->
				<div class="clearfix"></div>
				<div class="pagination-container margin-top-30 margin-bottom-0">
					<nav class="pagination">
						{{$review->links()}}
					</nav>
				</div>
				<!-- Pagination / End -->

			</div>

			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop