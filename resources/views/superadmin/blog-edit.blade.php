@extends('layout-admin.layout-superadmin')

@section('menu6')
    class="active"
@stop

@section('title')
Blog Perawatku
@stop

@section('css')
	<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
@stop
    
@section('content')

<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<div class="row">
			
			<form method="POST" action="" enctype="multipart/form-data">
				@method('PATCH')
				@csrf
				<input type="hidden" name="id" value="{{$blog->id}}">
				<div class="col-lg-12 col-md-12">
					<div class="dashboard-list-box margin-top-0">
						<h4 class="gray">Edit Blog</h4>
						<div class="dashboard-list-box-static">
							
							<!-- Avatar -->
							<div class="edit-profile-photo rumah-sakit">
								<img src="{{asset('/images/'.$blog->gambar)}}" alt="">
								<div class="change-photo-btn">
									<div class="photoUpload">
									    <span><i class="fa fa-upload"></i> Upload Foto Blog (ukuran besar)</span>
									    <input type="file" class="upload" name="gambar" value="" />
									</div>
								</div>
							</div>
		
							<!-- Details -->
							<div class="my-profile">

								<label>Judul Blog</label>
								<input value="{{ $blog->judul }}" type="text" name="judul" maxlength="100" required>

								<label>Meta Deskripsi (Ringkasan 160 karakter)</label>
								<input value="{{ $blog->meta }}" type="text" name="meta" maxlength="160" required>

								<label>Isi Blog</label>
								<textarea rows="5" name="konten" id="editor" required>
                                	{{ $blog->konten }}
                                </textarea>

							</div>
		
							<div class="form-row center">
								<button type="submit" class="button margin-top-15">Simpan Blog</button>
							</div>
						</div>
					</div>
				</form>
			<!-- Profile -->
			</div>


			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop