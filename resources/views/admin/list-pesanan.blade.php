@switch($joins->booking_status)
    @case(1)
        <?php $joins['booking_status'] = "Booking Diterima"; ?>
        @break
    @case(2)
        <?php $joins['booking_status'] = "Booking Ditolak"; ?>
        @break
    @default
    	<?php $joins['booking_status'] = "Belum Diproses"; ?>
@endswitch
<div class="list-box-listing bookings">
	<div class="list-box-listing-img"><img src="{{asset('/images/'.$joins->userDetail->foto)}}" alt=""></div>
	<div class="list-box-listing-content">
		<div class="inner">
			<h3>{{ $joins->userDetail->nama }}
				<span class="booking-status">{{ $joins->booking_status }}</span>
			</h3>

			<div class="inner-booking-list">
				<h5>Waktu Pemesanan:</h5>
				<ul class="booking-list">
					<li>{{ $joins->tanggal_order->format('d-m-Y') }}</li>					
				</ul>
			</div>

			<div class="inner-booking-list">
				<h5>Kode Transaksi:</h5>
				<ul class="booking-list">
					<li>kode transaksi</li>					
				</ul>
			</div>
						
			<div class="inner-booking-list">
				<h5>Pasien:</h5>
				<ul class="booking-list">
					<li class="highlighted">{{ $joins->pasien['nama_pasien'] }} - {{ $joins->rumah_sakit->nama }} </li>
				</ul>
			</div>		

			<div class="inner-booking-list">
				<h5>Perawat:</h5>
				<ul class="booking-list">
					<li>{{ $joins->perawat->nama }}</li>
					<li>{{ $joins->perawat->telepon }}</li>
				</ul>
			</div>

			@if($joins->booking_status == 'Booking Diterima')
			<div class="inner-booking-list">
				<h5>Status Pembayaran:</h5>
				<ul class="booking-list">
					@if($joins->payment_status == '0')
					<li class="highlighted"><a href="{{url('/pemesanan/bukti-pembayaran/'.$joins->id)}}">Pembayaran belum diterima</a></li>
					@elseif($joins->payment_status == '1')
					<li class="highlighted">Pembayaran sudah diterima</li>
					@else
					<li class="highlighted"><a href="{{url('/pemesanan/bukti-pembayaran/'.$joins->id)}}">Pembayaran belum di konfirmasi admin</a></li>
					@endif
				</ul>
			</div>
			@endif

		</div>
	</div>
</div>
<div class="buttons-to-right">
	@if($joins->payment_status == 1)
		<a href="{{url('/pemesanan/bukti-pembayaran/'.$joins->id)}}" class="button detail"><i class="sl sl-icon-credit-card"></i> Bukti</a>
	@else
		@switch($joins->booking_status)
		    @case("Booking Diterima")
				<a href="{{url('/admin/pemesanan/tolak/'.$joins->id)}}" class="button delete"><i class="sl sl-icon-close"></i> Tolak</a>
		        @break
		    @case("Booking Ditolak")
				<a href="{{url('/admin/pemesanan/terima/'.$joins->id)}}" class="button detail"><i class="sl sl-icon-check"></i> Terima</a>
		        @break
		    @default
				<a href="{{url('/admin/pemesanan/tolak/'.$joins->id)}}" class="button delete"><i class="sl sl-icon-close"></i> Tolak</a>
				<a href="{{url('/admin/pemesanan/terima/'.$joins->id)}}" class="button detail"><i class="sl sl-icon-check"></i> Terima</a>
		@endswitch
	@endif
	<a href="{{url('/admin/pemesanan/detail/'.$joins->id)}}" class="button edit"><i class="sl sl-icon-book-open"></i> Detail</a>
</div>