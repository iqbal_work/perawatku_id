@extends('layout-admin.layout-admin')

@section('menu6')
    class="active"
@stop

@section('title')
Profil Rumah Sakit
@stop
    
@section('content')

<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Profil Rumah Sakit</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<form method="POST" action="" enctype="multipart/form-data">
				@if(!empty($rumahsakits))
					@method('PATCH')
				@endif
				@csrf
				<div class="col-lg-12 col-md-12">
					<div class="dashboard-list-box margin-top-0">
						<h4 class="gray">Profil Rumah Sakit</h4>
						<div class="dashboard-list-box-static">
							
							<!-- Avatar -->
							<div class="edit-profile-photo rumah-sakit">
								<img src="@if(!file_exists(public_path().('/images/'.$rumahsakits->foto))) {{asset('/images/ph_img.jpg')}} @else {{asset('/images/'.$rumahsakits->foto)}} @endif" alt="">
								<div class="change-photo-btn">
									<div class="photoUpload">
									    <span><i class="fa fa-upload"></i> Upload Foto Rumah Sakit (ukuran besar)</span>
									    <input type="file" class="upload" name="image" value="{{$rumahsakits['foto']}}" />
									</div>
								</div>
							</div>
		
							<!-- Details -->
							<div class="my-profile">

								<label>Nama Rumah Sakit</label>
								<input value="{{ $rumahsakits['nama'] }}" type="text" name="nama" required>

								<label>Alamat Rumah Sakit</label>
								<input value="{{ $rumahsakits['alamat'] }}" type="text" name="alamat" required>

								<div class="col-md-6 nopaddingleft">
									<label>Nomor Telepon Rumah Sakit</label>
									<input value="{{ $rumahsakits['telepon'] }}" type="text" name="telepon" required>
								</div>

								<div class="col-md-6 nopaddingright">
									<label>Email Rumah Sakit</label>
									<input value="{{ $rumahsakits['email'] }}" type="text" name="email" required>
								</div>

								<div class="col-md-6 nopaddingleft">
									<label>Nama Penanggung Jawab</label>
									<input value="{{ $rumahsakits['penanggungjawab'] }}" type="text" name="penanggungjawab" required>
								</div>

								<div class="col-md-6 nopaddingright">
									<label>Nomor Telepon Penanggung Jawab</label>
									<input value="{{ $rumahsakits['telepon_penanggungjawab'] }}" type="text" name="telepon_penanggungjawab" required>
								</div>

								<div class="col-md-12 nopaddingright">
									<label>Status Akun</label>
									@switch($rumahsakits->membership_status)
									    @case(1)
									        <?php $rumahsakits->membership_status = 'Premium Member'; ?>
									        @break
									    @default
									        <?php $rumahsakits->membership_status = 'Free'; ?> 
									        @break
									@endswitch
									<input type="text" value="{{$rumahsakits->membership_status}}" disabled>
								</div>

								<label>Deskripsi Rumah Sakit</label>
								<textarea name="deskripsi" id="notes" cols="30" rows="5" required>{{ $rumahsakits['deskripsi'] }}</textarea>

								<label>Nomor Rekening Rumah Sakit</label>
								<table id="pricing-list-container">
									<tr class="pricing-list-item pattern">
										@if(!empty($rekenings))
											@foreach($rekenings as $rekening)
												<td>
													<div class="fm-input pricing-name"><input type="text" value="{{$rekening->nama_bank}}" name="bank[]" /></div>
													<div class="fm-input pricing-name"><input type="text" value="{{$rekening->nomor_rekening}}" name="rekening[]" /></div>
													<div class="fm-close"><a href="{{url('/admin/rs/profil-rs/delete/'.$rekening->id)}}"><i class="fa fa-remove"></i></a></div>
												</td>
											@endforeach
										@endif
										<td>
											<div class="fm-input pricing-name"><input type="text" placeholder="Bank" name="bank[]" /></div>
											<div class="fm-input pricing-name"><input type="text" placeholder="Nomor Rekening" name="rekening[]" /></div>
											<div class="fm-close"><a href="#"><i class="fa fa-remove"></i></a></div>
										</td>
									</tr>
								</table>
								<a href="#" class="margin-top-0 button add-pricing-list-item">Tambah</a>

							</div>
		
							<div class="form-row center">
								<button type="submit" class="button margin-top-15">Simpan Data</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!-- Profile -->
		</div>

			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop