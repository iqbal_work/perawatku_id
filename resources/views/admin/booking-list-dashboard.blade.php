<strong>{{ $joins->userDetail->nama }}</strong>
<ul>
	<li class="@if($joins->payment_status == 'diterima') paid @else unpaid @endif">Rp. {{ number_format($joins->biaya, 0) }}</li>
	<li>Kode Transaksi: {{ $joins->kode_transaksi }} </li>
	<li>Tanggal Pemesanan : {{ $joins->tanggal_order->format('d-m-Y') }} </li>
</ul>
<div class="buttons-to-right">
	<a href="{{url('/pemesanan/bukti-pembayaran/'.$joins->id)}}" class="button gray">Lihat Bukti</a>
</div>