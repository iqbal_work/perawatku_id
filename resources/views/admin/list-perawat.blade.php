@inject('fee', 'App\Fee')
<?php 
	$biaya = $fee::where('perawat_id', $perawat->id)->first();
?>
<div class="list-box-listing">
	<div class="list-box-listing-img">
		<img src="@if(!file_exists(public_path().('/images/'.$perawat->foto))) {{asset('/images/ph_img.jpg')}} @else {{asset('/images/'.$perawat->foto)}} @endif" alt="">
	</div>
	<div class="list-box-listing-content">
		<div class="inner">
			<h3>{{ $perawat['nama'] }} @if(empty($biaya)) <span class="badge badge-danger"><span class="fa fa-exclamation"></span></span> @endif</h3>
			<span>{{ $perawat['umur'] }} Tahun, {{ $perawat['alamat'] }}</span><br>
			<span>Mulai Aktif: {{ $perawat['mulai_aktif'] }}</span>
			<div class="star-rating" data-rating="5"></div>
		</div>
	</div>
</div>
<div class="buttons-to-right">
	<a href="{{url('/admin/perawat/detail-perawat/'.$perawat['id'])}}" class="button detail"><i class="sl sl-icon-book-open"></i> Detail</a>
	<a href="{{url('/admin/perawat/edit-perawat/'.$perawat['id'])}}" class="button edit"><i class="sl sl-icon-note"></i> Edit</a>
	<a href="#confirm-dialog" class="popup-with-zoom-anim button delete"><i class="sl sl-icon-close"></i> Hapus</a>
</div>

<div id="confirm-dialog" class="zoom-anim-dialog mfp-hide">
	<div class="small-dialog-header">
		<h3 class="center">Ingin hapus?</h3>
	</div>
	<div class="margin-top-0 divcenter center">
		<a href="{{url('/admin/perawat/hapus-perawat/'.$perawat['id'])}}"><button class="button edit">Iya</button></a>
		<a href="#"><button class="button delete">Tidak</button></a>
	</div>
</div>