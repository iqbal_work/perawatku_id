@extends('layout-admin.layout-admin')

@section('menu7')
    class="active"
@stop

@section('title')
Upgrade Akun Rumah Sakit
@stop
    
@section('content')
@inject('rumahsakit', 'App\RumahSakit')
<?php
	$status = $rumahsakit->where('user_id', Auth::id())->first()->membership_status;
?>

<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Upgrade Akun Rumah Sakit</h2>
				</div>
			</div>
		</div>

		@if ($status == 0)
		<div class="row">
			<div class="payment">

				<div class="payment-tab payment-tab-active">
					<div class="payment-tab-trigger">
						<input checked id="free" name="plan" type="radio" value="free">
						<label for="free">Free Account</label>
					</div>

					<div class="payment-tab-content">
						<p>Jumlah perawat yang bisa dimasukan terbatas, yaitu sebanyak 1 perawat.</p>
					</div>
				</div>


				<div class="payment-tab">
					<div class="payment-tab-trigger">
						<input type="radio" name="plan" id="premium" value="premium">
						<label for="premium">Premium Account</label>						
					</div>

					<div class="payment-tab-content">
						<p>Jumlah perawat yang bisa dimasukan tidak terbatas, dengan melakukan pembayaran untuk upgrade akun rumah sakit.</p>
						<p>Detail pembayaran sebagai berikut:</p>
						<div class="row">
							<div class="col-md-6">
								<div class="card-label">
									<label for="norek">Nomor Rekening</label>
									<input id="norek" name="norek" value="Bank Mandiri: 1234  5678  9876  5432" disabled="true" type="text">
								</div>
							</div>
							<div class="col-md-6">
								<div class="card-label">
									<label for="norek">Jumlah Nominal</label>
									<input id="nominal" name="nominal" value="Rp 1.800.000/tahun" disabled="true" type="text">
								</div>
							</div>
						</div>
						<p>Jika sudah melakukan pembayaran, silahkan klik tombol Konfirmasi Pembayaran dibawah ini.</p>
						<form action="" method="POST" >
							@csrf
							<div class="form-row center">
								<button type="submit" class="button margin-top-30">Konfirmasi Pembayaran</button>
							</div>
						</form>
						
					</div>
				</div>

			</div>

			

			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>
		@else
		<div class="row">
			<div class="col-md-12">

				<div class="booking-confirmation-page">
					<i class="fa fa-check-circle"></i>
					<h3 class="margin-top-30">Terima kasih atas pembayaranmu!</h2>
					<p>Tim Perawatku.id akan segera memproses upgrade akun rumah sakit jika pembayaran sudah diterima. Jika ada kendala, tim Perawatku.id akan segera menghubungi kamu.</p>
				</div>

			</div>
		</div>
		@endif

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop