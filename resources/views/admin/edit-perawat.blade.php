@extends('layout-admin.layout-admin')

@section('menu3')
    class="active"
@stop

@section('title')
Edit Perawat
@stop
    
@section('content')

<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Edit Perawat</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">

				<div id="add-listing">

					<!-- Section -->
					<div class="add-listing-section">

						<!-- Headline -->
						<div class="add-listing-headline">
							<h3>Edit Data Perawat</h3>
						</div>

						<!-- Row -->
						<div class="row with-forms">
							<form method="POST" action="{{url('/admin/perawat/edit-perawat')}}" enctype="multipart/form-data">
								@method('PATCH')
								@csrf
								<input type="hidden" name="id" value="{{$perawats->id}}">
								<div class="col-md-12">
									<div class="edit-profile-photo">
										<img src="@if(!file_exists(public_path().('/images/'.$perawats->foto))) {{asset('/images/ph_img.jpg')}} @else {{asset('/images/'.$perawats->foto)}} @endif" alt="">
										<div class="change-photo-btn">
											<div class="photoUpload">
											    <span><i class="fa fa-upload"></i> Ganti Foto Perawat</span>
											    <input type="file" class="upload" name="foto" />
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<h5>Nama Lengkap</h5>
									<input type="text" name="nama" value="{{ $perawats->nama }}">
								</div>

								<div class="col-md-6">
									<h5>Alamat</span></h5>
									<input type="text" name="alamat" value="{{ $perawats->alamat }}">
								</div>

								<div class="col-md-6">
									<h5>Domisili</span></h5>
									<input type="text" name="domisili" value="{{ $perawats->domisili }}">
								</div>

								<div class="col-md-6">
									<h5>Jenis Kelamin</span></h5>
									<select name="jenis_kelamin" class="chosen-select" required="required">
										<option selected="true">Jenis Kelamin</option>
										<option value="Laki Laki" @if($perawats->jenis_kelamin == "Laki Laki") selected @endif>Laki - Laki</option>
										<option value="Perempuan" @if($perawats->jenis_kelamin == "Perempuan") selected @endif>Perempuan</option>
									</select>
								</div>

								<div class="col-md-6">
									<h5>Agama</span></h5>
									<select name="agama" class="chosen-select" required="required">
										<option selected="true">Agama</option>
										<option value="Islam" @if($perawats->agama == "Islam") selected @endif>Islam</option>
										<option value="Katolik" @if($perawats->agama == "Katolik") selected @endif>Katolik</option>
										<option value="Protestan" @if($perawats->agama == "Protestan") selected @endif>Protestan</option>
										<option value="Budha" @if($perawats->agama == "Budha") selected @endif>Budha</option>
										<option value="Hindu" @if($perawats->agama == "Hindu") selected @endif>Hindu</option>
										<option value="Lainnya" @if($perawats->agama == "Lainnya") selected @endif>Lainnya</option>
									</select>
								</div>

								<div class="col-md-6">
									<h5>Umur</h5>
									<input type="text" name="umur" value="{{ $perawats->umur }}">
								</div>

								<div class="col-md-6">
									<h5>Nomor Telepon</h5>
									<input type="text" name="telepon" value="{{ $perawats->telepon }}">
								</div>

								<div class="col-md-6">
									<h5>Email</h5>
									<input type="text" name="email" value="{{ $perawats->email }}">
								</div>

								<div class="col-md-6">
									<h5>Pendidikan</h5>
									<select name="pendidikan" class="chosen-select" required="required">
										<option selected="true">Tingkat Pendidikan</option>
										<option value="D4" @if($perawats->pendidikan == "D4") selected @endif>D4</option>
										<option value="D3" @if($perawats->pendidikan == "D3") selected @endif>D3</option>
										<option value="S1" @if($perawats->pendidikan == "S1") selected @endif>S1</option>
										<option value="SMK" @if($perawats->pendidikan == "SMK") selected @endif>SMK</option>
										<option value="Lainnya" @if($perawats->pendidikan == "Lainnya") selected @endif>Lainnya</option>
									</select>
								</div>

								<div class="col-md-6">
									<h5>Institusi</h5>
									<input type="text" name="institusi" value="{{ $perawats->institusi }}">
								</div>

								<div class="col-md-6">
									<h5>Sertifikasi yang Dimiliki</h5>
									<input type="text" name="sertifikasi" value="{{ $perawats->sertifikasi }}">
								</div>

								<div class="col-md-6">
									<h5>Mulai Aktif</h5>
									<select name="mulai_aktif" class="chosen-select">
										<option value="1" @if($perawats->mulai_aktif == 1) selected @endif>Belum ada pengalaman</option>
										<option value="2" @if($perawats->mulai_aktif == 2) selected @endif >Pengalaman kurang dari 5 tahun</option>
										<option value="3" @if($perawats->mulai_aktif == 3) selected @endif >Pengalaman 5 - 10 tahun</option>
										<option value="4" @if($perawats->mulai_aktif == 4) selected @endif >Pengalaman lebih dari 10 tahun</option>
									</select>
								</div>

								<div class="col-md-6">
									<h5>Pengalaman menjadi Perawat</h5>
									<textarea name="pengalaman" id="notes" cols="30" rows="3">{{ $perawats->pengalaman }}</textarea>
								</div>

								<div class="col-md-6">
									<h5>Deskripsi/Keterangan Tambahan Perawat</h5>
									<textarea name="deskripsi" id="notes" cols="30" rows="3">{{ $perawats->deskripsi }}</textarea>
								</div>

								<div class="col-md-6">
									<h5>Jenis Perawatan</h5>
									<select name="jenis_perawatan" class="chosen-select">
										<option value="1" @if($perawats->jenis_perawatan == 1) selected @endif>Visit Perawat</option>
										<option value="2" @if($perawats->jenis_perawatan == 2) selected @endif>Visit Bidan</option>
										<option value="3" @if($perawats->jenis_perawatan == 3) selected @endif>Visit Fisioterapi</option>
									</select>
								</div>

								<div class="col-md-6">
									<h5>Bersedia tinggal dengan pasien?</h5>
									<select name="bersedia" class="chosen-select">
										<option value="1" @if($perawats->bersedia == 1) selected @endif>Iya</option>
										<option value="2" @if($perawats->bersedia == 2) selected @endif>Tidak</option>
									</select>
								</div>

								<div class="form-row center">
									<button type="submit" class="button margin-top-15">Simpan Data</button>
								</div>
							</form>
							<!-- Avatar -->
							

						</div>
						<!-- Row / End -->

					</div>

					

				</div>
			</div>

			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2018 Perawatku.id. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

@stop