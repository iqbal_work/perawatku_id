<h3>Pesanan baru dari website dengan kode: {{ $kode }}</h3>

<p>
    <ul>
        <li>Nama: {{ $nama }}</li>
        <li>Alamat: {{ $alamat }}</li>
        <li>Email: {{ $email }}</li>
        <li>HP: {{ $nomor_hp }}</li>
        <li>Kunjungan: {{ $kunjungan }}</li>
        <li>Layanan: @if ($book->layanan != NULL || $book->layanan != "")
                {{ $book->layanan }}
            @else
                -
                @endif | Rp {{ number_format($layanan, 0, ',', '.') }}</li>
        <li>Tindakan: @if ($book->tindakan != NULL || $book->tindakan != "")
                {{ $book->tindakan }}
            @else
                -
                @endif | Rp {{ number_format($tindakan, 0, ',', '.') }}</li>
        <li>Kode Unik: {{ $kodeunik }}</li>
    </ul>
</p>
<p>Total Biaya: Rp {{ number_format($total, 0, ',', '.') }}</p>