<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Bukti Pembayaran - Perawatku.id</title>
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" href="{{asset('css/invoice.css')}}">

	<link rel="shortcut icon" type="image/png" sizes="32x32" href="{{asset('images/logo/favicon-32.png')}}">
	<link rel="shortcut icon" type="image/png" sizes="64x64" href="{{asset('images/logo/favicon-64.png')}}">
	<link rel="shortcut icon" type="image/png" sizes="96x96" href="{{asset('images/logo/favicon-96.png')}}">
</head> 

<body>

<!-- Print Button -->
<a href="javascript:window.print()" class="print-button" style="float: none">Cetak bukti pembayaran</a>

<!-- Invoice -->
<div id="invoice">

	<!-- Header -->
	<div class="row">
		<div class="col-md-6">
			<div id="logo"><img src="{{asset('images/logo/logo-full.png')}}" alt="Logo Perawatku"></div>
		</div>

		<div class="col-md-6">	

			<p id="details">
				<strong>Kode:</strong> {{ $booking->kode_transaksi }} <br>
				<strong>Tanggal:</strong> {{ $booking->created_at->format('d-m-Y') }} <br>
			</p>
		</div>
	</div>


	<!-- Client & Supplier -->
	<div class="row">
		<div class="col-md-12">
			<h2>Detail Pembayaran</h2>
		</div>

		<div class="col-md-2">
			<strong class="margin-bottom-5">Penyedia</strong>
			<p>
				Perawatku.id
			</p>
		</div>

		<div class="col-md-4">
			<strong class="margin-bottom-5">Pemesan</strong>
			<p>
				{{ $pasien->nama_pasien }} - {{ $pasien->nomor_hp }}
			</p>
		</div>

		<div class="col-md-6">
			<strong class="margin-bottom-5">Transfer</strong> (atas nama Ogy Winenriandhika)<br>
			@foreach ($bank as $rekenings)
				<div class="col-md-6">
					<span>{{ $rekenings->nama_bank }}</span>
				</div>

				<div class="col-md-6">
					<span>{{ $rekenings->nomor_rekening }}</span>
				</div>
			@endforeach
		</div>
	</div>


	<!-- Invoice -->
	<div class="row">
		<div class="col-md-12">
			<table class="margin-top-20">
				<tr>
					<th>Kunjungan</th>
					<th>Layanan</th>
					<th>Tindakan</th>
					<th>Kode Unik</th>
				</tr>

				<tr>
					<td>{{ $booking->tanggal_order->format('d-m-Y') }}</td>
					<td>@if ($book->layanan != NULL || $book->layanan != "")
							{{ $book->layanan }}
						@else
							-
						@endif</td>
					<td>@if ($book->tindakan != NULL || $book->tindakan != "")
							{{ $book->tindakan }}
						@else
							-
						@endif</td>
					<td>Rp {{ $booking->kodeunik }}</td>
				</tr>
			</table>
		</div>
		
		<div class="col-md-4 col-md-offset-8">	
			<table id="totals">
				<tr>
					<th>Total Biaya</th>
					<th><span>Rp {{ number_format($total, 0, ',', '.') }}</span></th>
				</tr>
			</table>
		</div>
	</div>


	<!-- Footer -->
	<div class="row">
		<div class="col-md-12">
			<ul id="footer">
				<li><span>www.perawatku.id</span></li>
				<li><a href="#">info@perawatku.id</a></li>
				<li>0857-2532-6342</li>
			</ul>
		</div>
	</div>
		
</div>
@if (Auth::user()->role == 'user')<a href="{{url('/profil')}}" class="print-button">Kembali ke Profil</a>
@else 
	@if($join->rumahsakit != null)
		<a href="{{url('/admin/dashboard')}}" class="print-button">Kembali ke Dashboard</a>
	@else
		<a href="{{url('/superadmin/dashboard')}}" class="print-button">Kembali ke Dashboard</a>
	@endif
@endif
</html>
