@extends('layout-user.layout')

@section('menu5')
    class="current"
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
	{{asset('images/logo/favicon-96.png')}}
@stop

@section('title')
Gabung Perawatku
@stop
    
@section('content')

<div class="clearfix"></div>

<!-- Container -->
<div class="container">

	<div class="row">
		<div class="col-md-12">

			<section class="center">
				<!-- <h2>Gabung Menjadi Perawat di Perawatku.id</h2> -->
				<!-- <p>We're sorry, but the page you were looking for doesn't exist.</p> -->

				<div class="row" style="margin: 30px 0;">
					<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSf1t60IfhwA7bAKqxwe6IKX4oKDHYhSPbxTloncc0WoQg0_pg/viewform?embedded=true" width="768" height="500" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
				</div>

			</section>

		</div>
	</div>

</div>
<!-- Container / End -->

@stop