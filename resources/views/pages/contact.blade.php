@extends('layout-user.layout')

@section('title')
    Contact
@stop

@section('desc')
    Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('content')

    <div class="clearfix"></div>

    <div class="container">

        <div class="row margin-bottom-70">
            <div class="col-md-12">

                <h3 class="headline margin-top-70 margin-bottom- text-center">Hubungi Kami</h3>

                <form method="post" action="{{url('/kirim-pesan')}}" enctype="multipart/form-data">
                    <div class="row">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-6">
                            <label>Nama Lengkap</label>
                            <input placeholder="Nama Lengkap" type="text" name="nama" required>
                        </div>
                        <div class="col-md-6">
                            <label>Email</label>
                            <input placeholder="Email" type="email" name="email" required>
                        </div>
                        <div class="col-md-6">
                            <label>Nomor HP</label>
                            <input placeholder="HP" type="text" name="hp" required>
                        </div>
                        <div class="col-md-6">
                            <label>Subjek Pesan</label>
                            <input placeholder="Subjek" type="text" name="subjek" required>
                        </div>
                        <div class="col-md-12">
                            <label>Isi Pesan</label>
                            <textarea rows="5" name="pesan" required></textarea>
                        </div>
                    </div>

                    <div class="form-row center">
                        <button type="submit" name="submit" class="button margin-top-15">Kirim Pesan</button>
                    </div>

                    <div class="clearfix"></div>
                    @if(Session::has('success'))
                        <div class="alert alert-success center">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                </form>

            </div>

        </div>
    </div>

@stop