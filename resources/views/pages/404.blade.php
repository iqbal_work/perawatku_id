@extends('layout-user.layout')

@section('title')
404 Not Found
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop
    
@section('content')

<div class="clearfix"></div>

<!-- Container -->
<div class="container">

	<div class="row">
		<div class="col-md-12">

			<section id="not-found" class="center">
				<h2>404 <i class="fa fa-question-circle"></i></h2>
				<p>We're sorry, but the page you were looking for doesn't exist.</p>

				<!-- Search -->
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="margin-top-50 margin-bottom-10">
							<button class="button" onclick="window.location.href='{{url('/')}}'" style="margin:0 auto; font-size: 18px;">Back to Home</button>
						</div>
					</div>
				</div>
				<!-- Search Section / End -->


			</section>

		</div>
	</div>

</div>
<!-- Container / End -->

@stop