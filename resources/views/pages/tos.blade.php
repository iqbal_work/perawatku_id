@extends('layout-user.layout')

@section('title')
    Syarat dan Ketentuan
@stop

@section('desc')
    Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('content')

    <div class="clearfix"></div>

    <!-- Container -->
    <div class="container">

        <div class="row margin-bottom-70">
            <div class="col-md-12" style="text-align: justify;">

                <h3 class="headline margin-top-70 margin-bottom- text-center">Syarat & Ketentuan Perawatku.id</h3>

                <p>
                    Dengan mengakses situs web ini Anda mengetahui dan setuju untuk terikat oleh Syarat dan Ketentuan Penggunaan (selanjutnya, 'Syarat', 'Ketentuan Layanan', 'Syarat dan Ketentuan', 'Perjanjian'). Jika Anda tidak setuju dengan Syarat dan Ketentuan Penggunaan ini, dimohon untuk tidak mengakses atau menggunakan situs ini. Jika Anda berusia dibawah 18 tahun, Anda membenarkan bahwa Anda memiliki orang tua atau izin dari wali hukum untuk menggunakan Website ini. Pelanggaran Syarat dan Ketentuan Penggunaan ini dapat mengakibatkan pemutusan akun Anda dengan atau tanpa pemberitahuan sebelumnya.<br><br>
                    Perawatku.id dapat merevisi dan memperbarui syarat dan ketentuan ini kapanpun. Tinjau syarat dan ketentuan secara berkala, karena penggunaan lanjutan Anda dari situs web ini akan mengartikan bahwa Anda menerima revisi atau pembaharuan apapun.

                </p>
                <ol type="1">
                    <li>Pemesan/Pengguna wajib memberikan informasi yang benar dan lengkap mengenai pengguna layanan baik, data personal, kondisi pengguna dan tindakan atas layanan yang akan dilakukan.</li>
                    <li>Perawatku.id tidak menyediakan alat kesehatan yang mendukung atas tindakan yang dipesan. Kecuali untuk tensimeter, stetoskop dan thermometer, karena penyedia layanan akan membawa alat tersebut saat melakukan pelayanan.</li>
                    <li>Medical Personal (Perawat/Terapis) adalah tenaga medis yang sudah mendapatkan lisensi dan pengarahan untuk melakukan pelayanan dengan baik.</li>
                    <li>Perawatku.id tidak bertanggung jawab secara langsung untuk hal-hal yang memperburuk pengguna layanan yang melibatkan medical personal Perawatku.id. Tanggung jawab atas seluruh biaya serta tuntutan yang mungkin timbul atas kejadian tersebut akan menjadi tanggung jawab pribadi medical personal Perawatku.id. Jika ada informasi yang dapat membantu untuk proses investigasi, maka Perawatku hanya dapat membantu sebagai mediator dalam mempertemukan kedua pihak untuk mencari penyelesaian masalah tersebut.</li>
                    <li>Perubahan & Pembatalan:</li>
                </ol>
                <ol type="a" style="margin-left: 30px;">
                    <li>Semua pemesanan Perawatku.id bersifat fleksibel, dapat diubah, dibatalkan & dikembalikan uang (sesuai kebijakan Perawatku.id), namun tidak dapat dialihkan ke orang/pihak lain.</li>
                    <li>Dengan melakukan pemesanan di Perawatku.id, Anda dianggap telah memahami, menerima dan menyetujui kebijakan dan ketentuan pembatalan, serta segala syarat dan ketentuan tambahan yang diberlakukan. Kami akan mencantumkan kebijakan dan ketentuan pembatalan tersebut di setiap Surat Konfirmasi yang Kami kirim ke Anda. Harap dicatat bahwa tarif atau penawaran tertentu tidak memenuhi syarat untuk pembatalan atau pengubahan. Anda bertanggung-jawab untuk memeriksa dan memahami sendiri kebijakan dan ketentuan pembatalan tersebut sebelumnya</li>
                    <li>Jika Anda ingin melihat ulang, melakukan perubahan, atau membatalkan pesanan Anda, harap konfirmasi langsung ke Perawatku.id melalui telepon atau email ke cs@perawatku.id. Harap dicatat bahwa Anda mungkin saja dikenakan biaya tambahan atas pembatalan sesuai dengan kebijakan dan ketentuan pembatalan.</li>
                    <li>Kami akan bertanggung-jawab terhadap perubahan yang secara signifikan berpengaruh pada ketentuan Produk dan Layanan yang telah Kami konfirmasikan sebelumnya, yaitu:
                        <ol type="i">
                            <li>Pembatalan pemesanan;</li>
                            <li>Penggantian Medical Personal;</li>
                        </ol>
                    </li>
                    <li>Dalam hal perubahan signifikan, maka Anda memiliki pilihan untuk:
                        <ol type="i">
                            <li>Menerima penggantian yang Kami tawarkan; atau</li>
                            <li>Menerima pengembalian pembayaran dalam waktu 14 (empat belas) hari kerja.</li>
                        </ol>
                    </li>
                    <li>Kami tidak bertanggung-jawab ataupun menanggung kerugian Anda dalam hal Kami tidak dapat memberi Layanan kepada Anda, akibat dari hal-hal yang terjadi akibat keadaan memaksa atau yang diluar kekuasaan Kami atau Mitra Kami untuk mengendalikan, seperti, tapi tidak terbatas pada: perang, kerusuhan, teroris, perselisihan industrial, tindakan pemerintah, bencana alam, kebakaran atau banjir, cuaca ekstrim, dan lain sebagainya.</li>
                </ol>
                <br>
                <p><strong>Ketentuan Tambahan</strong></p>

                <ol type="1">
                    <li>Perlu diketahui bahwa Perawat kami bukanlah Asisten rumah tangga yang dipekerjakan melakukan pekerjaan rumah tangga yang bukan porsinya seperti membersihkan rumah, mencuci pakaian seluruh keluarga serta memasak untuk satu rumah. Perawat kami hanya melakukan tugas atau pekerjaan yang berkaitan dengan medis pasien saja.
                        </li>
                    <li>Perawat kami adalah tenaga professional yang berpengalaman, bekerja sesuai dengan Job order dan standar kompetensi asuhan keperawatan dan memiliki Surat Tanda Registrasi (STR) perawat.
                        </li>
                    <li>Apabila Pengguna Jasa melanggar kesepakatan dengan memaksa Perawat kami melakukan pekerjaan Asisten Rumah Tangga untuk mengurus pekerjaan rumah tangga, serta tidak memberikan hak-hak perawat sesuai dengan yang terlampir dalam kontrak, Perawatku akan memberikan teguran lisan, atau menarik Perawat kami dengan atau tanpa melalui persetujuan dari Pengguna Jasa.
                        </li>
                    <li>Segala tindakan kriminal atau pidana yang dilakukan oleh tenaga kerja adalah bukan tanggung jawab pihak agency, untuk hal ini pihak agency membantu dalam memberikan informasi keberadaan tenaga kerja.
                        </li>
                    <li>Agar perawat betah bekerja, dihimbau, menjalin hubungan kerja yang baik. Diperhatikan kualitas komunikasi, makan cukup dan istirahat yang memadai. (Hindari komentar negatif, seperti: perawat kerjanya tidur terus, padahal, semalam, perawat yang bersangkutan begadang (tidak tidur), dan lain sebagainya.
                    </li>
                </ol>

                <p><strong>Biaya dan Syarat Pembelian</strong></p>

                <p>Anda setuju untuk membayar semua Biaya atau beban ke Akun Anda sesuai dengan beban, biaya, dan persyaratan penagihan yang berlaku pada saat biaya atau beban bertempo dan dilunasi. Dengan menyediakan Perawatku.id nomor kartu kredit atau rekening PayPal dan informasi pembayaran yang terkait, Anda setuju bahwa Perawatku.id berwenang untuk menagih akun Anda atas semua biaya dan pembayaran jatuh tempo dan terhutang untuk Perawatku.id dan bahwa tidak ada pemberitahuan tambahan atau persetujuan yang diperlukan.
                </p>

                <p><strong>Penggunaan Hukum</strong></p>

                <p>Anda tidak dapat menggunakan layanan kami dengan cara apapun yang melanggar hukum, peraturan, perjanjian atau tarif atau melanggar hak hukum pihak ketiga, dengan cara apapun, memfitnah, penipuan, tindakan tidak senonoh, menyinggung atau menipu, mengancam, melecehkan, menyalahgunakan atau mengintimidasi orang lain, merusak nama atau reputasi dari Website, afiliasinya, atau anak, untuk mematahkan keamanan pada jaringan komputer, atau untuk mengakses account yang bukan milik Anda, dan dengan cara apapun yang mengganggu penggunaan pelanggan laindan kenyamanan dari layanan yang diberikan oleh website.</p>
                <p>Anda dapat menggunakan website ini hanya sesuai dengan Syarat dan Ketentuan Penggunaan ini dan untuk tujuan yang sah sebagaimana diatur oleh undang-undang. Anda mungkin tidak menyalahgunakan situs ini dengan cara apapun. Anda juga tidak diizinkan untuk menggunakan perangkat lunak otomatis ketika mengakses dan menggunakan situs ini.</p>

                <p><strong>Ketentuan Lain-lain</strong></p>

                <p>Website dapat dilihat secara internasional dan mungkin berisi referensi ke produk atau layanan yang tidak tersedia atau disetujui di semua negara. Referensi untuk produk atau jasa tertentu tidak berarti bahwa produk tersebut atau layanan sesuai atau tersedia untuk semua usia pembelian secara hukum di semua lokasi, atau bahwa kami berniat untuk membuat produk atau layanan tersebut tersedia di lokasi tersebut. Kegagalan kami untuk menjalankan atau menerapkan hak atau ketentuan Persyaratan Layanan ini tidak akan beroperasi sebagai diabaikannya hak atau ketentuan tersebut.</p>

                <p><strong>Kepatuhan Pemerintah</strong></p>

                <p>Kinerja kami di bawah Ketentuan Layanan ini tunduk pada hukum yang ada dan proses hukum, dan tidak ada yang terkandung dalam Ketentuan Layanan ini adalah pengurangan hak kami untuk mematuhi pemerintah, permintaan pengadilan dan penegakan hukum atau persyaratan yang berkaitan dengan penggunaan Kronusasia atau informasi yang tersedia atau yang dikumpulkan oleh kami sehubungan dengan penggunaan tersebut.</p>

                <p><strong>Ketentuan Hukum dan Keberlakuan</strong></p>

                <p>Jika ada ketentuan atau bagian dari Ketentuan Layanan ini dinyatakan ilegal, tidak sah, atau tidak dapat diterapkan, secara keseluruhan atau sebagian, itu harus diubah sejauh minimum yang diperlukan untuk memperbaiki kekurangan atau diganti dengan ketentuan yang sedekat secara hukum diperbolehkan dengan ketentuan bahwa tidak sah atau tidak dapat diterapkan dan tidak akan mempengaruhi legalitas, keabsahan atau keberlakuan dari ketentuan lain atau bagian dari Ketentuan Layanan ini, dan ketentuan lain dari Syarat dan Ketentuan akan tetap berlaku sepenuhnya.</p>

            </div>
        </div>

    </div>
    <!-- Container / End -->

@stop