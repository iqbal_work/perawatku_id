@extends('layout-user.layout')

@section('title')
    FAQ
@stop

@section('desc')
    Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('content')

    <div class="clearfix"></div>

    <!-- Container -->
    <div class="container">

        <div class="row margin-bottom-70">
            <div class="col-md-12">

                <h3 class="headline margin-top-70 margin-bottom- text-center">FAQ Perawatku.id</h3>

                <div class="style-2">

                    <div class="toggle-wrap">
                        <span class="trigger"><a href="#">Apa itu Perawatku?<i class="sl sl-icon-plus"></i></a></span>
                        <div class="toggle-container">
                            <p>Perawatku adalah platform online yang membantu kamu mendapatkan tenaga caregiver dan praktisi kesehatan (Perawat, Bidan dan Fisoterapis) yang bisa langsung datang ke lokasimu.</p>
                        </div>
                    </div>

                    <div class="toggle-wrap">
                        <span class="trigger"><a href="#">Apa keunggulan Perawatku?<i class="sl sl-icon-plus"></i> </a></span>
                        <div class="toggle-container">
                            <p>Layanan Homecare yang tersedia dalam 24 jam, dengan tenaga praktisi yang profesional,  berpengalaman dan bermitra dengan beberapa rumah sakit di Jakarta.</p>
                        </div>
                    </div>

                    <div class="toggle-wrap">
                        <span class="trigger"><a href="#">Bagaimana cara mendapatkan dan memilih tenaga kesehatan yang sesuai dengan kebutuhan saya?<i class="sl sl-icon-plus"></i> </a></span>
                        <div class="toggle-container">
                            <p>Perawatku terhubung secara cepat dan mudah dengan ratusan tenaga kesehatan professional dan rumah sakit untuk membantu Anda menemukan perawat home care, perawat lansia atau perawat orang tua, perawat orang sakit dan terapis kesehatan sesuai kebutuhan Anda.</p>
                        </div>
                    </div>

                    <div class="toggle-wrap">
                        <span class="trigger"><a href="#">Area mana saja yang dilayani oleh Perawatku?<i class="sl sl-icon-plus"></i> </a></span>
                        <div class="toggle-container">
                            <p>Untuk saat ini, layanan homecare tersedia hanya di area Jabodetabek saja.</p>
                        </div>
                    </div>

                    <div class="toggle-wrap">
                        <span class="trigger"><a href="#"> Bagaimana privasi data pasien Perawatku?<i class="sl sl-icon-plus"></i> </a></span>
                        <div class="toggle-container">
                            <p>Perawatku menghormati dan menjaga kerahasiaan serta integritas dari setiap histori pengguna. Catatan medis Anda hanya dapat dimiliki dan dikelola sendiri, sehingga hanya Anda yang dapat memilih kepada siapa Anda ingin memberikan catatan medis Anda.</p>
                        </div>
                    </div>

                    <div class="toggle-wrap">
                        <span class="trigger"><a href="#"> Bagaimana cara mengetahui background detail tenaga kesehatan yang diinginkan?<i class="sl sl-icon-plus"></i> </a></span>
                        <div class="toggle-container">
                            <p>Anda bisa cek dengan klik  detail pada foto profil tenaga kesehatan yang anda inginkan.</p>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <!-- Container / End -->

@stop