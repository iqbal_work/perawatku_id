<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($blogs as $blog)
        <url>
            <loc>https://perawatku.id/blog/{{ str_replace(' ', '-',$blog->judul) }}</loc>
            <lastmod>{{ $blog->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
        <url>
            <loc>https://perawatku.id/blog</loc>
            <lastmod>2018-08-17T00:00:00+00:00</lastmod>
            <changefreq>daily</changefreq>
            <priority>1</priority>
        </url>
        <url>
            <loc>https://perawatku.id/faq</loc>
            <lastmod>2018-08-17T00:00:00+00:00</lastmod>
            <changefreq>weekly</changefreq>
            <priority>1</priority>
        </url>
        <url>
            <loc>https://perawatku.id/kontak</loc>
            <lastmod>2018-08-17T00:00:00+00:00</lastmod>
            <changefreq>weekly</changefreq>
            <priority>1</priority>
        </url>
        <url>
            <loc>https://perawatku.id/layanan</loc>
            <lastmod>2018-08-17T00:00:00+00:00</lastmod>
            <changefreq>weekly</changefreq>
            <priority>1</priority>
        </url>
        <url>
            <loc>https://perawatku.id/perawat</loc>
            <lastmod>2018-08-17T00:00:00+00:00</lastmod>
            <changefreq>weekly</changefreq>
            <priority>1</priority>
        </url>
        <url>
            <loc>https://perawatku.id/</loc>
            <lastmod>2018-08-17T00:00:00+00:00</lastmod>
            <changefreq>weekly</changefreq>
            <priority>1</priority>
        </url>
</urlset>