<!DOCTYPE html>
<head>

<title>@yield('title') - Perawatku.id</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="{{asset('css/style.css')}}">
<link rel="stylesheet" href="{{asset('css/colors/main.css')}}" id="colors">

@yield('css')

<link rel="shortcut icon" type="image/png" sizes="32x32" href="{{asset('images/logo/favicon-32.png')}}">
<link rel="shortcut icon" type="image/png" sizes="64x64" href="{{asset('images/logo/favicon-64.png')}}">
<link rel="shortcut icon" type="image/png" sizes="96x96" href="{{asset('images/logo/favicon-96.png')}}">

</head>

<body>

<div id="wrapper">

	@include('layout-admin.navbar-admin')

	@yield('content')

</div>

<script type="text/javascript" src="{{asset('js/jquery-2.2.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/mmenu.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/chosen.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/rangeslider.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/counterup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/tooltips.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/custom.js')}}"></script>

@yield('js')

</body>
</html>