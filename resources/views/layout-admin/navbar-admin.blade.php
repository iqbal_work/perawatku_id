@inject('profpic', 'App\RumahSakit')
@inject('booking', 'App\Booking')

<?php 
	$foto = $profpic->where('user_id', Auth::id())->first();
	$booking = count($booking->where('booking_status', '0')->where('owner_user_id', Auth::id())->get());
?>

<header id="header-container" class="fixed fullwidth dashboard admin">

	<div id="header" class="not-sticky">
		<div class="container">
			
			<!-- Left Side Content -->
			<div class="left-side">
				
				<!-- Logo -->
				<div id="logo">
					<a href="{{url('/admin/dashboard')}}"><img src="{{asset('images/logo/logo2.png')}}" alt="Logo Perawatku"></a>
					<a href="{{url('/admin/dashboard')}}" class="dashboard-logo"><img src="{{asset('images/logo/logo2.png')}}" alt="Logo Perawatku"></a>
				</div>
				
			</div>
			<!-- Left Side Content / End -->

			<!-- Right Side Content / End -->
			<div class="right-side">
				<!-- Header Widget -->
				<div class="header-widget">
					
					<!-- User Menu -->
					<div class="user-menu">
						<!-- <div class="user-name"><span><img src="{{asset('images/profile/dashboard-admin.jpg')}}" alt=""></span>RSIA Bunda</div>
						<ul>
							<li><a href="{{url('/admin/profile-rs')}}"><i class="sl sl-icon-user"></i> Profil</a></li>
							<li><a href="#small-dialog" class="popup-with-zoom-anim"><i class="sl sl-icon-power"></i> Keluar</a></li>
						</ul> -->

						@if (!Auth::check())
						<a href="#sign-in-dialog" id = "modal-login" class="sign-in popup-with-zoom-anim"><i class="sl sl-icon-login"></i> Masuk</a>
					    @endif

					    @if (Auth::check())
					    <div class="user-menu">
							<div class="user-name">
								<span>
									<img src="@if(!file_exists(public_path().('/images/'.$foto->foto))) {{asset('/images/icons/avatar.jpg')}} @else {{asset('/images/'.$foto->foto)}} @endif" alt="">
									<!-- ukuran foto tolong di fixed in jadi kotak aja -->
								</span>{{ Auth::user()->name }}
							</div>
							<ul>
								<li><a href="#small-dialog" class="popup-with-zoom-anim"><i class="sl sl-icon-power"></i> Keluar</a></li>
							</ul>
						</div>
					    @endif
					</div>

				</div>
				<!-- Header Widget / End -->
			</div>
			<!-- Right Side Content / End -->

		</div>
	</div>
	<!-- Header / End -->

</header>

<div class="clearfix"></div>
<!-- Header Container / End -->

<!-- Dashboard -->
<div id="dashboard">

	<!-- Navigation
	================================================== -->

	<!-- Responsive Navigation Trigger -->
	<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Navigasi</a>

	<div class="dashboard-nav">
		<div class="dashboard-nav-inner">

			<ul>
				<li @yield('menu1')><a href="{{url('/admin/dashboard')}}"><i class="sl sl-icon-settings"></i> Dashboard</a></li>
				<li @yield('menu2')>
					<a href="{{url('/admin/pemesanan')}}">
						<i class="fa fa-calendar-check-o"></i> 
						Pemesanan 
						@if ($booking != 0)
						<span class="badge badge-danger">{{ $booking }}</span>
						@endif
					</a>
				</li>
			</ul>
			
			<ul data-submenu-title="Perawat">
				<li @yield('menu3')><a href="{{url('/admin/perawat/daftar-perawat')}}"><i class="sl sl-icon-layers"></i> Daftar Perawat</a></li>
				<li @yield('menu4')><a href="{{url('/admin/perawat/ulasan-perawat')}}"><i class="sl sl-icon-star"></i> Ulasan Pengguna</a></li>
				<li @yield('menu5')><a href="{{url('/admin/perawat/tambah-perawat')}}"><i class="sl sl-icon-plus"></i> Tambah Perawat</a></li>
			</ul>	

			<ul data-submenu-title="Rumah Sakit">
				<li @yield('menu6')><a href="{{url('/admin/rs/profil-rs')}}"><i class="sl sl-icon-user"></i> Profil Rumah Sakit @if($foto->membership_status != 1) <span class="badge badge-danger"><span class="fa fa-exclamation"></span></span> @endif</a></li>
				@if($foto->membership_status != 1)
				<li @yield('menu7')><a href="{{url('/admin/rs/upgrade-akun')}}"><i class="sl sl-icon-rocket"></i> Upgrade Akun</a></li>
				@endif
				<li @yield('menu8')><a href="{{url('/admin/rs/ubah-password')}}"><i class="sl sl-icon-key"></i> Ubah Password</a></li>
				<li><a href="#small-dialog" class="popup-with-zoom-anim"><i class="sl sl-icon-power"></i> Keluar</a></li>
			</ul>
			
		</div>
	</div>
	<!-- Navigation / End -->

	<div id="small-dialog" class="zoom-anim-dialog mfp-hide">
		<div class="small-dialog-header">
			<h3 class="center">Ingin keluar?</h3>
		</div>
		<div class="margin-top-0 divcenter center">
			<a href="{{url('/logout')}}"><button class="button edit">Iya</button></a>
			<a href="."><button class="button delete">Tidak</button></a>
		</div>
	</div>
