<!DOCTYPE html>
<head>

<title>@yield('title') - Perawatku.id</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="{{asset('css/style.css')}}">
<link rel="stylesheet" href="{{asset('css/colors/main.css')}}" id="colors">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />

<!-- Include CSS for icons. -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<style type="text/css">a:hover {text-decoration:none;}</style>

<link rel="shortcut icon" type="image/png" sizes="32x32" href="{{asset('images/logo/favicon-32.png')}}">
<link rel="shortcut icon" type="image/png" sizes="64x64" href="{{asset('images/logo/favicon-64.png')}}">
<link rel="shortcut icon" type="image/png" sizes="96x96" href="{{asset('images/logo/favicon-96.png')}}">

</head>

<body>

<div id="wrapper">

	@include('layout-admin.navbar-superadmin')

	@yield('content')

</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{asset('js/mmenu.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/chosen.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/rangeslider.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/counterup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/tooltips.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugin.js')}}"></script>

<script type="text/javascript" src="{{asset('js/ckeditor/ckeditor.js')}}"></script>

<script>
    CKEDITOR.replace( 'editor' );
</script>

</body>
</html>