<li>
	<div class="avatar"><img src="{{asset('images/'.$reviews->perawat->foto)}}" alt="" /></div>
	<div class="comment-content"><div class="arrow-comment"></div>
		<div class="comment-by"><a href="#">{{ $reviews->perawat->nama." - ".$reviews->rumahsakit->nama }}</a><span class="date">{{ $reviews->created_at->toFormattedDateString() }}</span>
			<div class="star-rating" data-rating="{{ $reviews->rating }}"></div>
		</div>
		<p>{{ $reviews->review }}</p>
	</div>
</li>