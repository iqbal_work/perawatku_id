@extends('layout-user.layout')

@section('menu2')
    class="current"
@stop

@section('desc')
    Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
    {{asset('images/logo/favicon-96.png')}}
@stop

@section('title')
    Tindakan
@stop

@section('content')

    <div class="clearfix"></div>

    <!-- Slider
    ================================================== -->
    <div class="listing-slider margin-bottom-0" style="background-image: url({{asset('/images/rumah-sakit/rs6.jpg')}}); background-repeat: no-repeat; background-position: 50%; background-size: cover;">
    </div>


    <!-- Content
    ================================================== -->
    <div class="container">
        <div class="row sticky-wrapper">
            <div class="col-lg-8 col-md-8 padding-right-30">

                <!-- Titlebar -->
                <div id="titlebar" class="listing-titlebar">
                    <div class="listing-titlebar-title">
                        <h2>Tindakan</h2>
                    </div>
                </div>

                <div id="rs-deskripsi" class="listing-section margin-bottom-25">

                    <h3 class="listing-desc-headline margin-bottom-20">Deskripsi</h3>

                    <p>
                        Deskripsi Tindakan
                    </p>

                </div>
            </div>
            <div class="col-lg-4 col-md-4 margin-top-50 sticky">
                @if (Auth::check())
                    <a href="{{url('/layanan/tindakan/pesan')}}" class="button book-now fullwidth">Pesan Layanan</a>
                @else
                    <a href="#sign-in-dialog" class="button book-now fullwidth sign-in popup-with-zoom-anim">Pesan Layanan</a>
                @endif
            </div>

        </div>
    </div>

@stop
