<?php 
	$name = str_replace(' ', '_', $rumahsakits->nama);
?>
<div class="col-lg-6 col-md-6">
	<a href="{{url('/rumah-sakit/'.$name)}}" class="listing-item-container compact">
		<div class="listing-item">
			<img src="{{asset('/images/rumah-sakit/'.$rumahsakits->foto)}}" alt="{{ $rumahsakits->nama }}">

			<div class="listing-item-content">
				<h3>{{ $rumahsakits->nama }}</h3>
				<span>{{ count($rumahsakits->perawat) }} Perawat</span>
			</div>
			<span class="category-box-btn">Detail</span>
		</div>
	</a>
</div>