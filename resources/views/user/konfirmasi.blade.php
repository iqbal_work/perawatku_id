@extends('layout-user.layout')

@section('title')
Konfirmasi Pemesanan
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
{{asset('images/logo/favicon-96.png')}}
@stop
    
@section('content')

@if ($book->owner_user_id == 1)
	<?php
		$pihak = 'Perawatku.id';
	?>
@else
	<?php
		$pihak = 'Rumah Sakit';
	?>
@endif

<div class="clearfix"></div>

<!-- Container -->
<div class="container">
	<div class="row">
		<div class="col-md-12">

			<div class="booking-confirmation-page">
				<i class="fa fa-check-circle"></i>
				@if($book->booking_status == 0 && $book->payment_status == 0)
					<h2 class="margin-top-30">Terima kasih atas pesananmu!</h2>
					<p>Pihak {{$pihak}} akan mengecek ketersediaan perawat untukmu. Tunggu informasi di halaman profil dan di whatsapp kamu untuk mendapatkan konfirmasi dari pihak {{$pihak}} agar selanjutnya segera melakukan pembayaran.</p>
				@else
					<h2 class="margin-top-30">Terima kasih atas pembayaranmu!</h2>
					<p>Pihak {{$pihak}} akan memproses pesanan kamu. Untuk mempercepat proses, silahkan mengirimkan bukti pembayaran ke whatsapp 0857-2532-6342 dan tunggu informasi selanjutnya dari kami.</p>
				@endif

				<a href="{{url('/profil')}}" class="button edit margin-top-30">Ke Halaman Profil</a>

				@if($book->booking_status != 0 && $book->payment_status == 1)
					<a href="{{url('/pemesanan/invoice/'.$book->kode_transaksi)}}" class="button detail">Invoice</a>
				@endif
			</div>

		</div>
	</div>
</div>
<!-- Container / End -->

@stop