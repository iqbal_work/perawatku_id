@extends('layout-user.layout')

@section('menu2')
    class="current"
@stop

@section('desc')
    Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
    {{asset('images/logo/favicon-96.png')}}
@stop

@section('title')
    Perawatan Bayi
@stop

@section('content')

    <div class="clearfix"></div>

    <!-- Slider
    ================================================== -->
    <div class="listing-slider margin-bottom-0" style="background-image: url({{asset('/images/rumah-sakit/rs6.jpg')}}); background-repeat: no-repeat; background-position: 50%; background-size: cover;">
    </div>


    <!-- Content
    ================================================== -->
    <div class="container">
        <div class="row sticky-wrapper">
            <div class="col-lg-8 col-md-8 padding-right-30">

                <!-- Titlebar -->
                <div id="titlebar" class="listing-titlebar">
                    <div class="listing-titlebar-title">
                        <h2>Perawatan Bayi</h2>
                    </div>
                </div>

                <div id="rs-deskripsi" class="listing-section margin-bottom-25">

                    <h3 class="listing-desc-headline margin-bottom-20">Deskripsi</h3>

                    <p>
                        Bayi merupakan hadiah dan karunia yang tidak dapat ditolak oleh orangtuanya. Pengetahuan bagaimana cara merawat bayi baru lahir perlu mendapat perhatian khusus, karna bayi baru lahir tidak dapat memberitahu anda apa yang mereka inginkan .Terlebih jika anda merupakan orang tua yang tidak cukup memiliki waktu luang untuk si buah hati.
                    </p>
                    <p>
                        Di sini kami dapat memberikan solusi yaitu menyediakan layanan untuk buah hati anda, seperti perawatan tali pusat dan pemenuhan nutrisi bayi anda dengan kasih sayang selayaknya kasih sayang orang tua terhadap si bayi. Kami mampu menjamin perawat yang ada di perawatku.id merupakan perawat yang terlatih, berpengalaman, dan mampu bertanggung jawab dalam merawat dan menjaga bayi.
                    </p>

                </div>
            </div>
            <div class="col-lg-4 col-md-4 margin-top-50 sticky">
                @if (Auth::check())
                    <a href="{{url('/layanan/perawatan-bayi/pesan')}}" class="button book-now fullwidth">Pesan Layanan</a>
                @else
                    <a href="#sign-in-dialog" class="button book-now fullwidth sign-in popup-with-zoom-anim">Pesan Layanan</a>
                @endif
            </div>

        </div>
    </div>

@stop
