@extends('layout-user.layout')

@section('menu4')
    class="current"
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
{{asset('images/logo/favicon-96.png')}}
@stop

@section('title')
Blog
@stop
    
@section('content')

<div class="clearfix"></div>

<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12 center">
				<h2>Blog Perawatku</h2>
				<span>Informasi terbaru dan menarik untuk menambah pengetahuan kamu</span>
			</div>
		</div>
	</div>
</div>

<section class="fullwidth padding-top-20 padding-bottom-50" data-background-color="#fff">
	<div class="container">

		<div class="row col-md-12">
			<!-- Blog Post Item -->
			@foreach($blog as $blogs)
			<div class="col-md-4">
				<a href="{{url('blog/'.str_replace(' ', '-',$blogs->judul))}}" class="blog-compact-item-container">
					<div class="blog-compact-item">
						<img src="{{asset('/images/'.$blogs->gambar)}}" alt="{{ $blogs->judul }}">
						<!-- <span class="blog-item-tag">Tips</span> -->
						<div class="blog-compact-item-content">
							<ul class="blog-post-tags">
								<li>{{ $blogs->created_at->format('d-m-Y') }}</li>
							</ul>
							<h3>{{ $blogs->judul }}</h3>
							<p>{!! str_limit($blogs->konten,75) !!}</p>
						</div>
					</div>
				</a>
			</div>
			@endforeach
			<!-- Blog post Item / End -->

			<!-- Pagination -->
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12">
					<div class="pagination-container margin-top-20 margin-bottom-40">
						<nav class="pagination">
							{{$blog->links()}}
						</nav>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

@stop