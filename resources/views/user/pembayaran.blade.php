@extends('layout-user.layout')

@section('title')
Konfirmasi Pembayaran
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
{{asset('images/logo/favicon-96.png')}}
@stop
    
@section('content')

<div class="clearfix"></div>

<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="center">Lakukan Pembayaran</h2>
				<h4 class="text-center">Saat ini, layanan Perawatku.id hanya tersedia untuk wilayah Jakarta.</h4>
			</div>
		</div>
	</div>
</div>

<!-- Container -->
<div class="container">
	<div class="row">

		<!-- Content
		================================================== -->
		<div class="col-lg-8 col-md-8 padding-right-30">

			<h3 class="margin-top-0 margin-bottom-15">Data Pemesan</h3>

			<div class="row margin-bottom-15">

				<div class="col-md-6">
					<h5>Nama</h5>
					<span>{{ $book->user_detail->nama }}</span>
				</div>

				<div class="col-md-6">
					<h5>Alamat</h5>
					<span>{{ $book->user_detail->alamat }}</span>
				</div>

				<div class="col-md-6">
					<h5>Email</h5>
					<span>{{ $book->user_detail->email }}</span>
				</div>

				<div class="col-md-6">
					<h5>Nomor Telepon</h5>
					<span>{{ $book->user_detail->nomor_hp }}</span>
				</div>

			</div>

			<h3 class="margin-top-50 margin-bottom-15">Data Pasien</h3>
			@if ($book->pasien->tipe_pasien != "3")
				<!-- untuk sendiri, orang lain, manula -->
				<div class="row">

					<div class="col-md-6">
						<h5>Nama</h5>
						<span>{{ $book->pasien->nama_pasien }}</span>
					</div>

					<div class="col-md-6">
						<h5>Alamat</h5>
						<span>{{ $book->pasien->alamat_pasien }}</span>
					</div>

					<div class="col-md-6">
						<h5>Email</h5>
						<span>{{ $book->pasien->email_pasien }}</span>
					</div>

					<div class="col-md-6">
						<h5>Nomor Telepon</h5>
						<span>{{ $book->pasien->nomor_hp }}</span>
					</div>

					<div class="col-md-6">
						<h5>Hari Kunjungan</h5>
						<span>{{ $book->tanggal_order->format('d-m-Y') }}</span>
					</div>

					<div class="col-md-6">
						<h5>Layanan</h5>
						<span>
							@if ($book->layanan != NULL || $book->layanan != "")
								{{ $book->layanan }}
							@else
								-
							@endif
						</span>
					</div>

					<div class="col-md-6">
						<h5>Tindakan</h5>
						<span>
							@if ($book->tindakan != NULL || $book->tindakan != "")
								{{ $book->tindakan }}
							@else
								-
							@endif
						</span>
					</div>

				</div>
			@else
				<!-- untuk bayi -->
				<div class="row">

					<div class="col-md-4">
						<h5>Nama Bayi</h5>
						<span>{{ $book->pasien->nama_pasien }}</span>
					</div>

					<div class="col-md-4">
						<h5>Alamat Bayi</h5>
						<span>{{ $book->pasien->alamat_pasien }}</span>
					</div>

					<div class="col-md-4">
						<h5>Berat Badan</h5>
						<span>{{ $book->pasien->berat_badan }}</span>
					</div>

					<div class="col-md-4">
						<h5>Tinggi Badan</h5>
						<span>{{ $book->pasien->tinggi_badan }}</span>
					</div>

					<div class="col-md-4">
						<h5>Umur</h5>
						<span>{{ $book->pasien->umur }}</span>
					</div>

					<div class="col-md-4">
						<h5>Suku</h5>
						<span>{{ $book->pasien->suku }}</span>
					</div>

					<div class="col-md-4">
						<h5>Agama</h5>
						<span>{{ $book->pasien->agama }}</span>
					</div>

					<div class="col-md-4">
						<h5>Kondisi Medis</h5>
						<span>{{ $book->pasien->kondisi_medis }}</span>
					</div>

					<div class="col-md-4">
						<h5>Hari Kunjungan</h5>
						<span>{{ $book->tanggal_order->format('d-m-Y') }}</span>
					</div>

					@if(!empty($book->layanan))
                        <div class="col-md-4">
                            <h5>Layanan</h5>
                            <span>
							@if ($book->layanan != NULL || $book->layanan != "")
								{{ $book->layanan }}
							@else
								-
							@endif
						</span>
                        </div>
                    @else
                        <div class="col-md-4">
                            <h5>Layanan</h5>
                            <span>-</span>
                        </div>
                    @endif

                    @if(!empty($book->tindakan))
                        <div class="col-md-4">
                            <h5>Tindakan</h5>
                            <span>
							@if ($book->tindakan != NULL || $book->tindakan != "")
								{{ $book->tindakan }}
							@else
								-
							@endif
						</span>
                        </div>
                    @else
                        <div class="col-md-4">
                            <h5>Tindakan</h5>
                            <span>-</span>
                        </div>
                    @endif

				</div>
			@endif


			<h3 class="margin-top-50 margin-bottom-30">Metode Pembayaran</h3>

			<!-- Payment Methods Accordion -->
			<div class="payment">
				<div class="payment-tab payment-tab-active">
					<div class="payment-tab-trigger">
						<input type="radio" name="cardType" id="creditCart" value="creditCard" checked="true">
						<label for="creditCart">Transfer via ATM</label>
					</div>

					<div class="payment-tab-content">
						<div class="row">
							<div class="col-md-4">
								<h5>Bank</h5>
							</div>

							<div class="col-md-4">
								<h5>Nomor Rekening</h5>
							</div>

							<div class="col-md-4">
								<h5>Atas Nama</h5>
							</div>

							@foreach ($book->bank as $rekenings)
								<div class="col-md-4">
									<span>{{ $rekenings->nama_bank }}</span>
								</div>

								<div class="col-md-4">
									<span>{{ $rekenings->nomor_rekening }}</span>
								</div>

								<div class="col-md-4">
									<span>{{ $rekenings->nama }}</span>
								</div>
							@endforeach

						</div>
					</div>
				</div>

			</div>
			<!-- Payment Methods Accordion / End -->

			<a href="{{url('/pemesanan/konfirmasi/'.$book->kode_transaksi)}}" class="button booking-confirmation-btn margin-top-40 margin-bottom-65">Lakukan Pembayaran</a>
		</div>


		<!-- Sidebar
		================================================== -->
		<div class="col-lg-4 col-md-4 margin-top-0 margin-bottom-60">

			<!-- Booking Summary -->
			<div class="listing-item-container compact order-summary-widget">
				<div class="listing-item">
					<img src="@if($book->user_detail->foto == NULL)
					{{asset('/images/icons/avatar.jpg')}}
					@else
					{{asset('/images/'.$book->user_detail->foto)}}
					@endif" alt="">

					<div class="listing-item-content">
						<h3 class="center">{{ $book->user_detail->nama }}</h3>
					</div>
				</div>
			</div>
			<div class="boxed-widget opening-hours summary margin-top-0">
				<h3><i class="fa fa-calendar-check-o"></i> Pemesanan</h3>
				<ul>
					<li>Kunjungan <span>{{ $book->tanggal_order->format('d-m-Y') }}</span></li>
					<li>Layanan
						<span>
							@if ($book->layanan != NULL || $book->layanan != "")
								{{ $book->layanan }}
							@else
								-
							@endif
						</span>
					</li>
					<li>Tindakan 
						<span>
							@if ($book->tindakan != NULL || $book->tindakan != "")
								{{ $book->tindakan }}
							@else
								-
							@endif
						</span>
					</li>
					<li>Kode Unik <span>Rp {{ $book->kodeunik }}</span></li>
					<li class="total-costs">Total Biaya <span>Rp {{ number_format($total, 0, ',', '.') }}</span></li>
				</ul>

			</div>
			<!-- Booking Summary / End -->

		</div>

	</div>
</div>
<!-- Container / End -->

@stop