@if($bookings->rumahsakit != null)
	<?php 
		$foto = '/images/rumah-sakit/'.$bookings->rumahsakit->foto;
		$nama = $bookings->rumahsakit->nama;
	?> 
@else
	<?php
		$foto = 'images/icons/avatar.jpg';
		$nama = 'Perawatku.id';
	?>
@endif

<div class="col-lg-12 col-md-12">
	<div class="listing-item-container list-layout">
		<div class="listing-item">
			
			<div class="listing-item-image">
				<img src="{{asset($foto)}}" alt="">
			</div>
			
			<div class="listing-item-content">
				<div class="listing-item-inner">
					<h5><strong>Status: {{ $bookings->booking_status }}</strong> | {{ $bookings->created_at->format('d-m-Y') }}</h5>
					<h3>@if ($bookings->perawat != null) {{ $bookings->perawat->nama." - ".$nama}} @else Perawatku.id @endif</h3>
					<span>Layanan: @if ($book->layanan != NULL || $book->layanan != "")
							{{ $book->layanan }}
						@else
							-
						@endif | Tindakan: @if ($book->tindakan != NULL || $book->tindakan != "")
							{{ $book->tindakan }}
						@else
							-
						@endif</span><br>
					<span>Biaya: Rp {{ number_format($bookings->biaya, 0, ',', '.') }} </span><br>
					@if ($bookings->booking_status != 'Belum Diproses')
						<a href="{{url('/pemesanan/pembayaran/'.$bookings->kode_transaksi)}}" class="link">Data Pemesanan</a>
						@if ($bookings->payment_status != '0')
							| <a href="{{url('/pemesanan/invoice/'.$bookings->kode_transaksi)}}" class="link">Invoice</a>
						@endif
					@endif
				</div>
			</div>
		</div>
	</div>
</div>