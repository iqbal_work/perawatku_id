@extends('layout-user.layout')

@section('menu2')
    class="current"
@stop

@section('desc')
    Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
    {{asset('images/logo/favicon-96.png')}}
@stop

@section('title')
    Perawatan Paliatif
@stop

@section('content')

    <div class="clearfix"></div>

    <!-- Slider
    ================================================== -->
    <div class="listing-slider margin-bottom-0" style="background-image: url({{asset('/images/rumah-sakit/rs6.jpg')}}); background-repeat: no-repeat; background-position: 50%; background-size: cover;">
    </div>


    <!-- Content
    ================================================== -->
    <div class="container">
        <div class="row sticky-wrapper">
            <div class="col-lg-8 col-md-8 padding-right-30">

                <!-- Titlebar -->
                <div id="titlebar" class="listing-titlebar">
                    <div class="listing-titlebar-title">
                        <h2>Perawatan Paliatif</h2>
                    </div>
                </div>

                <div id="rs-deskripsi" class="listing-section margin-bottom-25">

                    <h3 class="listing-desc-headline margin-bottom-20">Deskripsi</h3>

                    <p>
                        Perawatan paliatif adalah Suatu pendekatan yang meningkatkan kualitas hidup pasien dan keluarga mereka dalam menghadapi masalah yang berkaitan dengan penyakit yang mengancam jiwa melalui pencegahan dan meringankan penderitaan melalui identifikasi awal dan pengkajian cermat menyeluruh dan penanganan nyeri serta masalah fisik, psikologis, dan spiritual lainnya, di mulai sejak diagnosa ditegakkan, saat pasien menjalankan pengobatan sampai pasca kematian. </p>
                    <p>Perawatan paliatif sangat membutuhkan perhatian khusus karena tidak semua orang dapat melakukan perawatan paliatif. Hanya dengan orang yang tepat seperti perawat bersertifikasi yang mampu melakukan perawatan paliatif.</p>
                    <p>Perawatan yang dilakukan oleh seorang perawat akan sangat tepat karena perawat akan mampu memberikan asuhan keperawatan sesuai dengan ilmu yang legal. Beberapa tindakan seperti meringankan kesakitan dari segi fisik, emosional, dan spiritual yang dapat meningkatkan tujuan perawatan serta meningkatkan kepuasan perawatan, mengkaji tanda dan gejala serta meringankan tanda dan gejala yang muncul, mempertahankan kondisi fisik, emosional dan spiritual, dll dapat dilakukan perawat ketika merawat pasien dengan penyakit terminal.</p>
                    <p>Orang dengan penyakit terminal dirawat di Rumah Sakit dalam jangka waktu yang lama akan menambah masalah penyakit yang ada karena tertular penyakit dari lingkungan Rumah Sakit ataupun pasien lain. Oleh karena itu, menggunakan perawat dari Perawatku.id  yang bersertifikasi serta memiliki skill dan kemampuan maupun pengalaman untuk merawat seseorang dengan penyakit terminal di rumah merupakan pilihan tepat.</p>

                </div>
            </div>
            <div class="col-lg-4 col-md-4 margin-top-50 sticky">
                @if (Auth::check())
                    <a href="{{url('/layanan/perawatan-paliatif/pesan')}}" class="button book-now fullwidth">Pesan Layanan</a>
                @else
                    <a href="#sign-in-dialog" class="button book-now fullwidth sign-in popup-with-zoom-anim">Pesan Layanan</a>
                @endif
            </div>

        </div>
    </div>

@stop
