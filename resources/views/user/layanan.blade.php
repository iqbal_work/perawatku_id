@extends('layout-user.layout')

@section('menu2')
    class="current"
@stop

@section('desc')
    Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
    {{asset('images/logo/favicon-96.png')}}
@stop

@section('title')
    Layanan
@stop

@section('content')

    <div class="container margin-top-50 margin-bottom-50" id="layanan">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="headline centered">
                    Layanan yang Kami Miliki
                </h2>
                <h4 class="text-center">Saat ini, layanan Perawatku.id hanya tersedia untuk wilayah Jakarta.</h4>
            </div>
        </div>

        <div class="row icons-container margin-top-20">

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="icon-box-2 with-line">
                    <a href="{{url('layanan/perawatan-bayi')}}" id="img-perawatan-bayi"><i><img src="{{asset('images/icons/baby.png')}}"></i>
                        <h3>Perawatan Bayi</h3></a>
                    <a href="{{url('layanan/perawatan-bayi')}}" class="button" id="btn-perawatan-bayi">Pilih</a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="icon-box-2 with-line">
                    <a href="{{url('layanan/perawatan-luka')}}" id="img-perawatan-luka"><i><img src="{{asset('images/icons/luka.png')}}"></i>
                        <h3>Perawatan Luka</h3></a>
                    <a href="{{url('layanan/perawatan-luka')}}" class="button" id="btn-perawatan-luka">Pilih</a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="icon-box-2 with-line">
                    <a href="{{url('layanan/perawatan-medis')}}" id="img-perawatan-medis"><i><img src="{{asset('images/icons/medis.png')}}"></i>
                        <h3>Perawatan Medis</h3></a>
                    <a href="{{url('layanan/perawatan-medis')}}" class="button" id="btn-perawatan-medis">Pilih</a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="icon-box-2">
                    <a href="{{url('layanan/perawatan-paliatif')}}" id="img-perawatan-paliatif"><i><img src="{{asset('images/icons/non-medis.png')}}"></i>
                        <h3>Perawatan Paliatif</h3></a>
                    <a href="{{url('layanan/perawatan-paliatif')}}" class="button" id="btn-perawatan-paliatif">Pilih</a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="icon-box-2">
                    <a href="{{url('layanan/tindakan')}}" id="img-perawatan-tindakan"><i><img src="{{asset('images/icons/tindakan.png')}}"></i>
                        <h3>Tindakan Tambahan</h3></a>
                    <a href="{{url('layanan/tindakan')}}" class="button" id="btn-perawatan-tindakan">Pilih</a>
                </div>
            </div>
        </div>

    </div>

@stop