<div id="perawat-dialog{{$perawatt->id}}" class="perawat-dialog zoom-anim-dialog mfp-hide">

	<div class="small-dialog-header center">
		<h3>Detail Perawat</h3>
	</div>

	<!--Tabs -->
	<div class="sign-in-form style-1">

		<ul class="tabs-nav">
			<li class=""><a href="#tab1">Informasi</a></li>
			{{--<li><a href="#tab2">Biaya</a></li>--}}
			<!-- <li><a href="#tab3">Ulasan</a></li> -->
			<!-- <li><a href="#tab4">Pesan Perawat</a></li> -->
			
		</ul>

		<div class="tabs-container alt">

			<!-- Login -->
			<div class="tab-content" id="tab1" style="display: none;">

				<div class="col-md-12">
					<div class="edit-profile-photo center margin-top-25">
						<img src="@if(!file_exists(public_path().('/images/'.$perawatt->foto))) {{asset('/images/icons/avatar.jpg')}} @else {{asset('/images/'.$perawatt->foto)}} @endif" alt="">
					</div>
				</div>

				<div class="col-md-6 margin-bottom-10">
					<h5>Nama Lengkap</h5>
					<span>{{ $perawatt->nama }}</span>
				</div>

				<div class="col-md-6 margin-bottom-10">
					<h5>Umur</h5>
					<span>{{ $perawatt->umur }}</span>
				</div>

				<div class="clearfix"></div>

				<div class="col-md-6">
					<h5>Email</h5>
					<span>{{ $perawatt->asal_kampus }}</span>
				</div>

				<div class="col-md-6 margin-bottom-10">
					<h5>Sertifikasi yang Pernah Diikuti</h5>
					<span>{{ $perawatt->sertifikasi }}</span>
				</div>

				<div class="col-md-6 margin-bottom-10">
					<h5>Keahlian</h5>
					<span>{!! $perawatt->keahlian !!}</span>
				</div>

				<div class="col-md-6 margin-bottom-10">
					<h5>Pengalaman</h5>
					<span>{!! $perawatt->pengalaman !!}</span>
				</div>

				<div class="col-md-12 margin-bottom-30">
					<h5>Deskripsi Perawat</h5>
					<span>{!! $perawatt->deskripsi !!}</span>
				</div>

				{{--<div class="center">
					<button type="button" class="button divcenter" onclick="window.location.href='@if(Auth::check()) {{url('/pemesanan/'.$perawatt->id)}} @else #sign-in-dialog @endif'">Pesan Perawat</button>
				</div>--}}

			</div>

			<div class="tab-content" id="tab2" style="display:none">

				<div class="show-more">
					<div class="pricing-list-container">

						<h4>Daftar Layanan</h4>
						<ul>
							@foreach($perawatt->fee as $fee)
								@if ($fee->biaya != 0)
								<li>
									<h5>{{ $fee->layanan }}</h5>
									<span>Rp {{ $fee->biaya }}</span>
								</li>
								@endif
							@endforeach
						</ul>
					</div>
				</div>
				<a class="show-more-button" data-more-title="Lihat lebih banyak" data-less-title="Lihat lebih sedikit"><i class="fa fa-angle-down"></i></a>

			</div>

			<!-- <div class="tab-content" id="tab3" style="display: none;">

				<span class="leave-rating-title">Beri rating untuk perawat</span>
				
				<div class="row">
					<div class="col-md-6">
						<div class="clearfix"></div>
						<div class="leave-rating margin-bottom-15">
							<input type="radio" name="rating" id="rating-1" value="1"/>
							<label for="rating-1" class="fa fa-star"></label>
							<input type="radio" name="rating" id="rating-2" value="2"/>
							<label for="rating-2" class="fa fa-star"></label>
							<input type="radio" name="rating" id="rating-3" value="3"/>
							<label for="rating-3" class="fa fa-star"></label>
							<input type="radio" name="rating" id="rating-4" value="4"/>
							<label for="rating-4" class="fa fa-star"></label>
							<input type="radio" name="rating" id="rating-5" value="5"/>
							<label for="rating-5" class="fa fa-star"></label>
						</div>
						<div class="clearfix"></div>
					</div>

				</div>
	
				<form id="add-comment" class="add-comment">
					<fieldset>

						<div>
							<label>Ulasan untuk Perawat:</label>
							<textarea cols="40" rows="3"></textarea>
						</div>

					</fieldset>

					<button class="button divcenter">Kirim Ulasan</button>
					<div class="clearfix"></div>
				</form>

				<section class="comments listing-reviews margin-top-25">
					<ul>
						<li>
							<div class="avatar"><img src="{{asset('images/profile/review-avatar.jpg')}}" alt="" /></div>
							<div class="comment-content"><div class="arrow-comment"></div>
								<div class="comment-by">Benarivo Putra<span class="date">April 2018</span>
									<div class="star-rating" data-rating="5"></div>
								</div>
								<p>Keren banget sih perawatnya. Dahsyat!</p>
							</div>
						</li>

					 </ul>
				</section>

			</div> -->

			<!-- <div class="tab-content" id="tab4" style="display: none;">

				<div class="boxed-widget booking-widget">
					<h3 class="center"><i class="fa fa-calendar-check-o "></i> Pesan Perawat</h3>
					<div class="row with-forms  margin-top-0">

						<h5>Kunjungan:</h5>

						<div class="clear"></div>

						<div class="col-lg-12 col-md-12">
							<input type="text" id="booking-date" placeholder="Hari kunjungan" data-lang="en" data-large-mode="true" data-large-default="true" data-min-year="2017" data-max-year="2017" data-lock="from">
						</div>
						
						<div class="col-lg-12 col-md-12">
							<input type="text" placeholder="Alamat kunjungan">
						</div>

						<div class="col-lg-12 col-md-12">
							<select data-placeholder="Jam kunjungan" class="chosen-select">
								<option label="Jam Buka"></option>
								<option>01.00</option>
								<option>02.00</option>
								<option>03.00</option>
								<option>04.00</option>
								<option>05.00</option>
								<option>06.00</option>
								<option>07.00</option>
								<option>08.00</option>
								<option>09.00</option>
								<option>10.00</option>
								<option>11.00</option>
								<option>12.00</option>	
								<option>13.00</option>
								<option>14.00</option>
								<option>15.00</option>
								<option>16.00</option>
								<option>17.00</option>
								<option>18.00</option>
								<option>19.00</option>
								<option>20.00</option>
								<option>21.00</option>
								<option>22.00</option>
								<option>23.00</option>
								<option>00.00</option>
							</select>
						</div>

						<h5>Butuh perawat untuk:</h5>

						<div class="clear"></div>
						
						<div class="col-lg-12 col-md-12">
							<select data-placeholder="Berapa jam" class="chosen-select">
								<option label="Berapa jam"></option>
								<option>1-3 jam</option>
								<option>4-6 jam</option>
								<option>7-9 jam</option>
								<option>10-12 jam</option>
								<option>13-15 jam</option>
								<option>16-18 jam</option>
								<option>19-21 jam</option>
								<option>22-24 jam</option>
							</select>
						</div>

						<div class="col-lg-12 col-md-12">
							<select data-placeholder="Tindakan" class="chosen-select" multiple>
								<option label="Tindakan"></option>
								<option>Tindakan 1</option>
								<option>Tindakan 2</option>
								<option>Tindakan 3</option>
								<option>Tindakan 4</option>
							</select>
						</div>

					</div>
					
					<a href="{{url('/pemesanan')}}" class="button book-now fullwidth margin-top-5">Pesan Sekarang</a>
				</div>

			</div> -->

		</div>
	</div>
</div>