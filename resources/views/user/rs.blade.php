@extends('layout-user.layout')

@section('menu2')
    class="current"
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
{{asset('images/logo/favicon-96.png')}}
@stop

@section('title')
Daftar Rumah Sakit
@stop
    
@section('content')

<div class="clearfix"></div>

<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12 center">
				<h2>Daftar Rumah Sakit</h2>
				<span>{{ count($rumahsakit) }} rumah sakit dan {{ count($perawat) }} perawat telah terdaftar</span>
			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->
<div class="container">
	<div class="row">
		
		<!-- Search -->
		<div class="col-md-12">
			<div class="main-search-input margin-top-0 margin-bottom-50">

				<div class="main-search-input-item">
					<input type="text" placeholder="Cari rumah sakit" value=""/>
				</div>

				<div class="main-search-input-item">
					<select data-placeholder="Pilih Kota" class="chosen-select">
						<option label="Pilih Kota"></option>	
						<option>Jakarta</option>
						<!-- <option>Bandung</option>
						<option>Bogor</option>
						<option>Tangerang</option> -->
					</select>
				</div>

				<div class="main-search-input-item">
					<select data-placeholder="Pilih Layanan" class="chosen-select">
						<option label="Pilih Layanan"></option>	
						<option>Home Visit</option>
						<option>Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan</option>
						<option>Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan</option>
						<option>Paket - Homecare Standby 24 Jam Selama 1 Bulan</option>
						<option>Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi</option>
						<option>Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi</option>
						<option>Perawatan Pasien Lansia</option>
					</select>
				</div>

				<button class="button">Cari</button>
			</div>
		</div>
		<!-- Search Section / End -->

		<!-- <div class="col-md-6">

			<div class="margin-top-0 margin-bottom-30">

				<div class="fullwidth-filters">

					<div class="sort-by">
						<div class="sort-by-select">
							<select data-placeholder="Urutkan" class="chosen-select-no-single">
								<option>Urutkan</option>	
								<option>Rating tertinggi</option>
								<option>Terbanyak dilihat</option>
								<option>Baru terdaftar</option>
								<option>Banyaknya perawat</option>
							</select>
						</div>
					</div>

				</div>

			</div>

		</div> -->

		<div class="col-md-12" id="rs">
			<div class="row">

				@foreach($rumahsakit as $rumahsakits)
					@include('user.list-rs')
				@endforeach

			</div>

			<!-- Pagination -->
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12">
					<!-- Pagination -->
					<div class="pagination-container margin-top-20 margin-bottom-40">
						<nav class="pagination">
							{{$rumahsakit->links()}}
						</nav>
					</div>
				</div>
			</div>
			<!-- Pagination / End -->

		</div>

	</div>
</div>

@stop