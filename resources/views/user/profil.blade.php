@extends('layout-user.layout')

@section('title')
Profil
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
{{asset('images/logo/favicon-96.png')}}
@stop
    
@section('content')

<div class="clearfix"></div>

<!-- Content
================================================== -->
<div class="container margin-top-50" id="profil">
	<div class="row sticky-wrapper">


		@include('layout-user.sidebar')


		<!-- Content
		================================================== -->
		<div class="col-lg-8 col-md-8 padding-left-30">

			<h3 class="margin-top-0 margin-bottom-40">Daftar Pemesanan</h3>

			<!-- Listings Container -->
			<div class="row">

				@foreach ($booking as $bookings)
					@include('user.list-booking')
				@endforeach
				<!-- Listing Item / End -->

			</div>
			
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12">
					<!-- Pagination -->
					<div class="pagination-container margin-top-20 margin-bottom-40">
						<nav class="pagination">
							{{$booking->links()}}
						</nav>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>

@stop