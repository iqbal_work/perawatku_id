@extends('layout-user.layout')

@section('menu2')
    class="current"
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
{{asset('images/logo/favicon-96.png')}}
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/datedropper.css')}}">
@stop

@section('title')
{{ $rumahsakit->nama }}
@stop
    
@section('content')

<div class="clearfix"></div>

<!-- Slider
================================================== -->
<div class="listing-slider margin-bottom-0" style="background-image: url({{asset('/images/'.$rumahsakit->foto)}}); background-repeat: no-repeat; background-position: 50%; background-size: cover;">
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row sticky-wrapper">
		<div class="col-lg-12 col-md-12">

			<!-- Titlebar -->
			<div id="titlebar" class="listing-titlebar">
				<div class="listing-titlebar-title">
					<h2>{{ $rumahsakit->nama }} <!--<span class="listing-tag">Eat & Drink</span>--></h2>
					<span>
						<a href="#listing-location" class="listing-address">
							<i class="fa fa-map-marker"></i>
							<a href="#">{{ $rumahsakit->alamat }}</a>
						</a>
					</span>
					<!-- <div class="star-rating" data-rating="4">
						<div class="rating-counter"><a href="#listing-reviews">(31 reviews)</a></div>
					</div> -->
				</div>
			</div>

			<!-- Listing Nav -->
			<div id="listing-nav" class="listing-nav-container">
				<ul class="listing-nav">
					<li><a href="#rs-deskripsi" class="active">Deskripsi</a></li>
					<li><a href="#rs-perawat">Perawat</a></li>
				</ul>
			</div>
			
			<!-- Overview -->
			<div id="rs-deskripsi" class="listing-section margin-bottom-25">

				<h3 class="listing-desc-headline margin-bottom-20">Deskripsi</h3>

				<p>
				    {!! $rumahsakit->deskripsi !!}
				</p>

			</div>

			<div id="rs-perawat" class="listing-section margin-bottom-25">

				<h3 class="listing-desc-headline margin-top-70 margin-bottom-30">Perawat</h3>

				@foreach ($rumahsakit->perawat as $perawat)
					@include('user.list-perawat')
				@endforeach
				
			</div>

		</div>

	</div>
</div>

@foreach ($rumahsakit->perawat as $perawats)
	@include('user.detail-perawat')
@endforeach

@stop
