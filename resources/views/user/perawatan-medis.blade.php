@extends('layout-user.layout')

@section('menu2')
    class="current"
@stop

@section('desc')
    Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
    {{asset('images/logo/favicon-96.png')}}
@stop

@section('title')
    Perawatan Medis
@stop

@section('content')

    <div class="clearfix"></div>

    <!-- Slider
    ================================================== -->
    <div class="listing-slider margin-bottom-0" style="background-image: url({{asset('/images/rumah-sakit/rs6.jpg')}}); background-repeat: no-repeat; background-position: 50%; background-size: cover;">
    </div>


    <!-- Content
    ================================================== -->
    <div class="container">
        <div class="row sticky-wrapper">
            <div class="col-lg-8 col-md-8 padding-right-30">

                <!-- Titlebar -->
                <div id="titlebar" class="listing-titlebar">
                    <div class="listing-titlebar-title">
                        <h2>Perawatan Medis</h2>
                    </div>
                </div>

                <div id="rs-deskripsi" class="listing-section margin-bottom-25">

                    <h3 class="listing-desc-headline margin-bottom-20">Deskripsi</h3>

                    <p>
                        Manfaat memiliki perawat di rumah untuk memberikan perawatan jangka panjang seperti sebagai berikut; perawat dapat melakukan pengkajian, pemeriksaan fisik, berkolaborasi dengan dokter terkait pengobatan dan perawatan berbasis ilmu pengetahuan keperawatan yang dapat dipertanggungjawabkan. Perawat Homecare24 merupakan perawat yang memiliki background pendidikan minimal D3 keperawatan serta memiliki pengalaman. Tiap perawatan jangka panjang yang dilakukan oleh perawat di Perawatku.id memiliki tujuan perawatan prioritas dengan mengedepankan mempertahankan serta meningkatkan kualitas hidup tiap individu yang dirawat sehingga tiap perawatan yang dilakukan jelas, terukur dan terarah.
                    </p>
                    <p>Perawatan medis merupakan perawatan yang berhubungan dengan ilmu kesehatan atau ilmu kedokteran untuk pencegahan perawatan dan manajemen penyakit-penyakit serta proses stabilisasi mental fisik dan rohani. Dalam perawatan medis dapat dilakukan dengan cara minum obat,terapi,latihan fisik dll. Sesuai yang dianjurkan oleh pihak kedokteran itu sendiri.</p>
                    <p>Di dalam perawatku.id menyediakan pelayanan perawatan medis , seperti dibawah ini :
                    </p>
                    <p>
                        <strong>Kolostomi</strong><br>
                        Perawatan Kolostomi adalah pembuatan sebuah lubang di dinding perut untuk mengeluarkan feses pada pasien yang mengalami masalah di usus besar.<br><br>

                        <strong>NGT</strong><br>
                        Perawatan NGT adalah tindakan memasukan selang (selang nasogastik) melalui hidung sampai ke lambung untuk memberikan nutrisi dan obat-obatan pada pasien yang tidak mampu mengkonsumsi makanan, cairan dan obat-obatan melalui mulut.<br><br>

                        <strong>Perawatan Pasca Operasi</strong><br>
                        Perawatan pasca operasi sangat penting, mulai dari latihan mobilisasi, penanganan nyeri, pemberian makanan dan mengelola cairan infus. Penanganan nyeri dan pengelolaan infus tidak dapat dilakukan oleh orang awam karena membutuhkan ilmu tertentu untuk melakukannya.<br><br>

                        <strong>Perawatan Kateter Urin</strong><br>
                        Perawat kami dapat membantu anda dengan perawatan kateterisasi urin di rumah, baik itu pemasangan kateter, penggantian kateter atau cuci kandung kemih. Perawatan kateter urin membutuhkan penanganan yang tepat untuk mencegah terjadinya infeksi pada saluran perkemihan.<br><br>
                    </p>

                </div>
            </div>
            <div class="col-lg-4 col-md-4 margin-top-50 sticky">
                @if (Auth::check())
                    <a href="{{url('/layanan/perawatan-medis/pesan')}}" class="button book-now fullwidth">Pesan Layanan</a>
                @else
                    <a href="#sign-in-dialog" class="button book-now fullwidth sign-in popup-with-zoom-anim">Pesan Layanan</a>
                @endif
            </div>

        </div>
    </div>

@stop
