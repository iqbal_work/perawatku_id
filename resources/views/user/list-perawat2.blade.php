
<div class="col-lg-3 col-md-4">
	<a class="listing-item-container">
		<div class="listing-item">
			<img src="@if(!file_exists(public_path().('/images/'.$perawat->foto))) {{asset('/images/icons/avatar.jpg')}} @else {{asset('/images/'.$perawat->foto)}} @endif" alt="">
			<div class="listing-item-content center">
				<h3>{{ $perawat->nama }}</h3>
			</div>
		</div>
		<div class="center perawat-btn">
			{{--@if (Auth::check())
				<button type="button" class="button margin-top-10 margin-bottom-10" onclick="window.location.href='{{url('/pemesanan-perawat/'.$perawat->id)}}'">Pesan</button>
			@else
				<button class="button margin-top-10 margin-bottom-10 sign-in popup-with-zoom-anim" href="#sign-in-dialog">Pesan</button>
			@endif--}}
			
			<button class="button detail margin-top-10 margin-bottom-10 sign-in popup-with-zoom-anim" href="#perawat-dialog{{$perawat->id}}" class="">Detail</button>
		</div>
	</a>
</div>