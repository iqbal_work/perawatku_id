@extends('layout-user.layout')

@section('menu4')
    class="current"
@stop

@section('desc')
	@if(empty($blogs->meta))
		{!! str_limit($blogs->konten, 160) !!}...
	@else
		{{ $blogs->meta }}
	@endif
@stop

@section('image')
{{asset('/images/'.$blogs->gambar)}}
@stop

@section('title')
{{ $blogs->judul }}
@stop
    
@section('content')

<div class="clearfix"></div>

<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12 center">
				<h2>{{ $blogs->judul }}</h2>
			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">

	<!-- Blog Posts -->
	<div class="blog-page">
			<div class="row">


				<!-- Post Content -->
				<div class="col-lg-9 col-md-8 padding-right-30">


					<!-- Blog Post -->
					<div class="blog-post single-post">
						
						<!-- Img -->
						<img class="post-img" src="{{asset('/images/'.$blogs->gambar)}}" alt="{{ $blogs->judul }}">

						
						<!-- Content -->
						<div class="post-content">

							<ul class="post-meta">
								<li>Admin</li>
								<li>{{ $blogs->created_at->format('d-m-Y') }}</li>
								<li><a href="#">5 Komentar</a></li>
							</ul>

							{!! $blogs->konten !!}

							<!-- Share Buttons -->
							<!-- <ul class="share-buttons margin-top-40 margin-bottom-0">
								<li><a class="fb-share" href="#"><i class="fa fa-facebook"></i> Share</a></li>
								<li><a class="twitter-share" href="#"><i class="fa fa-twitter"></i> Tweet</a></li>
								<li><a class="gplus-share" href="#"><i class="fa fa-google-plus"></i> Share</a></li>
							</ul> -->
							<div class="clearfix"></div>

						</div>
					</div>
					<!-- Blog Post / End -->

					<!-- <section class="comments">
						<h4 class="headline margin-bottom-35">Komentar <span class="comments-amount">(5)</span></h4>

						<ul>
							<li>
								<div class="avatar"><img src="{{asset('images/icons/avatar.jpg')}}" alt="Muhammad Iqbal" /></div>
								<div class="comment-content"><div class="arrow-comment"></div>
									<div class="comment-by">Muhammad Iqbal<span class="date">22 August 2017</span></div>
									<p>Morbi velit eros, sagittis in facilisis non, rhoncus et erat. Nam posuere tristique sem, eu ultricies tortor imperdiet vitae. Curabitur lacinia neque non metus</p>
								</div>
							</li>
							<li>
								<div class="avatar"><img src="{{asset('images/icons/avatar.jpg')}}" alt="Benarivo" /> </div>
								<div class="comment-content"><div class="arrow-comment"></div>
									<div class="comment-by">Benarivo<span class="date">18 August 2017</span></div>
									<p>Commodo est luctus eget. Proin in nunc laoreet justo volutpat blandit enim. Sem felis, ullamcorper vel aliquam non, varius eget justo. Duis quis nunc tellus sollicitudin mauris.</p>
								</div>
							</li>
						 </ul>

					</section>
					
					<div class="clearfix"></div>

					<div id="add-review" class="add-review-box">

						<h3 class="listing-desc-headline margin-bottom-35">Tambah Komentar</h3>
									
						<form id="add-comment" class="add-comment">
							<fieldset>

								<div>
									<label>Isi Komentar:</label>
									<textarea cols="40" rows="3"></textarea>
								</div>

							</fieldset>

							<button class="button">Kirim Komentar</button>
							<div class="clearfix"></div>
						</form>

					</div> --> 
			
			</div>
			
			<div class="col-lg-3 col-md-4">
				<div class="sidebar right">

					<!-- <div class="widget">
						<h3 class="margin-top-0 margin-bottom-25">Search Blog</h3>
						<div class="search-blog-input">
							<div class="input"><input class="search-field" type="text" placeholder="Type and hit enter" value=""/></div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="widget margin-top-40">
						<h3>Got any questions?</h3>
						<div class="info-box margin-bottom-10">
							<p>Having any questions? Feel free to ask!</p>
							<a href="pages-contact.html" class="button fullwidth margin-top-20"><i class="fa fa-envelope-o"></i> Drop Us a Line</a>
						</div>
					</div> -->

					<div class="widget margin-top-40">

						<h3>Artikel Lainnya</h3>
						<ul class="widget-tabs">
						@foreach($listBlog as $blogside)
							<li>
								<div class="widget-content">
										<div class="widget-thumb">
										<a href="{{url('blog/'.str_replace(' ', '-',$blogside->judul))}}"><img src="{{asset('/images/'.$blogside->gambar)}}" alt="{{ $blogside->judul }}"></a>
									</div>
									
									<div class="widget-text">
										<h5><a href="{{url('blog/'.str_replace(' ', '-',$blogside->judul))}}">{{ str_limit($blogside->judul, 30) }}</a></h5>
										<span>{{ $blogside->created_at->format('d-m-Y') }}</span>
									</div>
									<div class="clearfix"></div>
								</div>
							</li>
						@endforeach
						</ul>

					</div>
					
					<!-- <div class="widget margin-top-40">
						<h3 class="margin-bottom-25">Social</h3>
						<ul class="social-icons rounded">
							<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
							<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
							<li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
							<li><a class="linkedin" href="#"><i class="icon-linkedin"></i></a></li>
						</ul>

					</div> -->

					<div class="clearfix"></div>
					<div class="margin-bottom-40"></div>
				</div>
			</div>
		</div>

	</div>
</div>

@stop