@extends('layout-user.layout')

@section('menu2')
    class="current"
@stop

@section('desc')
    Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
    {{asset('images/logo/favicon-96.png')}}
@stop

@section('title')
    Perawatan Luka
@stop

@section('content')

    <div class="clearfix"></div>

    <!-- Slider
    ================================================== -->
    <div class="listing-slider margin-bottom-0" style="background-image: url({{asset('/images/rumah-sakit/rs6.jpg')}}); background-repeat: no-repeat; background-position: 50%; background-size: cover;">
    </div>


    <!-- Content
    ================================================== -->
    <div class="container">
        <div class="row sticky-wrapper">
            <div class="col-lg-8 col-md-8 padding-right-30">

                <!-- Titlebar -->
                <div id="titlebar" class="listing-titlebar">
                    <div class="listing-titlebar-title">
                        <h2>Perawatan Luka</h2>
                    </div>
                </div>

                <div id="rs-deskripsi" class="listing-section margin-bottom-25">

                    <h3 class="listing-desc-headline margin-bottom-20">Deskripsi</h3>

                    <p>
                        Luka merupakan terputusnya jaringan kulit atau otot akibat cidera ataupun penyakit. Umunya luka dapat disebabkan oleh cidera atau kecelakaan, tekanan mekanaik seperti terlalu lama perawatan, operasi ataupun tindakan pembedahan, perubahan suhu terlalu ekstrim seperti luka bakar dan luka akibat penyakit vaskular non-degerneratif seperti kanker ataupun diabetes meilitus.
                    </p>
                    <p>
                        <strong>Klasifikasi Luka</strong><br>
                        Luka dapat diklasifikasikan menjadi dua macam, yaitu luka akut dan luka kronik.<br><br>

                        <strong>Luka Akut</strong> yang terjadi mendadak dalam waktu dekat dimana penyembuhan luka tersebut dapat diprediksikan. Contoh luka akut seperti  luka kecelakaan, trauma, ataupun luka setelah operasi.<br><br>

                        <strong>Luka Kronik</strong> merupakan luka yang dapat terjadi akibat gagalnya penyembuhan penyakit dan dalam waktu yang lama. Luka ini dapat dicontohkan seperti luka diabetes, luka kanker ataupun luka tekan akibata bed rest selama perawata.<br><br>

                        <strong>Penyembuhan Luka</strong><br>
                        Setiap kejadian luka, mekanisme tubuh akan mengupayakan mengembalikan komponen-komponen jaringan yang rusak tersebut dengan membentuk struktur baru dan fungsional sama dengan keadaan sebelumnya. Proses penyembuhan tidak hanya terbatas pada proses regenerasi yang bersifat lokal, tetapi juga sangat dipengaruhi oleh faktor endogen (seperti: umur, nutrisi, imunologi, pemakaian obat-obatan, kondisi metabolik).
                    </p>
                    <p>Tubuh yang sehat mempunyai kemampuan alami untuk melindungi dan memulihkan dirinya. Peningkatan aliran darah ke daerah yang rusak, membersihkan sel dan benda asing dan perkembangan awal seluler bagian dari proses penyembuhan. Proses penyembuhan terjadi secara normal tanpa bantuan, walaupun beberapa bahan perawatan dapat membantu untuk mendukung proses penyembuhan. Sebagai contoh, melindungi area yang luka bebas dari kotoran dengan menjaga kebersihan membantu untuk meningkatkan penyembuhan jaringan.</p>

                </div>
            </div>
            <div class="col-lg-4 col-md-4 margin-top-50 sticky">
                @if (Auth::check())
                    <a href="{{url('/layanan/perawatan-luka/pesan')}}" class="button book-now fullwidth">Pesan Layanan</a>
                @else
                    <a href="#sign-in-dialog" class="button book-now fullwidth sign-in popup-with-zoom-anim">Pesan Layanan</a>
                @endif
            </div>

        </div>
    </div>

@stop
