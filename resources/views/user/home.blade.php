@extends('layout-user.layout')

@section('menu1')
    class="current"
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
{{asset('images/logo/favicon-96.png')}}
@stop

@section('title')
Jasa Layanan Perawat yang Datang ke Rumah
@stop

@section('js')
<script type="text/javascript" src="{{asset('js/slider/background.cycle.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".main-search-container").backgroundCycle({
			imageUrls: [
			'images/slider/slide1.jpg',
			'images/slider/slide2.jpg',
			'images/slider/slide3.jpg',
			'images/slider/slide5.jpg'
			],
			fadeSpeed: 2000,
			duration: 5000,
			backgroundSize: SCALING_MODE_COVER
		});
	});
</script>
@stop
    
@section('content')

<div class="main-search-container">
	<div class="main-search-inner">

		<div class="container">
			
			<div class="row">
				<div class="col-md-6" id="hero-text">
					<h2 class="margin-top-0">Dapatkan Perawat ke Rumahmu</h2>
					<h4>Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.</h4>

					<a href="{{url('/layanan')}}" class="button margin-top-25" id="btn-layanan-landing">Pesan Layanan</a>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- Info Section -->
<div class="container margin-top-50 margin-bottom-50">

	{{--<div class="row margin-bottom-30">
		<div class="col-md-12" id="video">
			<iframe src="https://www.youtube.com/embed/mH9BxQmxNXE?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>
	</div>--}}

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2 class="headline centered">
				Layanan yang Kami Miliki
			</h2>
		</div>
	</div>

	<div class="row icons-container">
		<!-- Stage -->
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="icon-box-2 with-line">
				<a href="{{url('layanan/perawatan-bayi')}}"><i><img src="{{asset('images/icons/baby.png')}}"></i>
					<h3>Perawatan Bayi</h3></a>
			</div>
		</div>

		<!-- Stage -->
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="icon-box-2 with-line">
				<a href="{{url('layanan/perawatan-luka')}}"><i><img src="{{asset('images/icons/luka.png')}}"></i>
					<h3>Perawatan Luka</h3></a>
			</div>
		</div>

		<!-- Stage -->
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="icon-box-2 with-line">
				<a href="{{url('layanan/perawatan-medis')}}"><i><img src="{{asset('images/icons/medis.png')}}"></i>
					<h3>Perawatan Medis</h3></a>
			</div>
		</div>

		<!-- Stage -->
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="icon-box-2">
				<a href="{{url('layanan/perawatan-paliatif')}}"><i><img src="{{asset('images/icons/non-medis.png')}}"></i>
					<h3>Perawatan Paliatif</h3></a>
			</div>
		</div>
	</div>

</div>
<!-- Info Section / End -->

<!-- <div class="parallax margin-top-50" data-background="images/slider/slide5.jpg" data-color="#36383e" data-color-opacity="0.6"
	data-img-width="800" data-img-height="505">

	<div class="text-content white-font">
		<div class="container">

			<div class="row">
				<div class="col-lg-6 col-sm-8">
					<h2>Perawatku.id</h2>
					<p>Perawatku.id adalah</p>
					<a href="#" class="button margin-top-25">Cari Perawat</a>
				</div>
			</div>

		</div>
	</div>

</div> -->

<!-- Fullwidth Section -->
<section id="traction" class="fullwidth padding-top-60 padding-bottom-60" data-background-color="#71a7f8">

	<div class="container">

		<div class="row">

			<div class="col-md-12">
				<h2 class="headline centered margin-bottom-30">Perjalanan Perawatku.id</h2>
			</div>

			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="icon-box-2">
					<span>2</span>
					<h3>Rumah Sakit</h3>
				</div>
			</div>

			<!-- Stage -->
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="icon-box-2">
					<span>249</span>
					<h3>Perawat</h3>
				</div>
			</div>

			<!-- Stage -->
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="icon-box-2">
					<span>45</span>
					<h3>Pasien</h3>
				</div>
			</div>

			<!-- Stage -->
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="icon-box-2">
					<span>4</span>
					<h3>Stakeholder</h3>
				</div>
			</div>
		</div>

	</div>

</section>

<div class="container margin-bottom-20 margin-top-50">
	<div class="row">

		<div class="col-md-12">
			<h2 class="headline centered margin-bottom-20">Berkolaborasi dengan</h2>
		</div>
		
		<!-- Carousel -->
		<div class="col-md-12">
			<div class="logo-slick-carousel dot-navigation">
				
				<div class="item">
					<img src="{{asset('images/collab/collab1.jpg')}}" alt="">
				</div>
				
				<div class="item">
					<img src="{{asset('images/collab/collab2.jpg')}}" alt="">
				</div>
				
				<div class="item">
					<img src="{{asset('images/collab/collab3.jpg')}}" alt="">
				</div>
				
				<div class="item">
					<img src="{{asset('images/collab/collab4.jpg')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/collab/collab5.jpg')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/collab/collab6.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/collab/collab7.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/collab/collab8.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/collab/collab9.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/collab/collab10.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/collab/collab11.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/collab/collab12.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/collab/collab13.png')}}" alt="">
				</div>

			</div>
		</div>

	</div>

	<div class="row">

		<div class="col-md-12">
			<h2 class="headline centered margin-top-30 margin-bottom-20">Diliput oleh</h2>
		</div>

		<!-- Carousel -->
		<div class="col-md-12">
			<div class="logo-slick-carousel dot-navigation">

				<div class="item">
					<img src="{{asset('images/featuring/feat1.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/featuring/feat2.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/featuring/feat3.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/featuring/feat4.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/featuring/feat5.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/featuring/feat6.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/featuring/feat7.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/featuring/feat8.png')}}" alt="">
				</div>

				<div class="item">
					<img src="{{asset('images/featuring/feat9.png')}}" alt="">
				</div>

			</div>
		</div>

	</div>
</div>

<section id="blog" class="fullwidth padding-top-50 padding-bottom-50" data-background-color="#f9f9f9">
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<h3 class="headline centered margin-bottom-50">
					Artikel Perawatku
				</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="simple-slick-carousel dots-nav">

					@foreach($blog as $blogs)
					<div class="carousel-item">
						<a href="{{url('blog/'.str_replace(' ', '-',$blogs->judul))}}" class="listing-item-container compact">
							<div class="listing-item">
								<img src="{{asset('/images/'.$blogs->gambar)}}" alt="{{ $blogs->judul }}">
								<div class="listing-item-content">
									<span>{{ $blogs->created_at->format('d-m-Y') }}</span>
									<h3>{{ $blogs->judul }}</h3>
								</div>
							</div>
						</a>
					</div>
					@endforeach

				</div>
				
			</div>

			<div class="col-md-12 centered-content">
				<a href="{{url('/blog')}}" class="button border margin-top-10"><strong>Lihat Lebih Banyak</strong></a>
			</div>

		</div>

		<div class="row">

			<div class="col-md-12">
				<h3 class="headline centered margin-top-45 margin-bottom-45">
					Apa Kata Mereka
				</h3>
			</div>

			<div class="fullwidth-carousel-container margin-top-20">
				<div class="testimonial-carousel testimonials">

					<div class="fw-carousel-review">
						<div class="testimonial-box">
							<div class="testimonial">Pelayanan keperawatan yang berkualitas mempunyai arti bahwa pelayanan yang diberikan kepada individu, keluarga ataupun masyarakat haruslah baik (bersifat etis) dan benar (berdasarkan ilmu dan hukum yang berlaku. Menurut saya dengan adanya layanan homecare yang berada di perawatku.id sangat membantu...</div>
						</div>
						<div class="testimonial-author">
							<img src="{{asset('images/testimoni/testi1.jpg')}}" alt="">
							<h4>Nidya Tiara Sari <span>Mahasiswi</span></h4>
						</div>
					</div>

					<div class="fw-carousel-review">
						<div class="testimonial-box">
							<div class="testimonial">Perawat yang tersedia atau yang hadir ke rumah merupakan perawat yang benar benar berkompeten dan mampu menangani saya untuk mengontrol gula darah saya sesuai dengan SOP yang berlaku dan sama dengan pelayanan di rumah sakit, yang bila mana di bandingkan dengan datang ke rumah sakit mengunakan jasa...</div>
						</div>
						<div class="testimonial-author">
							<img src="{{asset('images/testimoni/testi2.jpg')}}" alt="">
							<h4>Fitri Oktavani <span>Mahasiswi</span></h4>
						</div>
					</div>

					<div class="fw-carousel-review">
						<div class="testimonial-box">
							<div class="testimonial">Pelayanan perawat yang saya pesan datang tepat waktu dan melakukan pekerjaan sesuai dengan yang saya butuhkan. Dan perawat yang dating selalu memberikan respon yang terbaik untuk membantu setiap keluh kesah saya mengenai tekanan darah tinggi yang saya derita, di perawatku.id memberikan pelayanan...</div>
						</div>
						<div class="testimonial-author">
							<img src="{{asset('images/testimoni/testi3.jpg')}}" alt="">
							<h4>Dewi Sulistiani <span>Mahasiswi</span></h4>
						</div>
					</div>

					<div class="fw-carousel-review">
						<div class="testimonial-box">
							<div class="testimonial">Terimakasih perawatku.id sudah memberikan sarana dan fasilitas yang terbaik untuk keluarga saya. Dan penangan yang dilakukan cepat dan tepat sesuai dengan ketentuan kerja. Pelayan rehabilitas dan terapi fisik yang dilakkan sangat membantu keluarga saya yang terkena musibah kecelakaan dan kebutuhan...</div>
						</div>
						<div class="testimonial-author">
							<img src="{{asset('images/testimoni/testi4.jpg')}}" alt="">
							<h4>Ria Ayu Lestari <span>Mahasiswi</span></h4>
						</div>
					</div>

				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-md-12">
				<h3 class="headline centered margin-top-45 margin-bottom-45">
					Instagram Perawatku
				</h3>
			</div>

			<div class="col-md-12">
				<!-- GrahamSnaps Widget Embed -->
				<div id="gs-grid" class="gs-wsmOslTz8GiVBg"></div>
				<script>(function(a,b,c,d,e){if(!(e in a)){a.gs=function(){a.gs.q.push(arguments);};a.gs.q=[];}var f=b.createElement(c);f.src=d;f.async=!0;var g=b.getElementsByTagName(c)[0];g.parentNode.insertBefore(f,g);})(window,document,"script","https://cdn.grahamsnaps.com/js/grid.js","gs");gs("Grid", "wsmOslTz8GiVBg");</script>
			</div>

		</div>

	</div>
</section>

@stop

@section('js')

	<script>
        function send() {
            window.location='https://api.whatsapp.com/send?phone=+6281807977699&text=tes';
        }
	</script>

@stop