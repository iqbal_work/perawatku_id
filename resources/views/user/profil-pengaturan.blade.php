@extends('layout-user.layout')

@section('title')
Pengaturan
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
{{asset('images/logo/favicon-96.png')}}
@stop
    
@section('content')

<?php  $gambar = ''; ?>

<div class="clearfix"></div>

<!-- Content
================================================== -->
<div class="container margin-top-50">
	<div class="row sticky-wrapper">


		@include('layout-user.sidebar')

		<!-- Content
		================================================== -->
		<div class="col-lg-8 col-md-8 padding-left-30">
		
			<!-- Reviews -->
			<div id="listing-reviews" class="listing-section">
				<h3 class="margin-top-0 margin-bottom-20">Pengaturan Akun</h3>

				<div class="clearfix"></div>

				<div class="row">

					<!-- Profile -->
					<div class="col-lg-12 col-md-12 margin-bottom-30">

						@if (empty($user->nomor_ktp))
							<div class="alert alert-danger">
								<span>Mohon lengkapi data pribadi terlebih dahulu.</span>
							</div>
						@endif

						<div class="dashboard-list-box margin-top-0">
						 <form method="post" action="" enctype="multipart/form-data">	
						 	@csrf 	
						 	@if ($user != null)
						 		@method('PATCH')
						 	@endif

							<h4 class="gray margin-top-15">Detail Informasi</h4>

							<div class="dashboard-list-box-static">

									<?php 
										if ($user != null) {
											$gambar = $user->foto;
										}
										else{
											$gambar = "icons/avatar.jpg";
										}
									?>
								<div class="edit-profile-photo">
									<img id="imagecrop" src="{{asset('images/'.$gambar)}}" alt="">
									<div class="change-photo-btn">
										<div class="photoUpload">
										    <span><i class="fa fa-upload"></i> Upload Photo</span>
										    <input type="file"  class="upload" id="inputImage" name="foto" accept=".jpg,.jpeg,.png" value="">
										</div>
									</div>
								</div>
			
								<!-- Details -->
								<div class="my-profile">
									
									<label>Nama Lengkap</label>
									<input name="nama" type="text" @if($user != null) value= "{{$user->nama}}" @endif placeholder="Nama Lengkap" required="required">

									<label>Nomor HP</label>
									<input name="nomor_hp" @if($user != null) value= "{{$user->nomor_hp}}" @endif  type="text" placeholder="Nomor HP" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" required="required">

									<label>Alamat</label>
									<input name="alamat" type="text"  @if($user != null) value="{{$user->alamat}}" @endif required="required" placeholder="Alamat">

									<label>Nomor KTP</label>
									<input name="nomor_ktp" @if($user != null) value="{{$user->nomor_ktp}}" @endif  type="text" placeholder="Nomor KTP" required="required">

								</div>
			
								<div class="form-row center">
									<button type ="submit" class="button margin-top-15">Simpan Data</button>
								</div>

							</div>
							</form>
						</div>
					</div>

					<!-- Change Password -->
					<div class="col-lg-12 col-md-12 margin-bottom-30">
						<div class="dashboard-list-box margin-top-0">
							<h4 class="gray">Ubah Password</h4>
							<div class="dashboard-list-box-static">

								@if(Session::get('errorpass') != null)		
									<div class="notification error closeable">
										<p><span>{{Session::get('errorpass')}}</span></p>
										<a class="close"></a>
									</div>
							    @endif	

							    @if(Session::get('successpass') != null)		
									<div class="notification success closeable">
										<p><span>{{Session::get('successpass')}}</span></p>
										<a class="close"></a>
									</div>
							    @endif	
								
								@include('user.errors')
								
							   	<form action="{{url('/profil/pengaturan/ubah-password')}}" method="POST">
									@csrf
									<div class="my-profile">
										<label class="margin-top-0">Password Lama</label>
										<input type="password" name="old-pass" required>

										<label>Password Baru</label>
										<input type="password" name="password" required>

										<label>Konfirmasi Password Baru</label>
										<input type="password" name="password_confirmation" required>

										<div class="form-row center">
											<button type="submit" class="button margin-top-15">Ubah Password</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
			</div>


		</div>

	</div>
</div>

@stop