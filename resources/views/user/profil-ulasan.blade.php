@extends('layout-user.layout')

@section('title')
Ulasan
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
{{asset('images/logo/favicon-96.png')}}
@stop
    
@section('content')

<div class="clearfix"></div>

<!-- Content
================================================== -->
<div class="container margin-top-50">
	<div class="row sticky-wrapper">


		@include('layout-user.sidebar')

		<!-- Content
		================================================== -->
		<div class="col-lg-8 col-md-8 padding-left-30">
		
			<!-- Reviews -->
			<div id="listing-reviews" class="listing-section">
				<h3 class="margin-top-0 margin-bottom-20">Daftar Ulasan</h3>

				<div class="clearfix"></div>

				<!-- Reviews -->
				<section class="comments listing-reviews">
					<ul>
						@foreach ($review as $reviews)
							@include ('user.list-review')
						@endforeach
					 </ul>
				</section>

				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12">
						<!-- Pagination -->
						<div class="pagination-container margin-top-20 margin-bottom-40">
							<nav class="pagination">
								{{$review->links()}}
							</nav>
						</div>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>

@stop