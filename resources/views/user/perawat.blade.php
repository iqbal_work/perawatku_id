@extends('layout-user.layout')

@section('menu3')
    class="current"
@stop

@section('image')
{{asset('images/logo/favicon-96.png')}}
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('title')
Daftar Perawat
@stop
    
@section('content')

@inject('counter', 'App\Perawat')
<?php 
	$perawat = $counter::where('user_id', 1)->get();
?>

<div class="clearfix"></div>

<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12 center">
				<h2>Daftar Perawat</h2>
				<span>{{ count($perawat) }} perawat telah terdaftar</span>
			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->
<div class="container">
	<div class="row">
		
		<!-- Search -->
		{{--<form method="POST" action="">
			@csrf
			<div class="col-md-12">
				<div class="main-search-input margin-top-0 margin-bottom-50">

					<div class="main-search-input-item">
						<select data-placeholder="Pilih Kota" class="chosen-select" name="kota">
							<option label="Pilih Kota"></option>	
							<option value="Jakarta">Jakarta</option>
						</select>
					</div>

					<div class="main-search-input-item">
						<select data-placeholder="Pilih Layanan" class="chosen-select" name="layanan">
							<option label="Pilih Layanan"></option>
							<option value="Home Visit">Home Visit</option>
							<option value="Paket - Homecare 8 Jam Kerja Per Hari">Paket - Homecare 8 Jam Kerja Per Hari</option>
							<option value="Paket - Homecare 12 Jam Kerja Per Hari">Paket - Homecare 12 Jam Kerja Per Hari</option>
							<option value="Paket - Homecare Standby 24 Jam">Paket - Homecare Standby 24 Jam</option>
							<option value="Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi">Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi</option>
							<option value="Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi">Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi</option>
							<option value="Perawatan Pasien Lansia">Perawatan Pasien Lansia</option>
						</select>
					</div>

					<button type="submit" class="button">Cari</button>
				</div>
			</div>
		</form>--}}
		
		<!-- Search Section / End -->

		<!-- <div class="col-md-6">

			<div class="margin-top-0 margin-bottom-30">

				<div class="fullwidth-filters">

					<div class="sort-by">
						<div class="sort-by-select">
							<select data-placeholder="Urutkan" class="chosen-select-no-single">
								<option>Urutkan</option>	
								<option>Rating tertinggi</option>
								<option>Terbanyak dilihat</option>
								<option>Baru terdaftar</option>
								<option>Banyaknya perawat</option>
							</select>
						</div>
					</div>

				</div>

			</div>

		</div> -->

		<div class="col-md-12" id="perawat">
			<div class="row">
				@if(!empty($perawats->id))
					<h1 class="center">No result available, please try another city/services</h1>
				@else
					@foreach($perawats as $perawat)
						@include('user.list-perawat2')
					@endforeach
				@endif

			</div>

			<!-- Pagination -->
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12">
					<!-- Pagination -->
					<div class="pagination-container margin-top-20 margin-bottom-40">
						<nav class="pagination">
							{{$perawats->links()}}
						</nav>
					</div>
				</div>
			</div>
			<!-- Pagination / End -->

		</div>

	</div>
</div>

@foreach ($perawats as $perawatt)
	@include('user.detail-perawat')
@endforeach

@stop