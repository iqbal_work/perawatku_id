@extends('layout-user.layout')

@section('title')
Pasien
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
{{asset('images/logo/favicon-96.png')}}
@stop
    
@section('content')

<div class="clearfix"></div>

<!-- Content
================================================== -->
<div class="container margin-top-50">
	<div class="row sticky-wrapper">


		@include('layout-user.sidebar')


		<!-- Content
		================================================== -->
		<div class="col-lg-8 col-md-8 padding-left-30">

			<h3 class="margin-top-0 margin-bottom-0">Data Pasien</h3>

			<!-- Listings Container -->
			<div class="col-md-12 margin-bottom-50">

				<div id="data-pasien">

					<h5 class="margin-top-50 margin-bottom-20"><strong>Pasien Orang Lain/Manula</strong></h5>

					<div class="row">

						<form method="post" action="" enctype="multipart/form-data">
						@csrf
						@if(!empty($pasien))
							@method('PATCH')
							<input type="hidden" name="id" value="{{$pasien->id}}">
						@endif
						<div class="col-md-6">
							<input id="nama-pasien" name="nama-pasien" type="text" value="@if($pasien != null) {{$pasien->nama_pasien}} @endif" placeholder="Nama Pasien" required>
						</div>

						<div class="col-md-6">
							<input id="alamat-pasien" name="alamat-pasien" type="text" value="@if($pasien != null) {{$pasien->alamat_pasien}} @endif" placeholder="Alamat Pasien" required>
						</div>

						<div class="col-md-6">
							<input id="email-pasien" name="email-pasien" type="text" value="@if($pasien != null) {{$pasien->email}} @endif" placeholder="Email" required>
						</div>

						<div class="col-md-6">
							<input id="telepon-pasien" name="telepon-pasien" type="text" value="@if($pasien != null) {{$pasien->nomor_hp}} @endif" placeholder="Nomor Telepon" required>
						</div>

						<div class="form-row center">
							<button type="submit" class="button margin-top-15 margin-bottom-20">Simpan Data</button>
						</div>

						</form>

					</div>

				</div>

				<div id="data-bayi">

					<h5 class="margin-top-30 margin-bottom-20"><strong>Pasien Bayi</strong></h5>

					<div class="row">

						<form method="post" action="" enctype="multipart/form-data">
						@csrf
						@if(!empty($bayi))
							@method('PATCH')
							<input type="hidden" name="id" value="{{$bayi->id}}">
						@endif
						<div class="col-md-6">
							<input id="nama-bayi" name="nama-bayi" type="text" value="@if(!empty($bayi)) {{$bayi->nama_pasien}} @endif" placeholder="Nama Bayi" required>
						</div>

						<div class="col-md-6">
							<input id="alamat-bayi" name="alamat-bayi" type="text" value="@if(!empty($bayi)) {{$bayi->alamat_pasien}} @endif" placeholder="Alamat Bayi" required>
						</div>

						<div class="col-md-6">
							<input id="bb-bayi" name="bb-bayi" type="number" value="@if(!empty($bayi)) {{$bayi->berat_badan}} @endif" placeholder="Berat Badan" required>
						</div>

						<div class="col-md-6">
							<input id="tb-bayi" name="tb-bayi" type="number" value="@if(!empty($bayi)) {{$bayi->tinggi_badan}} @endif" placeholder="Tinggi Badan" required>
						</div>

						<div class="col-md-6">
							<select data-placeholder="Umur" class="chosen-select" id="umur" required>
								<option label="Umur"></option>
								<option value="< 1 tahun" @if(!empty($bayi) && $bayi->umur == "< 1 tahun") selected @endif>< 1 tahun</option>
								<option value="1 tahun" @if(!empty($bayi) && $bayi->umur == "1 tahun") selected @endif>1 tahun</option>
								<option value="2 tahun" @if(!empty($bayi) && $bayi->umur == "2 tahun") selected @endif>2 tahun</option>
								<option value="3 tahun" @if(!empty($bayi) && $bayi->umur == "3 tahun") selected @endif>3 tahun</option>
								<option value="4 tahun" @if(!empty($bayi) && $bayi->umur == "4 tahun") selected @endif>4 tahun</option>
							</select>
						</div>

						<div class="col-md-6">
							<select data-placeholder="Suku" class="chosen-select" id="suku" required>
								<option label="Suku"></option>
								<option value="Batak" @if(!empty($bayi) && $bayi->suku == "Batak") selected @endif>Batak</option>
								<option value="Minang" @if(!empty($bayi) && $bayi->suku == "Minang") selected @endif>Minang</option>
								<option value="Jawa" @if(!empty($bayi) && $bayi->suku == "Jawa") selected @endif>Jawa</option>
								<option value="Chinese" @if(!empty($bayi) && $bayi->suku == "Chinese") selected @endif>Chinese</option>
								<option value="Lainnya" @if(!empty($bayi) && $bayi->suku == "Lainnya") selected @endif>Lainnya</option>
							</select>
						</div>

						<div class="col-md-6">
							<select data-placeholder="Agama" class="chosen-select" id="agama" required>
								<option label="Agama"></option>
								<option @if(!empty($bayi) && $bayi->agama == "Islam") selected @endif>Islam</option>
								<option @if(!empty($bayi) && $bayi->agama == "Kristen") selected @endif>Kristen</option>
								<option @if(!empty($bayi) && $bayi->agama == "Budha") selected @endif>Budha</option>
								<option @if(!empty($bayi) && $bayi->agama == "Hindu") selected @endif>Hindu</option>
								<option @if(!empty($bayi) && $bayi->agama == "Lainnya") selected @endif>Lainnya</option>
							</select>
						</div>

						<div class="col-md-6">
							<select data-placeholder="Kondisi Medis" class="chosen-select" id="kondisi-medis" name="kondisi-medis">
								<option label="Kondisi Medis"></option>
								<option value="Berat Badan lahir rendah" @if(!empty($bayi) && $bayi->kondisi_medis == "Bayi Berat Badan lahir rendah (BBLR)") selected @endif>Bayi Berat Badan lahir rendah (BBLR)</option>
								<option value="Down Syndrome" @if(!empty($bayi) && $bayi->kondisi_medis == "Bayi Down Syndrome") selected @endif >Bayi Down Syndrome</option>
								<option value="Labioskizis/Labiopalatokisizis" @if(!empty($bayi) && $bayi->kondisi_medis == "Labioskizis/Labiopalatokisizis") selected @endif>Labioskizis/Labiopalatokisizis</option>
								<option value="Hirschprung" @if(!empty($bayi) && $bayi->kondisi_medis == "Hirschprung") selected @endif>Hirschprung</option>
								<option value="Hidrosefalus" @if(!empty($bayi) && $bayi->kondisi_medis == "Hidrosefalus") selected @endif>Hidrosefalus</option>
								<option value="Penyakit Jantung Bawaan" @if(!empty($bayi) && $bayi->kondisi_medis == "Penyakit Jantung Bawaan (PJB)") selected @endif>Penyakit Jantung Bawaan (PJB)</option>
								<option value="Neonatal Jaundice/Hiperbiliburinemia" @if(!empty($bayi) && $bayi->kondisi_medis == "Neonatal Jaundice/Hiperbiliburinemia") selected @endif>Neonatal Jaundice/Hiperbiliburinemia</option>
							</select>
						</div>

						<div class="form-row center">
							<button type="submit" class="button margin-top-15">Simpan Data</button>
						</div>

						</form>

					</div>

				</div>

			</div>
			
			<div class="clearfix"></div>			

		</div>

	</div>
</div>

@stop