@extends('layout-user.layout')

@section('title')
	Pemesanan Perawat
@stop

@section('desc')
	Perawatku membantu kamu mendapatkan perawat yang bisa langsung datang ke lokasimu.
@stop

@section('image')
	{{asset('images/logo/favicon-96.png')}}
@stop

@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('css/plugins/datedropper.css')}}">
@stop

@section('content')

	<div class="clearfix"></div>

	<div id="titlebar">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="center">Pemesanan Perawat</h2>
				</div>
			</div>
		</div>
	</div>

	<!-- Container -->
	<div class="container" id="user-order">
		<div class="row">
			<form method="POST" action="{{url('/pemesanan-layanan')}}" enctype="multipart/form-data">
			@csrf
			<!-- Content
			================================================== -->
				<div class="col-lg-12 col-md-12">
					@if(!empty($user_detail->id))
						<input type="hidden" name="user_detail_id" value="{{$user_detail->id}}">
					@else
						<input type="hidden" name="user_detail_id" value="{{$user_detail->id}}">
						<input type="hidden" name="user_id" value="{{ $user->id }}">
					@endif

					<h3 class="margin-top-0 margin-bottom-30">Data Pemesan</h3>

					<div class="row">

						<div class="col-md-6">
							@if(!empty($user_detail->nama))
								<input name="nama" type="text" value="{{ $user_detail->nama }}" placeholder="Nama">
							@else
								<input name="nama" type="text" value="{{ $user->name }}" placeholder="Nama">
							@endif
						</div>

						<div class="col-md-6">
							@if(!empty($user_detail->alamat))
								<input name="alamat" type="text" value="{{ $user_detail->alamat }}" placeholder="Alamat">
							@else
								<input name="alamat" type="text" value="" placeholder="Alamat">
							@endif
						</div>

						<div class="col-md-6">
							@if(!empty($user_detail->email))
								<input name="email" type="text" value="{{ $user_detail->email }}" placeholder="Email">
							@else
								<input name="email" type="text" value="{{ $user->email }}" placeholder="Email">
							@endif
						</div>

						<div class="col-md-6">
							@if(!empty($user_detail->hp))
								<input name="hp" type="text" value="{{ $user_detail->nomor_hp }}" placeholder="Nomor HP">
							@else
								<input name="hp" type="text" value="{{ $user->hp }}" placeholder="Nomor HP">
							@endif
						</div>

					</div>

					@if ($pemesanan == 0 || $pemesanan == 2 || $pemesanan == 4)
						<input type="radio" onclick="javascript:pasienCheck();" name="pasien" id="sendiri" value="sendiri" checked> Pemesanan untuk saya sendiri<br>
						<input type="radio" onclick="javascript:pasienCheck();" name="pasien" id="oranglain" value="orang-lain"> Pemesanan untuk orang lain
					@elseif ($pemesanan == 1)
						<input type="radio" onclick="javascript:pasienCheck();" name="pasien" id="bayi" value="bayi" checked> Pemesanan untuk bayi
					@elseif ($pemesanan == 3)
						<input type="radio" onclick="javascript:pasienCheck();" name="pasien" id="manula" value="manula" checked> Pemesanan untuk manula
					@endif

					<div id="data-pasien" style="display: none;">

						<h3 class="margin-top-50 margin-bottom-30">Data Pasien</h3>

						<div class="row">
							<div class="col-md-6">
								<input id="nama-pasien" name="nama-pasien" type="text" value="" placeholder="Nama Pasien" >
							</div>

							<div class="col-md-6">
								<input id="alamat-pasien" name="alamat-pasien" type="text" value="" placeholder="Alamat Pasien" >
							</div>

							<div class="col-md-6">
								<input id="email-pasien" name="email-pasien" type="text" value="" placeholder="Email" >
							</div>

							<div class="col-md-6">
								<input id="telepon-pasien" name="telepon-pasien" type="text" value="" placeholder="Nomor Telepon" >
							</div>
						</div>

					</div>

					@if ($pemesanan == 3)
						<div id="data-manula">

							<h3 class="margin-top-50 margin-bottom-30">Data Pasien</h3>

							<div class="row">
								<div class="col-md-6">
									<input id="nama-pasien" name="nama-pasien" type="text" value="" placeholder="Nama Pasien" >
								</div>

								<div class="col-md-6">
									<input id="alamat-pasien" name="alamat-pasien" type="text" value="" placeholder="Alamat Pasien" >
								</div>

								<div class="col-md-6">
									<input id="email-pasien" name="email-pasien" type="text" value="" placeholder="Email" >
								</div>

								<div class="col-md-6">
									<input id="telepon-pasien" name="telepon-pasien" type="text" value="" placeholder="Nomor Telepon" >
								</div>
							</div>

						</div>
					@endif

					@if ($pemesanan == 1)
						<div id="data-bayi">

							<h3 class="margin-top-50 margin-bottom-30">Data Bayi</h3>

							<div class="row">

								<div class="col-md-4">
									<input id="nama-bayi" name="nama-bayi" type="text" value="" placeholder="Nama Bayi" >
								</div>

								<div class="col-md-4">
									<input id="alamat-bayi" name="alamat-bayi" type="text" value="" placeholder="Alamat Bayi" >
								</div>

								<div class="col-md-4">
									<input id="bb-bayi" name="bb-bayi" type="number" value="" placeholder="Berat Badan (dalam kg)" >
								</div>

								<div class="col-md-4">
									<input id="tb-bayi" name="tb-bayi" type="number" value="" placeholder="Tinggi Badan (dalam cm)" >
								</div>

								<div class="col-md-4">
									<select data-placeholder="Umur" class="chosen-select" id="umur" name="umur">
										<option label="Umur"></option>
										<option value="< 1 tahun">< 1 tahun</option>
										<option value="1 tahun">1 tahun</option>
										<option value="2 tahun">2 tahun</option>
										<option value="3 tahun">3 tahun</option>
										<option value="4 tahun">4 tahun</option>
									</select>
								</div>

								<div class="col-md-4">
									<select data-placeholder="Suku" class="chosen-select" id="suku" name="suku" >
										<option label="Suku"></option>
										<option value="Batak">Batak</option>
										<option value="Minang">Minang</option>
										<option value="Jawa">Jawa</option>
										<option value="Chinese">Chinese</option>
										<option value="Lainnya">Lainnya</option>
									</select>
								</div>

								<div class="col-md-4">
									<select data-placeholder="Agama" class="chosen-select" id="agama" name="agama">
										<option label="Agama"></option>
										<option value="Islam">Islam</option>
										<option value="Kristen">Kristen</option>
										<option value="Buddha">Buddha</option>
										<option value="Hindu">Hindu</option>
										<option value="Lainnya">Lainnya</option>
									</select>
								</div>

								<div class="col-md-4">
									<select data-placeholder="Kondisi Medis" class="chosen-select" id="kondisi-medis" name="kondisi-medis">
										<option label="Kondisi Medis"></option>
										<option value="Berat Badan lahir rendah ">Bayi Berat Badan lahir rendah (BBLR)</option>
										<option value="Down Syndrome">Bayi Down Syndrome</option>
										<option value="Labioskizis/Labiopalatokisizis">Labioskizis/Labiopalatokisizis</option>
										<option value="Hirschprung">Hirschprung</option>
										<option value="Hidrosefalus">Hidrosefalus</option>
										<option value="Penyakit Jantung Bawaan">Penyakit Jantung Bawaan (PJB)</option>
										<option value="Neonatal Jaundice/Hiperbiliburinemia">Neonatal Jaundice/Hiperbiliburinemia</option>
									</select>
								</div>

							</div>

						</div>
					@endif

					<h3 class="margin-top-50 margin-bottom-30">Pemesanan Layanan</h3>
					<div class="row margin-bottom-15">

						<div class="col-md-2">
							<span>Hari Kunjungan</span>
							<input name="order_date" type="text" id="booking-date" placeholder="Hari kunjungan" data-lang="en" data-large-mode="true" data-large-default="true" data-min-year="2018" data-max-year="2018" data-lock="from" value="Hari Kunjungan">
						</div>

						@if ($pemesanan != 4)
							<div class="col-md-4">
								<span>Pilihan Layanan</span>
								<select name="layanan" id="layanan" onchange="opsi()" data-placeholder="Pilih Layanan" class="chosen-select">
									<option label="Pilih Layanan"></option>
									@foreach($layanan as $lay)
										<option value="{{$lay->id}}">{{$lay->jasa}}</option>
									@endforeach
								</select>
							</div>

							<div class="col-md-4">
								<span>Pilihan Tindakan</span>
								<select name="tindakan" id="tindakan" onchange="opsi()" data-placeholder="Pilih Tindakan" class="chosen-select">
									<option label="Pilih Tindakan"></option>
									<option value="0" selected>- Tanpa Tindakan -</option>
									@foreach($layanan as $lay)
										<option value="{{$lay->id}}">{{$lay->tindakan}}</option>
									@endforeach
								</select>
							</div>

							<input type="hidden" id="tindakanasli" name="tindakanasli">
							<input type="hidden" id="jasaasli" name="jasaasli">
						@elseif ($pemesanan == 4)
							<div class="col-md-8">
								<span>Pilihan Tindakan</span>
								<select name="tindakan" id="tindakan" onchange="opsi2()" data-placeholder="Pilih Tindakan" class="chosen-select" required>
									<option label="Pilih Tindakan"></option>
									@foreach($layanan as $lay)
										@if ($lay->tindakan != "")
											<option value="{{$lay->id}}">{{$lay->tindakan}}</option>
										@endif
									@endforeach
								</select>
							</div>

							<input type="hidden" id="tindakanasli" name="tindakanasli">
						@endif

						<div class="col-md-2 pricing-price">
							<span>Total Biaya</span>
							<input id="biaya" name="biaya" type="text" data-unit="RP" placeholder="Biaya" readonly>
						</div>

					</div>

						<input type="checkbox" name="agree" value="Agree" required> Saya setuju dengan <a href="{{url('/syarat-ketentuan')}}" target="_blank"><u>Syarat dan Ketentuan</u></a> dari Perawatku.id

					@include('user.errors')

					<div class="clear"></div>

					<div class="center">
						<button type="submit" class="button booking-confirmation-btn margin-top-40 margin-bottom-40">Simpan Data</button>
					</div>
				</div>
			</form>

		</div>
	</div>
	<!-- Container / End -->

@stop

@section('js')
	<script src="{{asset('js/datedropper.js')}}"></script>
	<script>$('#booking-date').dateDropper();</script>
	<script>$('#tanggal-lahir').dateDropper();</script>

	<script type="text/javascript">
		var layanan = {!! $layananjs !!};
		console.log(layanan);

        function opsi() {
            var opsi1, opsi2, tambah = 0;
            opsi1 = document.getElementById("layanan").value;
            opsi2 = document.getElementById("tindakan").value;
            tambah = layanan[opsi1]["biaya"] + layanan[opsi2]["biaya"];

            if (opsi2 == 0) {
            	document.getElementById('biaya').value = layanan[opsi1]["biaya"];    
            	document.getElementById('jasaasli').value = layanan[opsi1]["jasa"];      
            }
            else if (opsi1 == 0) {
                document.getElementById('biaya').value = layanan[opsi2]["biaya"];
            	document.getElementById('tindakanasli').value = layanan[opsi2]["tindakan"];
			}
            else {
                document.getElementById('biaya').value = tambah;
                document.getElementById('jasaasli').value = layanan[opsi1]["jasa"];
           		document.getElementById('tindakanasli').value = layanan[opsi2]["tindakan"];
			}
			/*alert(opsi1);*/
		}

		function opsi2() {
            var opsi, total = 0;
            opsi = document.getElementById("tindakan").value;

            if (opsi != 0) {
                document.getElementById('biaya').value = layanan[opsi]["biaya"];
                document.getElementById('tindakanasli').value = layanan[opsi]["tindakan"];
            }

            document.getElementById("layanan").required = false;
		}

        function pasienCheck() {

                if (document.getElementById('sendiri').checked) {
                    document.getElementById('data-pasien').style.display = 'none';
                    document.getElementById("nama-pasien").required = false;
                    document.getElementById("alamat-pasien").required = false;
                    document.getElementById("email-pasien").required = false;
                    document.getElementById("telepon-pasien").required = false;
                }
                else if (document.getElementById('oranglain').checked) {
                    document.getElementById('data-pasien').style.display = 'block';
                    document.getElementById("nama-pasien").required = true;
                    document.getElementById("alamat-pasien").required = true;
                    document.getElementById("email-pasien").required = true;
                    document.getElementById("telepon-pasien").required = true;
                }
                else if (document.getElementById('manula').checked) {
                    document.getElementById('data-pasien').style.display = 'block';
                    document.getElementById("nama-pasien").required = true;
                    document.getElementById("alamat-pasien").required = true;
                    document.getElementById("email-pasien").required = true;
                    document.getElementById("telepon-pasien").required = true;
                }
            	else if (document.getElementById('bayi').checked) {
                    document.getElementById('data-bayi').style.display = 'block';
                    document.getElementById('data-pasien').style.display = 'none';
                    document.getElementById("nama-bayi").required = true;
                    document.getElementById("alamat-bayi").required = true;
                    document.getElementById("bb-bayi").required = true;
                    document.getElementById("tb-bayi").required = true;
                    document.getElementById("umur").required = true;
                    document.getElementById("suku").required = true;
                    document.getElementById("agama").required = true;
                    document.getElementById("kondisi-medis").required = true;
                    document.getElementById("nama-pasien").required = false;
                    document.getElementById("alamat-pasien").required = false;
                    document.getElementById("email-pasien").required = false;
                    document.getElementById("telepon-pasien").required = false;
                }
        }

	</script>
@stop