-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2018 at 08:45 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perawatku`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE `activity_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `activity` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_logs`
--

INSERT INTO `activity_logs` (`id`, `user_id`, `activity`, `created_at`, `updated_at`) VALUES
(1, 1, 'Update Data Perawat : <b>rika agustina</b>', '2018-07-21 03:37:28', '2018-07-21 03:37:28'),
(2, 3, 'Booking Baru dari rivo</br> Layanan: 250000dan Tindakan: 100000', '2018-08-02 12:50:36', '2018-08-02 12:50:36'),
(3, 3, 'Booking Baru dari rivo</br> Layanan: 250000 dan Tindakan: 100000', '2018-08-02 13:53:00', '2018-08-02 13:53:00'),
(4, 3, 'Booking Baru dari rivo</br> Layanan: 250000 dan Tindakan: 100000', '2018-08-02 19:58:55', '2018-08-02 19:58:55'),
(5, 3, 'Booking Baru dari rivo</br> Layanan: 250000 dan Tindakan: ', '2018-08-02 20:47:41', '2018-08-02 20:47:41'),
(6, 3, 'Booking Baru dari rivo</br> Layanan: 400000 dan Tindakan: ', '2018-08-02 20:50:12', '2018-08-02 20:50:12'),
(7, 3, 'Booking Baru dari rivo</br> Layanan: 400000 dan Tindakan: ', '2018-08-12 21:50:34', '2018-08-12 21:50:34'),
(8, 3, 'Booking Baru dari rivo</br> Layanan: 400000 dan Tindakan: ', '2018-08-12 22:00:13', '2018-08-12 22:00:13'),
(9, 3, 'Booking Baru dari rivo</br> Layanan: 300000 dan Tindakan: ', '2018-08-13 07:14:58', '2018-08-13 07:14:58'),
(10, 3, 'Booking Baru dari rivo</br> Layanan: 450000 dan Tindakan: ', '2018-08-13 07:23:35', '2018-08-13 07:23:35'),
(11, 3, 'Booking Baru dari rivo</br> Layanan: 450000 dan Tindakan: ', '2018-08-13 07:27:09', '2018-08-13 07:27:09'),
(12, 3, 'Booking Baru dari rivo</br> Layanan: 800000 dan Tindakan: ', '2018-08-13 07:48:27', '2018-08-13 07:48:27'),
(13, 3, 'Booking Baru dari rivo</br> Layanan: 800000 dan Tindakan: ', '2018-08-13 07:50:09', '2018-08-13 07:50:09'),
(14, 3, 'Booking Baru dari rivo</br> Layanan: 800000 dan Tindakan: ', '2018-08-13 08:15:29', '2018-08-13 08:15:29'),
(15, 3, 'Booking Baru dari rivo</br> Layanan: 9000000 dan Tindakan: ', '2018-08-13 08:16:59', '2018-08-13 08:16:59'),
(16, 3, 'Booking Baru dari rivo</br> Layanan: 450000 dan Tindakan: ', '2018-08-13 08:17:36', '2018-08-13 08:17:36'),
(17, 3, 'Booking Baru dari rivo</br> Layanan: 9000000 dan Tindakan: 150000', '2018-08-13 08:18:19', '2018-08-13 08:18:19'),
(18, 3, 'Booking Baru dari rivo</br> Layanan:  dan Tindakan: 150000', '2018-08-13 08:21:17', '2018-08-13 08:21:17'),
(19, 3, 'Booking Baru dari rivo</br> Layanan: 450000 dan Tindakan: 0', '2018-08-13 08:30:21', '2018-08-13 08:30:21'),
(20, 3, 'Booking Baru dari rivo</br> Layanan: 14000000 dan Tindakan: 0', '2018-08-13 08:30:49', '2018-08-13 08:30:49'),
(21, 3, 'Booking Baru dari rivo</br> Layanan: 9000000 dan Tindakan: 0', '2018-08-13 08:32:39', '2018-08-13 08:32:39'),
(22, 3, 'Booking Baru dari rivo</br> Layanan: 800000 dan Tindakan: 100000', '2018-08-13 08:33:14', '2018-08-13 08:33:14'),
(23, 3, 'Booking Baru dari rivo</br> Layanan: 9000000 dan Tindakan: 0', '2018-08-13 08:33:31', '2018-08-13 08:33:31'),
(24, 3, 'Booking Baru dari rivo</br> Layanan: 300000 dan Tindakan: 100000', '2018-08-13 14:29:07', '2018-08-13 14:29:07'),
(25, 3, 'Booking Baru dari rivo</br> Layanan: 300000 dan Tindakan: 100000', '2018-08-13 14:34:39', '2018-08-13 14:34:39'),
(26, 3, 'Booking Baru dari rivo</br> Layanan: 400000 dan Tindakan: 200000', '2018-08-13 14:37:58', '2018-08-13 14:37:58'),
(27, 3, 'Booking Baru dari rivo</br> Layanan: 400000 dan Tindakan: 0', '2018-08-21 02:49:45', '2018-08-21 02:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta` varchar(160) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `konten` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `user_id`, `judul`, `gambar`, `meta`, `konten`, `created_at`, `updated_at`) VALUES
(1, '1', 'Blog 1', 'blog1.jpg', NULL, 'Blog ini adalah uji coba pertama untuk kemaslahatan umat di bulan ramadhan bro cus meong', '2018-07-20 17:00:00', NULL),
(2, '1', 'asdada', 'blogs/WPKtwWJrgopOc1HMsAUOw72YxQ0TWReP2K6l3i60.png', 'asdasdad', '<p><i><b>asdasdasdasdasd</b></i>\r\n                                	\r\n                                </p>', '2018-08-02 02:59:58', '2018-08-02 02:59:58'),
(3, '1', 'tes 2', 'blogs/oxupTVwxpyqzf2mgZ7NkobIEs3dmqCueBfnt6p1O.png', 'asdasdad', '<p><strong>asdasdasdasdasd</strong></p><p><br></p><p>asdasdasd</p><p><img class=\"fr-fic fr-dib\" src=\"https://i.froala.com/download/c480a860b570754aca7bc165e06d6beffbb28b2a.png?1533205097\" style=\"width: 300px;\"></p><p><br></p><p>asdasdasdasdqweqwe</p>', '2018-08-02 03:19:14', '2018-08-02 03:21:29'),
(4, '1', 'Zxasdasd', 'blogs/NlwVZ4I3pBzDU3LwwxwTOuNI8UDL33n7yNmq2C8f.png', 'asdasd', '<p>poipoipoi</p>\r\n\r\n<figure class=\"easyimage easyimage-side\"><img alt=\"poipoi\" src=\"blob:http://localhost/78189c9e-7c2c-430a-a297-f69e75d78801\" style=\"width:311px\" />\r\n<figcaption>poipoi</figcaption>\r\n</figure>\r\n\r\n<p>asdasdasd</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>asdadsad</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>asdasdasd</p>', '2018-08-20 01:38:14', '2018-08-20 01:41:09');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_transaksi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `owner_user_id` int(11) NOT NULL,
  `perawat_id` int(11) DEFAULT NULL,
  `layanan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tindakan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_order` date NOT NULL,
  `booking_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `payment_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `bank` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biaya` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kodeunik` int(3) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `kode_transaksi`, `user_detail_id`, `owner_user_id`, `perawat_id`, `layanan`, `tindakan`, `tanggal_order`, `booking_status`, `payment_status`, `bank`, `biaya`, `kodeunik`, `created_at`, `updated_at`) VALUES
(1, '1_1', 1, 3, 4, '1', NULL, '2018-06-29', '0', '0', NULL, NULL, NULL, NULL, NULL),
(2, '1_2', 1, 3, 4, '1', NULL, '2018-06-29', '0', '0', NULL, NULL, NULL, NULL, NULL),
(3, '1_3', 1, 3, 4, '1', NULL, '2018-06-29', '0', '0', NULL, NULL, NULL, NULL, NULL),
(4, '1_4', 1, 3, 4, '1', NULL, '2018-06-29', '0', '0', NULL, NULL, NULL, NULL, NULL),
(5, '16_5', 1, 1, 16, '107', NULL, '2018-07-20', '1', '0', NULL, NULL, NULL, '2018-07-20 00:50:02', '2018-07-20 00:50:48'),
(6, '20_6', 1, 1, 20, '137', NULL, '2018-07-21', '1', '0', NULL, NULL, NULL, '2018-07-20 22:29:14', '2018-07-20 22:29:55'),
(35, 'prwt-MKRmg', 17, 1, NULL, '300000', '100000', '2018-08-14', '0', '0', NULL, '400000', NULL, '2018-08-13 14:34:39', '2018-08-13 14:34:39'),
(36, 'prwt-FkY48', 17, 1, NULL, '400000', '200000', '2018-08-14', '1', '1', NULL, '600000', 622, '2018-08-13 14:37:58', '2018-08-13 16:34:27'),
(37, 'prwt-xScvN', 17, 1, NULL, '400000', '0', '2018-08-21', '0', '0', NULL, '400000', 599, '2018-08-21 02:49:45', '2018-08-21 02:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `id` int(10) UNSIGNED NOT NULL,
  `perawat_id` int(11) NOT NULL,
  `layanan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biaya` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fees`
--

INSERT INTO `fees` (`id`, `perawat_id`, `layanan`, `biaya`, `created_at`, `updated_at`) VALUES
(1, 1, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(2, 1, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(3, 1, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(4, 1, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(5, 1, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(6, 1, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(7, 1, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(8, 2, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(9, 2, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(10, 2, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(11, 2, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(12, 2, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(13, 2, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(14, 2, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(15, 3, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(16, 3, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(17, 3, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(18, 3, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(19, 3, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(20, 3, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(21, 3, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(22, 4, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(23, 4, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(24, 4, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(25, 4, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(26, 4, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(27, 4, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(28, 4, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(29, 5, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(30, 5, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(31, 5, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(32, 5, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(33, 5, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(34, 5, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(35, 5, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(36, 6, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(37, 6, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(38, 6, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(39, 6, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(40, 6, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(41, 6, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(42, 6, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(43, 7, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(44, 7, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(45, 7, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(46, 7, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(47, 7, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(48, 7, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(49, 7, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(50, 8, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(51, 8, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(52, 8, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(53, 8, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(54, 8, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(55, 8, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(56, 8, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(57, 9, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(58, 9, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(59, 9, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(60, 9, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(61, 9, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(62, 9, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(63, 9, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(64, 10, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(65, 10, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(66, 10, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(67, 10, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(68, 10, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(69, 10, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(70, 10, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(71, 11, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(72, 11, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(73, 11, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(74, 11, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(75, 11, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(76, 11, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(77, 11, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(78, 12, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(79, 12, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(80, 12, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(81, 12, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(82, 12, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(83, 12, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(84, 12, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(85, 13, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(86, 13, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(87, 13, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(88, 13, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(89, 13, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(90, 13, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(91, 13, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(92, 14, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(93, 14, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(94, 14, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(95, 14, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(96, 14, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(97, 14, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(98, 14, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(99, 15, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(100, 15, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(101, 15, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(102, 15, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(103, 15, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(104, 15, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(105, 15, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(106, 16, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(107, 16, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(108, 16, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(109, 16, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(110, 16, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(111, 16, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(112, 16, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(113, 17, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(114, 17, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(115, 17, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(116, 17, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(117, 17, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(118, 17, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(119, 17, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(120, 18, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(121, 18, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(122, 18, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(123, 18, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(124, 18, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(125, 18, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(126, 18, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(127, 19, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(128, 19, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(129, 19, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(130, 19, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(131, 19, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(132, 19, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(133, 19, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL),
(134, 20, 'Home Visit', 150000, '2018-07-20 17:00:00', NULL),
(135, 20, 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(136, 20, 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan', 150000, '2018-07-20 17:00:00', NULL),
(137, 20, 'Paket - Homecare Standby 24 Jam Selama 1 Bulan', 50000, '2018-07-20 17:00:00', NULL),
(138, 20, 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi', 150000, '2018-07-20 17:00:00', NULL),
(139, 20, 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi', 50000, '2018-07-20 17:00:00', NULL),
(140, 20, 'Perawatan Pasien Lansia', 50000, '2018-07-20 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `layanans`
--

CREATE TABLE `layanans` (
  `id` int(10) NOT NULL,
  `kategori` varchar(191) DEFAULT NULL,
  `jasa` varchar(191) DEFAULT NULL,
  `tindakan` varchar(191) DEFAULT NULL,
  `biaya` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `layanans`
--

INSERT INTO `layanans` (`id`, `kategori`, `jasa`, `tindakan`, `biaya`) VALUES
(1, 'bayi', 'Home Visit', '', 300000),
(2, 'bayi', NULL, 'Perawatan Tali Pusar', 100000),
(3, 'bayi', NULL, 'Pemandian Bayi', 100000),
(4, 'luka', 'Homecare', NULL, 400000),
(5, 'luka', NULL, 'Perawatan Luka Kecil', 150000),
(6, 'luka', NULL, 'Perawatan Luka Sedang', 200000),
(7, 'luka', NULL, 'Perawatan Luka Berat', 250000),
(8, 'luka', NULL, 'Perawatan Kolostomi', 150000),
(9, 'luka', NULL, 'Perawatan Trakeostomi (hanya perawatan)', 150000),
(10, 'medis', 'Home Visit', NULL, 300000),
(11, 'medis', 'Homecare 8 Jam', NULL, 450000),
(12, 'medis', 'Homecare 24 jam', NULL, 800000),
(13, 'medis', NULL, 'Tindakan Kateter', 150000),
(14, 'medis', NULL, 'Tindakan IVGT', 100000),
(15, 'medis', NULL, 'Pemasangan Infus', 150000),
(16, 'medis', NULL, 'Tindakan Suction', 100000),
(17, 'medis', NULL, 'Tindakan Nebulizer (tanpa obat)', 100000),
(18, 'paliatif', 'Visit', NULL, 300000),
(19, 'paliatif', 'Homecare 8 Jam', NULL, 450000),
(20, 'paliatif', 'Homecare 24 jam', NULL, 800000),
(21, 'paliatif', NULL, 'Tindakan Kateter', 150000),
(22, 'paliatif', NULL, 'Tindakan IVGT', 100000),
(23, 'paliatif', NULL, 'Pemasangan Infus', 150000),
(24, 'paliatif', NULL, 'Tindakan Suction', 100000),
(25, 'paliatif', NULL, 'Tindakan Nebulizer (tanpa obat)', 100000),
(26, 'medis', 'Paket 8 Jam/Bulan', NULL, 9000000),
(27, 'medis', 'Paket 24 Jam/Bulan', NULL, 14000000),
(28, 'paliatif', 'Paket 8 Jam/Bulan', NULL, 9000000),
(29, 'paliatif', 'Paket 24 Jam/Bulan', NULL, 14000000);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(43, '2014_10_12_000000_create_users_table', 1),
(44, '2014_10_12_100000_create_password_resets_table', 1),
(45, '2018_04_27_033515_create_rumah_sakits_table', 1),
(46, '2018_04_27_084043_create_perawats_table', 1),
(47, '2018_04_27_090813_create_bookings_table', 1),
(48, '2018_04_27_094309_create_pasiens_table', 1),
(49, '2018_05_04_032356_create_user_details_table', 1),
(50, '2018_05_09_070051_create_user_reviews_table', 1),
(51, '2018_05_09_075126_create_fees_table', 1),
(52, '2018_05_22_165607_create_blogs_table', 1),
(53, '2018_06_03_071644_create_activity_logs_table', 1),
(54, '2018_06_11_183827_create_subscriber_table', 1),
(55, '2018_06_16_063631_create_rekenings_table', 1),
(56, '2018_06_27_154143_create_user_pasiens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pasiens`
--

CREATE TABLE `pasiens` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `tipe_pasien` int(11) NOT NULL,
  `nama_pasien` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat_pasien` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_pasien` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_hp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `berat_badan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tinggi_badan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `umur` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kondisi_medis` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pasiens`
--

INSERT INTO `pasiens` (`id`, `user_id`, `booking_id`, `tipe_pasien`, `nama_pasien`, `alamat_pasien`, `email_pasien`, `nomor_hp`, `berat_badan`, `tinggi_badan`, `umur`, `suku`, `agama`, `kondisi_medis`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 'Iqbal', 'Indonesia', 'mantap@jos.gandos', '081231231', NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-20 17:00:00', NULL),
(2, 2, 2, 2, 'John', 'Indonesia', 'mantap@jos.gandos', '081231231', NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-20 17:00:00', NULL),
(3, 2, 3, 1, 'Iqbal', 'Indonesia', 'mantap@jos.gandos', '081231231', NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-20 17:00:00', NULL),
(4, 2, 4, 2, 'John', 'Indonesia', 'mantap@jos.gandos', '081231231', NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-20 17:00:00', NULL),
(41, 17, 35, 3, 'asd', 'asd', NULL, NULL, '12', '12', '2 tahun', 'Jawa', 'Hindu', 'Labioskizis/Labiopalatokisizis', '2018-08-13 14:34:39', '2018-08-13 14:34:39'),
(42, 17, 36, 2, 'asd', 'asd', 'asd', '12', NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-13 14:37:58', '2018-08-13 14:37:58'),
(43, 17, 37, 1, 'rivo', '123', 'asd@mail.com', '123', NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-21 02:49:45', '2018-08-21 02:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `perawats`
--

CREATE TABLE `perawats` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `domisili` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `umur` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pendidikan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institusi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sertifikasi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mulai_aktif` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pengalaman` text COLLATE utf8mb4_unicode_ci,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_perawatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bersedia` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `perawats`
--

INSERT INTO `perawats` (`id`, `user_id`, `nama`, `alamat`, `domisili`, `jenis_kelamin`, `agama`, `umur`, `telepon`, `email`, `pendidikan`, `institusi`, `sertifikasi`, `foto`, `mulai_aktif`, `pengalaman`, `deskripsi`, `jenis_perawatan`, `bersedia`, `created_at`, `updated_at`) VALUES
(1, 3, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd1@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(2, 3, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd2@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(3, 3, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd3@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(4, 3, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd4@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(5, 3, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd5@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(6, 4, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd6@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(7, 4, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd7@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(8, 4, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd8@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(9, 4, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd9@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(10, 4, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd10@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(11, 5, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd11@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(12, 5, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd12@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(13, 5, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd13@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(14, 5, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd14@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(15, 5, 'John Doe', 'Indoenesia', NULL, NULL, NULL, '12', '0226621234', 'jd15@mail.com', '123123123213', '123444444', 'Cisco', 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png', NULL, '4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', NULL, NULL, '2018-07-20 17:00:00', NULL),
(16, 1, 'Aida Fitri', 'Batipuh', '-', '-', '-', '-', '87895463062', 'aidafitri2606@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 1, 'Alfin Rahmat', 'Jln. Cubadak Ampo, Gang cubadang VI, RT 02, RW 08, Kel. Anduriang, Kec. Kuranji, Padang, Sumatera Barat', '-', '-', '-', '-', '82288293876', 'allfine29@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 1, 'Anisah Khayrani Hasibuan', 'Pasar Ipuh, Kec. Ulu Barumun, Kab. Padang Lawas, Sumatera Utara', '-', '-', '-', '-', '85261721996', 'anisah.khayrani@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 1, 'Arita Copri Cornika', 'Padang Karambia Kec. Sungai Limau, Kab Padang Pariaman', '-', '-', '-', '-', '82174473117', 'aritacopri@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 1, 'Cindy Olivia', 'Koto Nan IV, Kelurahan Pelangai, Kecamatan Ranah Pesisir, Kabupaten Pesisir Selatan', '-', '-', '-', '-', '82174318155', 'cindy.olivia30_42@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 1, 'Delfiandi Rozi', 'Bandar Buat RT 004 RW 003 No 18 Kelurahan Bandar Buat Kecamatan Lubuk Kilangan Padang', '-', '-', '-', '-', '81364318547', 'rozinew21@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 1, 'Deni Putri Wahyu Ningsih', 'Koto Lamo Kec. Kapur IX Kab Lima Puluh Kota', '-', '-', '-', '-', '831845179', 'denipwn@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 1, 'Devita Hadia Nova', 'Komplek Mandala Citra Indah blok F5 No 11, Serang, Banten', '-', '-', '-', '-', '82285210320', 'devitahadi16@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 1, 'Dhea Putri', 'Perum Pondok Jati Mekar No A/9 Padang', '-', '-', '-', '-', '81268256648', 'dheaputri.220493@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 1, 'Dian Junila', 'Simpang Aia Manih Jorong Limo Niniak', '-', '-', '-', '-', '85375115317', 'dian.junila@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 1, 'Dian Khairani', 'Sungai Tarab Batusangkar', '-', '-', '-', '-', '85355635163', 'diankhairani20@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 1, 'Dila Amalina', 'Jorong Bintungan, Nagari Panyalaian, Kec X Koto, Kab Tanah Datar', '-', '-', '-', '-', '85363111314', 'dila1p6y@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 1, 'Eldasswinda', 'Jl. Berlian 2 No 112 Perumnas Pegambiran', '-', '-', '-', '-', '81372281264', 'eldasswinda94@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 1, 'Eldila Purnama Suci', 'Jln Raya Sungai Pua No 77 Kampung Baru Jorong Limo Suku Kecamatan Sungai Pua, Agam', '-', '-', '-', '-', '82392428408', 'eldillapurnamasuci27@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 1, 'Erlin Sayuti', 'Jl. Lapai 1 Blok I No 1 RT/RW 005/004 Kelurahan Kampung Lapai Kec Nanggalo', '-', '-', '-', '-', '81287291452', 'erlin_sayuti@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 1, 'Fauziah', 'simpang Lapau Belek Saniang Baka Kecamatan X Koto Singkarak, Kabupaten Solok', '-', '-', '-', '-', '85263496393', 'fauziah.faqad93@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 1, 'Fina Mutia Yahya', 'Jalan anggur IV No 217 Perumnas Balimbiang Kecamatan Kuranji kelurahan Kuranji Kota Padang', '-', '-', '-', '-', '81363341666', 'finamutia.ft9@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 1, 'Fitri Ramadhona', 'Jl. Ipuh nan Sati No 243 Kecamatan Sungai Tarab Kabupaten Tanah Datar', '-', '-', '-', '-', '83181944112', 'fitridhona94@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 1, 'Fitry Khairiyah', 'Jl Siti Manggopoh No 127 Desa Naras Hilir Kec. Pariaman Utara Kota Pariaman', '-', '-', '-', '-', '85272482135', 'fitrykhairiyah@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 1, 'Hamida Erwinda', 'Jl Moh Hatta No 112 Anduring', '-', '-', '-', '-', '82371878031', 'hamida.erwinda@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 1, 'Hanny Hafiza Naswir', 'Komp Puri Filano Asri Blok AA No 1 RT 003 RW 004 Kelurahan Kubu Dalam Parak Karakah Kecamatan Padang Timur', '-', '-', '-', '-', '85272291507', 'hafiza.hanny@ymail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 1, 'Henny Greviana', 'Jl Syekh  M Jamil No 41 RT 17 Guguk Malintang Padang Panjang', '-', '-', '-', '-', '81374203515', 'hennygreviana@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 1, 'Hidayatul Husna', 'Jorong Banjo Alam, Kenagarian Ampang Gadang, Kecamatan Ampek Angkek Kabupaten Agam', '-', '-', '-', '-', '85264102935', 'hidayatulhusna4693@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 1, 'ikhsan Dwisetyandi', 'RT 02 TW 03 Kel Subarang Beltung', '-', '-', '-', '-', '83180625123', 'indi.fbmania@gail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 1, 'Indahsyari Rezki Situmorang', 'Jl. Kampung Baru, Kelurahan Sioldengan, Kecamatan Rantau Sleatan, kabupaten abuhan Batu, Rantauprapat, Sumatera Utara', '-', '-', '-', '-', '81261151881', 'indah.situmorang@ymail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 1, 'Jefi Hermano', 'limau manis kulam, kec Lengayang, kabupaten pesisir selatan', '-', '-', '-', '-', '83182801049', 'hermanto_jefi@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 1, 'Lia Maharani', 'jln H. rasul telur desa talago sarik kec padang timur', '-', '-', '-', '-', '83181585507', 'maharani77lia@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 1, 'Liza Hadiyati', 'jln. Sudirman no A1, kecamatan sijunjung kabupaten sijunjung', '-', '-', '-', '-', '82283375864', 'liza.hadiyati@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 1, 'Lusi Refni', 'Jl. Kampung Baru, Kelurahan Sioldengan, Kecamatan Rantau Sleatan, kabupaten abuhan Batu, Rantauprapat, Sumatera Utara', '-', '-', '-', '-', '85263112742', 'lusirefni36@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 1, 'ibaadi indra', 'air kijang, nagari VII, kecamatan palupuh kabupaten agam', '-', '-', '-', '-', '85278351506', 'ibaadi.fkua@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 1, 'ika khairunnisa zuardi', 'jl. Banda gadang jorong batu hampar nagari kampung tangah kecamtan lubuk basung kabupaten agam', '-', '-', '-', '-', '85263702577', 'izfams@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 1, 'insana kamala', 'dusun karan desa taluk kecamatan pariaman selatan', '-', '-', '-', '-', '85271135431', 'kelompok.u.nand@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 1, 'jeki refialdinata', 'kerinci, jambi', '-', '-', '-', '-', '85263831567', 'jack_fial@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 1, 'laspita theresiana', 'jln raya km 6 tuapejat kabupaten kepulauan mentawai', '-', '-', '-', '-', '82172228388', 'laspitatheresiana@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 1, 'maria wulzi', 'situjuh gadang kec situjuh limo nagari kab lima puluh kota', '-', '-', '-', '-', '85274570071', 'mary_fujisawa@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 1, 'marlizayani', 'muara siberut kecamatan siberut selatan ka. Kepulauan mentawai', '-', '-', '-', '-', '81363239089', 'lizafaisal09@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 1, 'maya amanda', 'jl. Tabut no 26 kel pasir kec. Pariaman tengah, pariaman', '-', '-', '-', '-', '81267125760', 'mayadarwis@ymail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 1, 'metha yulianti', 'jl. Jati adaiah no 66 rt 01 rw 07 kelurahan jati kec. Padang timur, kota padang, sumatera barat', '-', '-', '-', '-', '85263817968', 'methayulianti14@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 1, 'muhammad saleh', 'jr air runding, kecamatan parit kabpaten pasaman barat', '-', '-', '-', '-', '85272910218', 'mhd.saleh10v@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 1, 'nadiatu mardiah', 'perumnas sidomulyo jl. Garuda raya no 49d RT/RW 01/018 kec. Marpoyan damai kel. Maharatu pekanbaru', '-', '-', '-', '-', '85278107382', 'nadiatulmardiah@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 1, 'nadrah roza', 'pasar bawan kecamatan ampek angkek nagari kabupaten agam', '-', '-', '-', '-', '82388288668', 'nadrah.roza@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 1, 'nofria', 'simpang pertanian korong sei pinang kabupaten padang pariaman', '-', '-', '-', '-', '81267863062', 'nofria92@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 1, 'nofriri rahmi akbar', 'perumnas bumi kencana permai blok G no 14 punggung kasik kecamatan lubuk alung kabupaten padang pariaman', '-', '-', '-', '-', '85363363477', 'nofririrahmi@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 1, 'nur samsi', 'jlan jendral sudirman no 104 manna bengkulu selatan', '-', '-', '-', '-', '82371908717', 'ns.nursamsi.ns@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 1, 'nurahima', 'silamh empat kecamatan duo koto kabupaten pasaman', '-', '-', '-', '-', '81374886042', 'nurrahima69@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 1, 'nusa elfitri', 'jl. Limau manis no 7 rt 2 rw 5', '-', '-', '-', '-', '85364119700', 'nusaefitri1992@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 1, 'putri wulan sory', 'jl semarang no 24 kel. Balai nan duo kec. Payakumbuh barat kota payakumbuh', '-', '-', '-', '-', '85374716644', 'putri.wulansory@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 1, 'rahfima welly', 'parit rantang no 72 payakumbuh', '-', '-', '-', '-', '81364404396', 'rahfima_welly@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 1, 'rahma dika saputri', 'jorong jambu lipo kecamatan lubuk tarok kabupaten sijunjung', '-', '-', '-', '-', '85274602993', 'rahmadikasaputri01@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 1, 'rahmah elfitri', 'jl. Syamratulangi barat no 19A, kp. Baru pariaman', '-', '-', '-', '-', '87895609474', 'rahmah.e_elfitri@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 1, 'reisky miranda', 'jln azki aris gang bali no 1A, rengat, riau', '-', '-', '-', '-', '85265812747', 'reisky.miranda@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 1, 'restiara azarah', 'jaan melati no 80 kelurahan balai gurun payakumbuh utara', '-', '-', '-', '-', '81267590338', 'tiazhara@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 1, 'rifky arnanda riansyah', 'jalan tenis meja no 14 rt 002 rw 013, kel batipuh panjang, kecamatan koto tangah, kota padang, sumatera barat', '-', '-', '-', '-', '85376045465', 'riansyaharnandarifky@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 1, 'rika agustina', 'perum jala utama IV blok j4 no 11, parak laweh, padang', 'asd', 'Jenis Kelamin', 'Agama', '-', '85363057694', 'rikaagustina_cilika@yahoo.com', 'Tingkat Pendidikan', '-', '-', '-', '1', '-', '-', '1', '1', '0000-00-00 00:00:00', '2018-07-21 03:37:28'),
(71, 1, 'rike nataya', 'jalan kajai I jorong murni panti kecamatan panti kab pasaman', '-', '-', '-', '-', '85263481294', 'rike.nataya@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 1, 'rizki kurniadi', 'jalan lintas timur, rt 001 rw 001 desa sungai akar, kecamatan batang gangsai, kabupaten indragiri hulu, provinsi riau', '-', '-', '-', '-', '85278073338', 'r.kurniadi0901@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 1, 'roby hidayat', 'Gg tabek kunci no 13 Andalas Padang', '-', '-', '-', '-', '85272287330', 'mandaevil@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 1, 'rosalina primarta mesuri', 'jl. Alai timur no 36 B rt 002 rw 009 kel. Alai parak kopi kec. Padang utara, Padang', '-', '-', '-', '-', '81374518411', 'rosalinameisuri@tahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 1, 'sicilia karolina', 'sioban kec. Sipora selatan kab. Kepulauan mentawai', '-', '-', '-', '-', '81374855009', 'chika_end@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 1, 'silvia anggraini', 'tarok indah permai I blok F/14 balai baru kecamatan kranji kelurahan gunung sarik, padang', '-', '-', '-', '-', '81267924244', 'silviaanggraini08@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 1, 'siska afrilina', 'jl dr wahidin kompleks pu no A 5 kalimantan barat', '-', '-', '-', '-', '85252221457', 'ka_afril@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 1, 'soraya indah sari', 'komplek kuala nyiur 2 G3 pasia nan tigo', '-', '-', '-', '-', '82285148553', 'sorayaindah_sari@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 1, 'sri rahayu putri', 'jl jendral sudirman no 51 kenagarian salido kec IV jurai, kab pessel propinsi sumatera barat', '-', '-', '-', '-', '81374700741', 'sri.rahayuputri@rocketmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 1, 'sri ulfanora', 'jl m hatta no 005 andaleh padang', '-', '-', '-', '-', '82169606091', 'sun_ola18@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 1, 'sriherteti mainingsih', 'perumahan villa bukit gading c/7 kel. Sungai sapih kec. Kuranji padang', '-', '-', '-', '-', '81363567501', 'sriherteti@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 1, 'suci asha rahmadini', 'koto tangah simalanggang, payakumbuh', '-', '-', '-', '-', '85363024720', 'uchy_4evatwinz@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 1, 'suci fourina', 'jl sei balang no 29 rt 03 rw 7 bandar buat lubuk kilangan padang', '-', '-', '-', '-', '85278109283', 'suci4ina@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 1, 'suci ririmatalata', 'jl raya siulak dusun II desa pasar senen kec. Siluak kab. Kerinci, jambi', '-', '-', '-', '-', '85266801834', 'nepunyaku@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 1, 'tika fitriolita', 'banuaran indah blok E 12 rt 003 rw 010 kelurahan banuaran kecamatan lubuk begaung kota padang', '-', '-', '-', '-', '85766003412', 'tika_fitriolita@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 1, 'tita adiatamah SY', 'jl koto panjang no 18 nagari limo koto, kecamatan koto VII, kabupaten sijunjung, sumbar', '-', '-', '-', '-', '85274015656', 'tita_adisy@hotmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 1, 'ulia rahma', 'jl sawahan dalam 1 no 18 padang', '-', '-', '-', '-', '87791881998', 'cuullya@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 1, 'vendro', 'jl. Sinaung jaya pasar tanjung ampalu sijunjung', '-', '-', '-', '-', '85263156803', 'vendroeno.ve@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 1, 'weni mailita', 'jl marapalam indah VII no 7 rt 001 rw 008 kel. Kubu marapalam, kec padang timur', '-', '-', '-', '-', '81363442354', 'wenimailita@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 1, 'wigi asma dahlena', 'tanjung pati kenagarian kotot tuo, kec. Harau, kab lima puluh kota, sumatera barat', '-', '-', '-', '-', '85274464674', 'wigi_asma_dahlena@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 1, 'winda susrianti', 'jln sepakat rt 15 rw 05 kel. Bungo timur, kec. Pasar muaro bungo kab. Bungo, jambi', '-', '-', '-', '-', '85274191790', 'windasusrianti@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 1, 'yaumil fajri', 'komplek villaku indah  IV blok K 9 rt 03 rw 06 kel kurao pagang kecamatan naggalo padang', '-', '-', '-', '-', '82283021254', 'x3_yaumilfahri@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 1, 'yeni gustita', 'koto panjang ulakan tapakis pariaman', '-', '-', '-', '-', '81374016636', 'gustitayeni@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 1, 'yoga dwi ratih', 'jl. Guk dama subarang koto baru kecamatan kubung kab. Solok, sumatera barat', '-', '-', '-', '-', '82386865446', 'yogadwiratih46@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 1, 'yose anggriyolla', 'jl raya saruaso simpang kubang landai samping KUD indomo saruaso barat kec. Tnajung emas, kab tanah datar, sumatera barat', '-', '-', '-', '-', '85364148151', 'yoseanggriyolla@rocketmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 1, 'yudi rahmat', 'jalan adinegoro gang al manar no 53 aur kuning bukittinggi', '-', '-', '-', '-', '82388101491', 'yudi.rahmat25@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 1, 'yulhemmi', 'jl. Kari musa jorong pasar matur desa matur hilia kec matur kab agam', '-', '-', '-', '-', '81374451522', 'yulhemmibunda@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 1, 'yulia fitri erningsih', 'baruah gunuang kec. Bukik barisan, kab. Lima puluh kota, payakumbuh', '-', '-', '-', '-', '85668955569', 'yuliaerningsih@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 1, 'yulita situngkir', 'dusun 1 emplasmen kebun marbau selatan kecamatan marbau kabupaten labuhan batu utara provinsi sumatera utara', '-', '-', '-', '-', '82390780601', 'nersmudayulita@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 1, 'afif D Alba', 'bengkong kolam blok E2 no 14 kel. Sadai kec. Bengkong kota batam', '-', '-', '-', '-', '82385973747', 'afifdalba@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 1, 'alber tanjung', 'jl. Mesjid lama no 9 indarung padang', '-', '-', '-', '-', '81363245116', 'alber.tanjung@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 1, 'asfri sri rahmadeni', 'jalan seraya no 1 batam', '-', '-', '-', '-', '85274522805', 'sweet_ani80@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 1, 'bunga permata wenny', 'jl. Sawahan dalam no 32 padang', '-', '-', '-', '-', '81267625625', 'ina.bunga625@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 1, 'debby sinthania', 'jl. Prof dr hamka no 117 air tawa barat', '-', '-', '-', '-', '81267556667', 'debby.sinthania@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 1, 'dedy siska', 'jl. Seraya no 1 kota batam', '-', '-', '-', '-', '85356388934', 'firman_gagah56@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 1, 'deswita', 'jl. Indrapuri tenayan raya pekanbaru', '-', '-', '-', '-', '82390800590', 'des_deswita@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 1, 'dewi puspita', 'wisma indah VI blok O no 3 kuranji padang', '-', '-', '-', '-', '81267222980', 'dewipuspita417@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 1, 'dian riani fitri', 'perumahan lambah biaro blok E5 kec. Ampek angkek canduang, kota Bukittinggi', '-', '-', '-', '-', '85374457065', 'dianriani2013@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 1, 'ditte ayu suntara', 'jl seraya no 1 batam', '-', '-', '-', '-', '85668828784', 'gamma.sundewa@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 1, 'dwi happy anggia sari', 'jln. Imam bonjol kelurahan alai gelombang kota pariaman', '-', '-', '-', '-', '81267870003', 'dwi.happyanggia@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 1, 'eka roza wijaya', 'perumahan eden park blok o no 22 batam center, rt 03 rw XII, kelurahan taman baloi kec. Batam kota batam', '-', '-', '-', '-', '81364345663', 'embungnurse@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 1, 'eli fariani ali', 'sungai gading kec. Sangir balai janggo, kab solok selatan', '-', '-', '-', '-', '85375009222', 'eli.farianii@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 1, 'fitria fajriani', 'jl kalumbuk no 5 rt 2 rw 4 kec kuranji padang', '-', '-', '-', '-', '81363557525', 'fitriafajriani@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 1, 'fourni ardiansyah', 'jln pancur mas rt 01 rw 01 kel sukarami kec selebar bengkulu', '-', '-', '-', '-', '82306439308', 'fourni.ardiansyahmaliki@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 1, 'hardani', 'simpang padang bulan, rt 02/ rw 09, desa bangun purba timur jaya, kec. Bangun purba, kab rokan hulu, riau', '-', '-', '-', '-', '85271112224', 'hardani_87@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 1, 'helena patricia', 'kompleks perumahan jala utama rindang alam blok c1', '-', '-', '-', '-', '85265409500', 'helenerianda@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 1, 'imelda rahmayunia kartika', 'jl balam ujung no 14A kelurahan labuh baru timur, kecamatan payung sekaki, pekanbaru', '-', '-', '-', '-', '81365604903', 'imelda.rahmayunia@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 1, 'indah komala sari', 'komp griya insani ambacang I blok F no 6 pauh, padang', '-', '-', '-', '-', '85375388439', 'indah.kumalasari2@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 1, 'isna aglusi badri', 'griya batu aji asri', '-', '-', '-', '-', '81374115857', 'isna_loushe@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 1, 'jumilia ', 'komplek pemda blok G/19 rt 003 rw 006 kelurahan koto luar kecamatan pauh padang', '-', '-', '-', '-', '85265632467', 'liajumalia@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 1, 'nahrul hayat', 'perum baitulhasanah blok C no 6', '-', '-', '-', '-', '85274856789', 'nahroel_ao@rocketmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 1, 'nehru nugroho', 'jl ciliwung bawah lempuing kelurahan ratu agung kota bengkulu', '-', '-', '-', '-', '85363260780', '84nehru.nugroho@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 1, 'nike puspita alwi', 'jl bandes surau gadang III no 10, kelurhana surau gadang rt V rw 1 kecamatan nanggalo, kota padang', '-', '-', '-', '-', '8116612987', 'nike_alwi@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 1, 'novriani husna', 'jl bagindo aziz chan no 102 pariaman', '-', '-', '-', '-', '82170200330', 'novriani.husna@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 1, 'randy refnandes', 'bel unp II jln sepak bola no 14 lubuk buaya padang', '-', '-', '-', '-', '81374314721', 'kiranrefnandes@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 1, 'resi novia', 'perumahan gesya residence blok A 8 no 6 kel belian kec. Batam', '-', '-', '-', '-', '85355467609', 'reno_sivia@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 1, 'revi yulia', 'perum pondok pelangi 3 blok G no 8A tiban', '-', '-', '-', '-', '85263135837', 'revi_yulia@ymail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 1, 'rezi prima', 'jl. Bodi nagari sungai tarab kec. Sungai tarab, kab. Tanah datar', '-', '-', '-', '-', '81266774426', 'rezi.prima@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 1, 'savitri gemini', 'komplek tamna buana indah blok G no 23 sei panas batam', '-', '-', '-', '-', '85261685898', 'bindy_sweat@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 1, 'sri burhani putri', 'perum asam jao blok E 18 kecamatan kubung kabupaten solok', '-', '-', '-', '-', '81266406660', 'viscere.88@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 1, 'susi erianti', 'jl. KH achmad dahlan no 73 sukajadi pekanbaru', '-', '-', '-', '-', '81378850058', 'susi_eriyanti@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 1, 'sutinah', 'jl. Tarmizi kadir RT 10 kel pakuan baru jambi', '-', '-', '-', '-', '85266401824', 'ns.titin@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 1, 'tomi jepisa', 'kp. Tangah cimparuh kota pariaman', '-', '-', '-', '-', '82284544966', 'tomijepisa95@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 1, 'trisya yona febrina', 'perum ciptaland blok mawar no 86 tiban batam', '-', '-', '-', '-', '81277979710', 'trisyayonaa@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 1, 'usraleli', 'jl srikandi perum widya graha II blok I no 18 kelurahan delima kecamatan tampan pekanbaru', '-', '-', '-', '-', '85278027818', 'israleli.211@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `perawats` (`id`, `user_id`, `nama`, `alamat`, `domisili`, `jenis_kelamin`, `agama`, `umur`, `telepon`, `email`, `pendidikan`, `institusi`, `sertifikasi`, `foto`, `mulai_aktif`, `pengalaman`, `deskripsi`, `jenis_perawatan`, `bersedia`, `created_at`, `updated_at`) VALUES
(136, 1, 'vevi suryenti putri', 'perumahan griya purnama regency blok B no 04 kelurahan suka karya kota baru, jambi', '-', '-', '-', '-', '85266499688', 'vevisuryentiputri@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 1, 'weddy martin', 'base camp, jorong anam koto selatan kec kinali', '-', '-', '-', '-', '81267847008', 'weddymartin@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 1, 'welly', 'perumahan jala mitra IV banuaran blok b4/3, kelurhan pampangan nan XX, kecamtan lubuk begalung padang', '-', '-', '-', '-', '85274757276', 'wellysajjaa@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 1, 'wesnawati', 'jalan ikhlas XIV no 8 andalas padang', '-', '-', '-', '-', '81267504992', 'ntew_nawati@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 1, 'windy freska', 'komplek pemda blok D no 21 lubuk minturun kecamatan koto tangah rt 02 rw 06, padang', '-', '-', '-', '-', '85263571000', 'windyfreska88@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 1, 'yecy anggreny', 'jl mashar no 3A labubaru barat, pekanbaru', '-', '-', '-', '-', '81378531111', 'yecy_anggreny@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 1, 'yeni devita', 'jl sikumbang jari perm. Najah muda tahap II blok A no 6, rt 003, rw 008, kel tampan kec payung sekaki, pekanbaru', '-', '-', '-', '-', '85359556822', 'vitandesta@ymail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 1, 'yessy z', 'desa suka makmur kec. Gunung lagan kab aceh singkil prov aceh', '-', '-', '-', '-', '81269185364', 'yessyzet@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 1, 'yulia devi putri', 'kp seraya', '-', '-', '-', '-', '82171146604', 'yulia.devi90@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 1, 'yuliana', 'jl iswahyudi lorong subur rt 07 no B3 pasir putih, jambi', '-', '-', '-', '-', '81366847993', 'anayuli561@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, 1, 'yureya nita', 'jl soekarno hatta, dusun campur sari, desa marsawa, kecamatan sentajo raya, kabupaten kuantan singingi, riau', '-', '-', '-', '-', '81363164486', 'rheamouse@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 1, 'sesha amiliano', 'jalan tan malaka km 14 nomor 27 dangung dangung, kabupaten lima puluh kota', '-', '-', '-', '-', '81267977166', 'sesha@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 1, 'hilma adha', 'sei pandahan kenagarian sundata kecamatan lubuk sikaping', '-', '-', '-', '-', '87892924579', 'hilmaadha@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, 1, 'septia rona imami', 'laman laweh jorong koto kaciak kecamatan tanjuang raya', '-', '-', '-', '-', '81363517515', 'thum83lynha@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 1, 'ridha agustina', 'perum kulim permai blok L 10 pekanbaru', '-', '-', '-', '-', '83181559228', 'ridaagustinaa@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 1, 'nurazizah', 'kel gn sarik kec. Kuranji', '-', '-', '-', '-', '82284563316', 'ezhi_zha@rocketmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 1, 'vicky valeri utami', 'perum mutiara garuda sakti blok M no 2, garuda sakti km 3, kel. Simpang baru, kec. Tampan, pekanbaru', '-', '-', '-', '-', '85355510106', 'vickyvaleri@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 1, 'mawa bani astuti', 'perum bukit asri blok c2 no 08 rt 010 rw 001 kel bacang, kec. Bukit intan, prov bangka belitung', '-', '-', '-', '-', '87895636055', 'banimawa@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 1, 'marsha zawina', 'jorong pasar tanjung ampalu, kelurhaan limo koto, kecamatan koto VII, kabupaten sijunjung', '-', '-', '-', '-', '85274479645', 'marshazawina@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 1, 'suci berlianti yon', 'jl. Lubuk kandang no. 8 rt 3 rw 2', '-', '-', '-', '-', '85263614064', 'uci.berlianti@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 1, 'suci dwi yoanda', 'jl lintas padang-medan no 14 sungai pandahan, lubuk sikaping pasaman', '-', '-', '-', '-', '81947654111', 'suciyoanda@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 1, 'cici andayeni', 'jl piai tangah no 96 kelurahan paiai tangah kec. Pauh, padang', '-', '-', '-', '-', '85376546192', 'chichiandayeni@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 1, 'safrini', 'jln adam BB no 21 kota padang panjang', '-', '-', '-', '-', '85263646949', 'rinii.lurchy2@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 1, 'novita kumala', 'pariaman', '-', '-', '-', '-', '81267084928', 'opikumala@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, 1, 'norisye olmi', 'komplek lubuk sejahtera lestari blok padova no 1 lubuk buaya padang', '-', '-', '-', '-', '82283796568', 'sye_muutz@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, 1, 'sty fani', 'parik lintang, jorong na 7 parik lintang, nagari ladang laweh, kecamatan banuhampu, kabupaten agam', '-', '-', '-', '-', '82283026201', 'fanisty05@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, 1, 'rani novia wardin', 'nibuang koto gadang hilir, kecamatan padang ganting, kabupaten tanah datar', '-', '-', '-', '-', '82389322901', 'noviawardin@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, 1, 'kurnia dwi yoza', 'jalan khatib sulaiman situjuah batua', '-', '-', '-', '-', '85669007703', 'dwi_nhea@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, 1, 'anis huriyati', 'jalan pancing 1 no 36 longk III kel indrakasih kec. Medan tembung kota medan sumatera utara', '-', '-', '-', '-', '85262779272', 'anis.huriyati@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, 1, 'resti maiwandira', 'jl. Sudirman no 228 kecamatan pariaman tengah kota pariaman', '-', '-', '-', '-', '81266773647', 'ladyrose_synester137@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, 1, 'aslamiati', 'jl padang lamo ds. Ulak banjir kec. Tebo ulu kab. Tebo, jambi', '-', '-', '-', '-', '82172259048', 'aslamiatiniezz@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, 1, 'yommy elsa', 'jl rasuna said no 33a kel. Balai batimah tiakar, kota payakumbuh', '-', '-', '-', '-', '81372870003', 'ami.jlex@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, 1, 'sri kendiyol jelisa', 'ujung air kec. Sutera kab. Pesisir selatan', '-', '-', '-', '-', '82383601877', 'kendiyolharthithory@ymail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, 1, 'dilla paryanti', 'kampung gadang, pariaman timur', '-', '-', '-', '-', '81270576618', 'diella.girlz@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, 1, 'lhona', 'koto jorong mandahiling, pagaruyung, tnajung emas, kab. Tanah datar', '-', '-', '-', '-', '82385850091', 'asslm_lhona@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, 1, 'nike isma putri', 'jln. Marapalam indah V no 6 padang', '-', '-', '-', '-', '82383474688', 'nikeismaputri@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, 1, 'widya zahranita', 'nagari pangian, kec. Linau buo, kab. Tanah datar', '-', '-', '-', '-', '85265175731', 'widyaraa.geulis@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, 1, 'luki desria', 'padang marapalam kelurahan lakitan utara kecamatan lengayang kabupaten pesisir selatan', '-', '-', '-', '-', '85374396716', 'luki.desria@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, 1, 'weri heryani', 'lungguk muto kec ampek angkek kab. Agam', '-', '-', '-', '-', '85274920274', 'weri_heryani@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, 1, 'rahmadeni irma', 'jorong parik rantang kecamatan kamang baru kabupaten sijunjung', '-', '-', '-', '-', '82391857939', 'caterpilaris_rdi@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, 1, 'elvira fitriani', 'batu gadang rt 002 rw 005 kelurahan batu gadang', '-', '-', '-', '-', '85766149110', 'elvirafitriani5@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, 1, 'willa septiadi', 'duku, kecamatan koto XI tarusan, kabupaten pesisir selatan', '-', '-', '-', '-', '81363619244', 'willachowil@ymail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, 1, 'elsa hazila', 'jalan soekarno hatta no 55A, bukittinggi', '-', '-', '-', '-', '83182222280', 'ezil_alezmutz@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, 1, 'rozi erlina', 'jorong taratak tangah, desa sumpur kudus, kecamatan sumpur kudus', '-', '-', '-', '-', '85278909501', 'rozierlina@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, 1, 'navilah ramadhana', 'jl. Manunggai no 14 kel. Tarok payakumbuh utara', '-', '-', '-', '-', '85263986473', 'navilahramadhana@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, 1, 'shinta dewi kasih brata', 'jalan ahmad karim no 13 rt 11 koto panjang, padang panjang timur', '-', '-', '-', '-', '81277826148', 'brathashintadewikasih@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, 1, 'fadhila yanti', 'koto dalam, rt 002 rw 005 kel. Pulai anak air, kec. Mandiangin koto selayan, kota bukittinggi', '-', '-', '-', '-', '85265977042', 'fadhilayanti@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, 1, 'fitrah qalbina', 'jl anak air lakuang no 33 kel. Pulai anak air kecamatan mandiangin koto selayan bukittinggi', '-', '-', '-', '-', '85263798282', 'qalbina.qalbina@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, 1, 'ridha mardiah', 'jalan sersan zainal no 11 aro IV korong kecamatan lubuk sikarah, kota solok', '-', '-', '-', '-', '85263738334', 'dharidhaqwerty@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, 1, 'ilham sanjaya', 'angkasa pura hiang rt 4 kec. Sitinjau laut, kab kerinci, pro. Jambi', '-', '-', '-', '-', '85265977576', 'ilhamsanjaya1012@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, 1, 'yosy vinaela', 'kp. Tangah jr III koto tinggi kenagarian sundata kecamatan lubuk sikaping', '-', '-', '-', '-', '81277886514', 'vinaelayosy@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, 1, 'rahmiati D.S', 'jl. Bakti abri no 19 padangsidempuan', '-', '-', '-', '-', '81363980442', 'rahmiati_ds@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, 1, 'rina novriana', 'jl sawahan dalam 1 no 18 kel. Sawahan kec padang timur', '-', '-', '-', '-', '85263362259', 'novriana.rina@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, 1, 'rozilawati nasril', 'padang alai, payakumbuh timur', '-', '-', '-', '-', '85263047747', 'rozilawatinasril@gmal.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, 1, 'loly hidayati', 'jl. Bahder johan, bukittinggi', '-', '-', '-', '-', '85274540555', 'chylo10713@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, 1, 'siska yulandri', 'jl mangga IX no 495 rt 01 rw 10 blok F perumnas belimbing padang', '-', '-', '-', '-', '81363417765', 'icka10_08@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(192, 1, 'andina ariesta putri', 'perumnas salasah indah blok D2 guguak dadok sijunjung', '-', '-', '-', '-', '85263040410', 'andinaariesta88@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(193, 1, 'almira gandhi', 'jl. Rambun bulan no 4 B lapai padang', '-', '-', '-', '-', '81363346338', 'almira.gandhi@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, 1, 'asnel sartika', 'jl s. parman no 125 ulak karang padang', '-', '-', '-', '-', '82391869939', 'asnel_sartika@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(195, 1, 'andi batafia', 'perumahan bukittinggi indah rt 002 rw 007 kelurahan pakan labuh, kec. Aur birugo tigo baleh, bukittinggi, sumatera barat', '-', '-', '-', '-', '8126724578', 'andi.batafia@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, 1, 'maila andra santi', 'jl. Labuh batu hampar kec. Koto XI tarusan ka. Pesisir selatan', '-', '-', '-', '-', '85364612002', 'mailaandrasanti@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, 1, 'rasyidah', 'jl tarok tarandam tarok dipo bukittinggi', '-', '-', '-', '-', '82172255738', 'idha.mr.83@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, 1, 'sri mardhiah putri', 'jl pampangan no 15 padang', '-', '-', '-', '-', '85376232561', 'putrisrimardhiah@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, 1, 'maya syafni', 'jorong koto baru kenagarian sumani kec X koto singkarak kab. Solok', '-', '-', '-', '-', '82170389339', 'maia.syafni@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(200, 1, 'reftika edelwis', 'perum lb gading permai I blok E no 14/15 rt 02 rw 10 kel. Lb buaya kec. Koto tangah padang', '-', '-', '-', '-', '82388524418', 'reftikae@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(201, 1, 'usriani andari', 'tanah lapang jorong gando paninggahan kecmaatan junjung sirih kabupaten solok', '-', '-', '-', '-', '85355136915', 'usriani@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, 1, 'erfiani destrianty', 'jl smp n 11 no 06 rt 001 kel rawa makmur permai kec. Muara bangkahulu', '-', '-', '-', '-', '85274061630', 'erfianidestrianty@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(203, 1, 'vivi oktasari', 'silangkung nagari lurah ampalu kec Vi koto sungai sarik kabupaten padang pariaman', '-', '-', '-', '-', '82382340354', 'oktasari.vivi@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(204, 1, 'fenny f', 'perumnas GSI blok E no 16 jorong ladang kapeh, padang sibusuk kec. Kupitan', '-', '-', '-', '-', '85364665775', 'fenny.firzal@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, 1, 'rini heldina', 'jl beringin III B no 5 lolong belanti, kota padang', '-', '-', '-', '-', '81374444524', 'riniheldina@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(206, 1, 'wwike yanti elfisa', 'jln berok I no 27 rt 001 rw 001 kelurahan berok nipah kecamatan padang barat', '-', '-', '-', '-', '85222424653', 'wiwikeyantielfisa@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(207, 1, 'rusmanwadi', 'jln muhammad yamin no 57A komplek pusako batusangkar', '-', '-', '-', '-', '81363358408', 'rusmanwadi@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, 1, 'firda damba wahyuni', 'palokan desa palokan inderapura, kec airpura', '-', '-', '-', '-', '85364771200', 'firdadambaw@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(209, 1, 'nola asril', 'perum pondok karya I, blok B3 padang', '-', '-', '-', '-', '85356006994', 'oladmezzo@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(210, 1, 'sari angreni', 'pasar baru muara panas kec. Bukit sundi kab. Solok', '-', '-', '-', '-', '81374040701', 'sariangreni90@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(211, 1, 'honesty putri', 'jl hiu 1 no 11 ulak karang selatan', '-', '-', '-', '-', '82171878044', 'honesty.putri@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(212, 1, 'sarie andhika putri', 'jl rambutan raya no 55 rt 03 rw13 belimbing kelurahan kuranji', '-', '-', '-', '-', '81363762425', 'sarieandhikaputri@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(213, 1, 'maharani', 'jorong I siguhung kec. Lubuk basung kab. Agam', '-', '-', '-', '-', '81374906210', 'mh124ni.z@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(214, 1, 'armanda tri murtiningsih', 'jl veteran no 282 kota solok', '-', '-', '-', '-', '82388767555', 'armanda3murti@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(215, 1, 'esthika ariany maisa', 'komplek griya permata I blok D2 no 3 kel. Tabing banda gadang kec. Nanggalo padang', '-', '-', '-', '-', '81363201387', 'maisa.thika@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(216, 1, 'kamariyah', 'jambi', '-', '-', '-', '-', '85357653983', 'cocom2fahri@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(217, 1, 'rika syafitri', 'jl parak karakah no 15 rt 01 rw 10 kec. Padang timur kel. Kubu dalam', '-', '-', '-', '-', '85278115304', 'rika_syafitri28@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(218, 1, 'indah mawarti', 'jl kalimantan perumnas muara bulian jambi', '-', '-', '-', '-', '82378267770', 'i.mawarti@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(219, 1, 'nova rita', 'wisma pondok indah rt 001 rw 005 kelurahan korong gadang kecamatan kuranji padang', '-', '-', '-', '-', '85268883466', 'nova_rita07@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(220, 1, 'indri ramadini', 'asrama polisi rimbo kaluang blok I nomor 4 raden saleh padang', '-', '-', '-', '-', '85220305510', 'indri.ramadini@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(221, 1, 'mery lingga anggraini', 'komp. Griya permata blok I no 2 rt 04 rw 01 kel. Tabing banda gadang kec. Nanggalo padang', '-', '-', '-', '-', '81363495324', 'mery_lingga@ymail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(222, 1, 'ratna dewi', 'jl birugo puhun no 87 rt 01 rw 03 bukittinggi', '-', '-', '-', '-', '81363875660', 'ratnadewi251183@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(223, 1, 'indaryani', 'jl agam no 461 rt 003 rw 005 kelurahan surau gadang, kecamatan nanggalo padang', '-', '-', '-', '-', '81374105291', 'meta.rikandi@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(224, 1, 'dian sari', 'jl m yunus no 122 rt 004 rw 003 kel. Anduring kec kuranji', '-', '-', '-', '-', '81267416823', 'dian_sari83@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(225, 1, 'yessi fadriyanti', 'wisma indah V jln gunung pasaman blok E3 no 1 tabing padang', '-', '-', '-', '-', '81374983655', 'fadri1975@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(226, 1, 'fitria nola rezkiki', 'jl birugo puhun no 305 B', '-', '-', '-', '-', '89609842456', 'fitrianola.rezkiki@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(227, 1, 'wiwit febrina', 'ipuh jalan mandiangin no 17B rt 004 rw 002 kelurahan campago ipuh kecamatan mandiangin koto selayan kota bukittinggi', '-', '-', '-', '-', '85263545403', 'wiwit.febrina@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(228, 1, 'yuanita ananda', 'perumahan griya samitra blok A no 2 kelurahan korong gadang kecamatna kuranji padang', '-', '-', '-', '-', '81363739746', 'yuanita_ananda68@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(229, 1, 'riris friandi', 'air biso nagari limo koto kec. Bonjol kab. Pasaman', '-', '-', '-', '-', '82183573769', 'endi.arbios@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(230, 1, 'lisavina juwita', 'lb.lintah no 26 kelurahan lb. lintah kurnaji padang', '-', '-', '-', '-', '85766026746', 'lisavina_juwita@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(231, 1, 'reska handayani', 'piliang lapau langkok jorong balai lalang kenagarian saning bakar kecamatan X koto singkarak kabupaten solok', '-', '-', '-', '-', '85263297989', 'handayanireska@ymail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(232, 1, 'utari christa wardhani', 'desa guguak VII koto kecamatan guguak kabupaten lima puluh kota', '-', '-', '-', '-', '85274282845', 'wardhaniutari@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(233, 1, 'liza wati', 'jl pemuda no 35 tanjungpinang kepulauan riau', '-', '-', '-', '-', '85274051620', 'liza_chintin@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(234, 1, 'hidayati', 'jl jenderal sudirman no 101 batas kota bukittinggi', '-', '-', '-', '-', '81374633145', 'at_hidayati@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(235, 1, 'nonong tri senja', 'komplek perumahan rimbo panjang sei abang lubuk alung padang pariaman', '-', '-', '-', '-', '82172617714', 'nonong.andika@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(236, 1, 'ernalinda rosya', 'jorong bansa, nagari gauang, kec. Kubung, kab.solok', '-', '-', '-', '-', '81363460980', 'ernalindarosya@yahoo.co.id', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(237, 1, 'netty herawati', 'perumahan solok permata indah blok E 23 no 160 gelanggang betung kel. Nan balimo kota solok', '-', '-', '-', '-', '81374240202', 'netty261075@gmail.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(238, 1, 'hotmaria julia dolok saribu', 'jl sungai jang komplek TNI AL blok A-06 tanjungpinang- kepulauan riau', '-', '-', '-', '-', '85278198887', 'hotmaria_joelya@yahoo.com', '-', '-', '-', '-', '-', '-', '-', '-', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(240, 1, 'Devi kumala sari', 'Cakung', 'Kayu tinggi', 'Perempuan', 'Islam', '-', '085280794453', 'devi.sari189377@gmail.com', 'S1', 'Universitas muhammadiyah jakarta', 'BTCLS', 'perawat/1.jpg', '1 - 5 tahun', 'Saat profesi ners', 'Saya lulusa s1 ners, sekarang bekerja di perusahaan admedika', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(241, 1, 'Rosalina Lanu', 'Timika baru', 'Papua', 'Perempuan', 'Protestan', '-', '081343040780', 'rosalina.lanu92@gmail.com', 'D3', 'Akper Sawerigading Pemda Luwu', 'Blm ada', 'perawat/2.jpg', '1 - 5 tahun', '1 thn pengalaman menjadi perawat', 'Ingin menjadi perawat profesional', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(242, 1, 'Sulih Ristiyani Ayu Saputri', 'Jl. Damai 74, Jagakarsa, Jakarta Selatan', 'Jakarta', 'Perempuan', 'Islam', '-', '083872245690', 'sulih12.2@gmail.com', 'S1', 'STIKES INDONESIA MAJU', 'BTCLS', 'perawat/3.jpg', 'Kurang dari 1 tahun', 'IGD', 'Disiplin', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(243, 1, 'Luluk Syahrotul Hizen', 'Dusun watu kodok RT 05 RW 03, kelurahan keblukan, kecamatan Kaloran, kabupaten temanggung', 'Temanggung', 'Perempuan', 'Islam', '-', '082313270625', 'luluksyahrotul12.lsh@gmail.com', 'D3', 'Akper Al-Kautsar temanggung', 'Wound care, BTCLS', 'perawat/4.jpg', 'Kurang dari 1 tahun', 'Home care dan perawatan luka DM ', 'Lulusan tahun 2017', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(244, 1, 'Shanty Tambunan', 'Jl. Rawasari barat VII no E 111 ', 'Jakarta pusat', 'Perempuan', 'Protestan', '-', '082299080012', 'shantutambunan2@gmail.com', 'D3', 'Akper RS PGI cikini', 'BTCLS', 'perawat/5.jpg', '5 - 10 tahun', 'Di RS PGI cikini selama 8 tahun', 'Mampu menangani pasien khemo', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(245, 1, 'Erma Taukhida', 'Jl.boulevard citra maja raya cluster green cove, maja, lebak banten', 'Semarang', 'Perempuan', 'Islam', '-', '081386131330', 'taukhidaerma@yahoo.co.id', 'D3', 'Stikes Telogorejo', 'STR, BTCLS', 'perawat/6.jpg', '1 - 5 tahun', '3,5 Tahun', 'Saya seorang perawat ramah lembut dan ulet . ', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(246, 1, 'Yetty indriani', 'Villa mas garden blok E no 62', 'Bekasi utara', 'Perempuan', 'Protestan', '-', '085718775606', 'indrianiyetty@gmail.com', 'S1', 'Stikes binawan', 'Btcls, toefl,  ielts', 'perawat/7.jpg', '1 - 5 tahun', 'Bekerja sebagai perawat pelaksana di rs anna medika selama 6bulan', 'Saya sudah tamat d3 keperawatan kemudian melanjutkan s1 dan profesi ners', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, 1, 'Retno Nurjayanti', 'padangan glodogan klaten selatan klaten jawa tengah', 'DKI Jakarta', 'Perempuan', 'Islam', '-', '085743120290', 'retnonurjayanti79@gmail.com', 'D3', 'STIKES MUHAMMADIYAH KLATEN', 'kemoterapi, kegawatdaruratan bedah,BLS,DLL', 'perawat/8.jpg', '1 - 5 tahun', 'RS PONDOK INDAGH PURI INDAH DARI 2013', 'BEKERJA DILINGKUNGAN RS ATAU LEMBAGA PEMERINTAHAN', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, 1, 'Hesti Diahwati', 'Jl.raya solo pule mantingan ngawi rt 05 rw 01 mantingan ngawi', 'Jawa timur', 'Laki Laki', 'Islam', '-', '082230299119', 'dhea_autya@yahoo.co.id', 'S1', 'Stikes Aisyiyah surakarta', 'Belum ada', 'perawat/9.jpg', 'Kurang dari 1 tahun', 'Pada terapi anak berkebutuhan khusus', 'Perawat anak dengan keluhan anak dengan berkebutuhan khusus', 'Visit Fisioterapi', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, 1, 'Chris Evo Nella Purba', 'Jalan ngumban surbakti, Medan', 'Medan', 'Perempuan', 'Protestan', '-', '081370445722', 'evochris840@gmail.com', 'D3', 'Akademi keperawatan herna Medan', 'Tidak ada', 'perawat/10.jpg', '5 - 10 tahun', 'Di Rumah sakit', 'Tidak ada', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, 1, 'Meillensie Meisella Debora', 'Jl. Prof. Dr. Latumenten II gang 2 no.15b rt.007/005 kel. Jelambar kec. Grogol petamburan', 'Jakarta Barat', 'Perempuan', 'Protestan', '-', '083872911333', 'lensie02@yahoo.com', 'D3', 'Akademi Keperawatan RS Husada', 'Pelatihan seminar dan btcls', 'perawat/11.jpg', 'Kurang dari 1 tahun', 'Belum ada karena baru lulus tahun 2018', 'Semangat yang tinggi, tepat waktu, jujur, dapat bekerja sama dengan team, labil', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, 1, 'Desvy Enggar Nurhabibah', 'Komp. Cijerah 1 blok 1 no 54 ', 'Bandung', 'Perempuan', 'Islam', '-', '089655162676', 'desvy.enggar12@gmail.com', 'S1', 'STIKES A.YANI CIMAHI', 'PPGD', 'perawat/12.jpg', 'Kurang dari 1 tahun', 'Perawat homecare, perawat klinik rumah bersalin', 'Saya org yg aktif dan cepat akrab terhadap org baru', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, 1, 'Nicolas Nazaretto', 'Perum Puri Gardenia. Blok E4. No. 5. Babelan. Bekasi', 'Babelan. Bekasi', 'Laki Laki', 'Protestan', '-', '081212694770', 'dayakenyah@gmail.com', 'D3', 'Akper RS PGI Cikini', 'BTCLS', 'perawat/13.jpg', '5 - 10 tahun', 'Penanganan pasien gawat darurat', 'Sabar', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(253, 1, 'Muhamad Arif Candra Mahardika', 'ds.babadan bantul yogyakarta', 'Yogyakarta', 'Laki Laki', 'Islam', '-', '085799122227', 'candramahardika23.cm@gmail.com', 'D3', 'akper KBH yogyakarta', 'ijazah. STR. BTCLS', 'perawat/14.jpg', 'Kurang dari 1 tahun', 'rs juwita bekasi', 'saya selalu on time. mampu bekerja tim', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, 1, 'Tias Ayu Aldila', 'Menggoran, Bengkal, Kranggan, Temanggung, Jawa Tengah', 'Temanggung', 'Perempuan', 'Islam', '-', '085600051939', 'tyasayu1995@gmail.com', 'S1', 'UNIVERSITAS MUHAMMADIYAH MAGELANG', 'BTLS , BCLS, BLS', 'perawat/15.jpg', 'Kurang dari 1 tahun', 'Belum pernah ', 'Pernah bekerja sebagai enumerator dari FKM UI', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, 1, 'Taufik Dwi Cahyono', 'Pacitan', 'Surakarta', 'Laki Laki', 'Islam', '-', '087758333093', 'afikdwii@gmail.com', 'Ners', 'Universitas Muhammadiyah Surakarta', 'BTCLS, HIPKABI Jateng', 'perawat/16.jpg', 'Kurang dari 1 tahun', 'Belum pernah bekerja menjadi perawat', 'Sabar dan jujur', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(256, 1, 'Erna Liana Herman', 'Desa Ujung Lamuru, Kecamatan Lappariaja, Kabupaten Bone.', 'Kabupaten Bone', 'Perempuan', 'Islam', '-', '085397551528', 'ernaherman7@gmail.com', 'D3', 'AKPER PEMKAB BULUKUMBA', 'Pelatihan BHD (Bantuan Hidup Dasar)', 'perawat/17.jpg', 'Kurang dari 1 tahun', 'Menjadi perawat adalah hal yang sangat mulia. Meski latar belakang seorang perawat terkadang di anak tirikan oleh pemerintah, tetapi menjadi manusia yang bisa memanusiakan orang lain, membantu meringankan sakit orang lain merupakan kepuasan tersendiri yang hanya bisa dirasakan oleh seorang perawat, dan saya bangga bisa merasakan hal tersebut.', 'Saya alumni salah satu PTS di Kabupaten Bulukumba, lulus dengan IPK 3,64. Saat ini saya sedang memiliki kesulitan dalam mencari pekerjaan. Padahal rasa solidaritas saya sebagai seorang perawat telah mengalir dalam hidup saya. Saya sangat ingin membantu orang lain dengan berstatus resmi sebagai perawat.', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, 1, 'Ekky Sulisti Yusuf', 'Perumahan mayang pratama blok e1 no 42 rt 15 rw 008 kelurahan mustikasari kecamatan mustikajaya ,bekasi timur 17157', 'Bekasi Timur', 'Perempuan', 'Islam', '-', '081319881004', 'ekky.tan@gmail.com', 'D3', 'Akademi kebidanan syahida komunika tasikmalaya', 'Kompetensi bidan', 'perawat/18.jpg', '1 - 5 tahun', 'Bekerja di klinik aizar medika selama 1.5 tahun', 'Bidan Berkompetensi', 'Visit Bidan', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, 1, 'Dwi Anggriani', 'Jl.grand depok city rt 01/02 kel. Tirtajaya kec. Sukmajaya', 'Bogor', 'Perempuan', 'Islam', '-', '08994290623', 'anghidwi369@gmail.com', 'SMK', 'Smk kesehatan logos', 'Belum mendapatkan sertifikasi', 'perawat/19.jpg', 'Kurang dari 1 tahun', 'Senang bisa merawat pasien', 'Hanya lulusan smk', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, 1, 'Silvia Rahmawati', 'Dsn. Dukuh Barat Desa Plumbon RT 19/05 Kec. Suruh Kab. Semarang Jawa Tengah', 'Semarang', 'Perempuan', 'Islam', '-', '085712400729', 'silvia.rahma1995@gmail.com', 'Ners', 'Politeknik Kesehatan Kemenkes Semarang', 'BTCLS, PPI, Toefl', 'perawat/20.jpg', 'Kurang dari 1 tahun', 'Satu tahun selama pendidikan Ners di berbagai RS di Jawa Tengah', 'Ramah, murah senyum, mandiri, tidak pantang menyerah dan selalu memberikan yang terbaik', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, 1, 'Aprilia Arifudin', 'Rt 19 rw04 ds.gamping kec.suruh. kab.trenggalek. jawa timur', 'Bekasi', 'Laki Laki', 'Islam', '-', '082331813775', 'aprilarif15@gmail.com', 'S1', 'STIKES KARYA HUSADA KEDIRI', 'PPGD', 'perawat/21.jpg', 'Kurang dari 1 tahun', '5 bulan di kamar bedah', 'Saya orang yang rajin dan dapat bekerja secara profesional', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, 1, 'Sesarine Kristina', 'Jati Wetan Kudus', 'Kudus Jawa Tengah', 'Perempuan', 'Protestan', '-', '085600230825', 'ksesarine@gmail.com', 'D3', 'Stikes St. Elisabeth Semarang', 'BTCLS', 'perawat/22.jpg', 'Kurang dari 1 tahun', 'RS Mardi Rahayu Kudus', 'wanita, muda', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, 1, 'Tasha Permatasari', 'Jl palem 1 no 8A petukangan utara jakarta selatan', 'Jakarta', 'Perempuan', 'Islam', '-', '081311227092', 'tashapermatasari48828@gmail.com', 'S1', 'STIKes Pertamina Bina Medika', 'Btcls, toefl', 'perawat/23.jpg', '1 - 5 tahun', '1 tahun ', 'Saya memahami prinsip luka, dan KDM ', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, 1, 'Richard Simanjuntak', 'Kampus universitas advent indonesia', 'Jakarta', 'Perempuan', 'Protestan', '-', '081218926531', 'richardsimanjuntak070@gmail.com', 'D3', 'Perguruan tinggi advent surya nusantara', 'Belum ada', 'perawat/24.jpg', '1 - 5 tahun', 'Perawat rumah sakit advent,perawat ambulans,perawat homecare,klinik aesthetic,klinik rawat jalan.', 'Saya lebih suka perawat homecare', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, 1, 'Ardi Surya', 'Jln.nasional - bandara cut nyak dhien , desa kubang gajah kec. Kuala pesisir, kab.nagan raya aceh', 'Nagan Raya', 'Laki Laki', 'Islam', '-', '085373329093', 'ardisuryamedic@gmail.com', 'D3', 'POLTEKKES ACEH', 'BTCLS (Basic Trauma Cardiac Life Support)', 'perawat/25.jpg', '1 - 5 tahun', '2 tahun bekerja sebagai paramedis di perusahaan ', 'Mempunyai peran penting dalam meningkatkan kesehatan keselamatan kerja di lingkungan perusahaan K 3 atau sering di sebut dalam departemen HSE ', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, 1, 'Diana Yuli Utami', 'Tawangsari rt 4 rw 1, no.39, sepanjang taman sidoarjo', 'Sidoarjo', 'Perempuan', 'Islam', '-', '081336588235', 'dianayuli92@gmail.com', 'S1', 'Stikes Hang Tuah Surabaya', 'STR, BTCLS', 'perawat/26.jpg', '1 - 5 tahun', 'Pernah bekerja sebagai perawat mata, perawat rawat luka.', 'Saya dapat bekerja dengan tim, dan saya bersungguh- sungguh dengan pekerjaan yang saya lakukan.', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, 1, 'Yunia Rifqoh', 'Jalan melur tugu IV, tugu utara, koja, jakarta utara', 'Jakarta', 'Perempuan', 'Islam', '-', '082220897824', 'yunia12437@gmail.com', 'D3', 'AKPER PEMKOT TEGAL', 'Pelatihan Asuhan paliatif, BTCLS, pelatihan keperawatan kardiovaskuler pemula.', 'perawat/27.jpg', '1 - 5 tahun', 'Di ruang rawat 1th, di ruang bedah 2tahun', 'Memiliki motivasi untuk bekerja keras, kemauan untuk belajar', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(267, 1, 'Sintya Pertiwi', 'Desa Sengonbugel rt 4 rw 2 mayong jepara', 'Jepara', 'Perempuan', 'Islam', '-', '082140409385', 'sintyapertiwi95@gmail.com', 'S1', 'Sekolah tinggi ilmi kesehatan muhammadiyah kudus', 'STR dan BTCLS', 'perawat/28.jpg', 'Kurang dari 1 tahun', 'Menyenangkan ketika melihat pasien sembuh dan tersenyum ', 'Saat melihat senyum pasien yg sudah sembuh dari sakitnya setelah kita rawat itu hal yg sangat menyenangkan bagi saya', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, 1, 'Aliffia Rusiriantin', 'Asrama yon armed 10 ds.cimandala kec.sukaraja kab bogor', 'Bogor', 'Perempuan', 'Islam', '-', '082240538352', 'oliphrusitianti@gmail.com', 'D3', 'Akademi kebidanan dharma husada kediri', 'Str', 'perawat/29.jpg', '1 - 5 tahun', 'Klinik perusahaan', 'Ulet, bertanggung jawab, suka belajar hal baru, profesional', 'Visit Bidan', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, 1, 'Deby Soraya br Sinuhaji', 'Kampus universitas advent indonesia', 'Jakarta', 'Perempuan', 'Protestan', '-', '081218926531', 'sorayadeby0@gmail.com', 'D3', 'Perguruan tinggi advent surya nusantara', 'Belum ada', 'perawat/30.jpg', '1 - 5 tahun', 'Perawat rumah sakit advent bandung 1 tahun,spesial nurse 1 tahun,ambulans 1 tahun,klinik aesthetic 1 tahun.', 'Saya suka homecare', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, 1, 'Lena', 'Jl.air dingin gg.taqwa 5', 'Pekanbaru', 'Perempuan', 'Protestan', '-', '085263909374', 'lenaoktavia95@gmail.com', 'D4', 'Poltekkes kemenkes riau', 'BTCLS', 'perawat/31.jpg', 'Kurang dari 1 tahun', 'Bekerja di klinik selama 8 bulan', 'Ceria, mudah bergaul', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, 1, 'Ardian Rudi Kristanto', 'Ds. Pohwates  Rt.004 Rw.001 Kec. Kepohbaru Kab. Bojonegoro', 'Sidoarjo', 'Laki Laki', 'Islam', '-', '082234934466', 'ardianrudi29@gmail.com', 'S1', 'STIKES Bahrul Ulum Jombang', 'Sertifikat PPGD', 'perawat/32.jpg', '1 - 5 tahun', 'Praktika Kesehatan di beberapa rumah sakit', 'S 1 Keperawatan Profesi Ners', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, 1, 'Gita Puspitasari', 'Jl Perintis Kemerdekaan No 24 Ciamis', 'Ciamis', 'Perempuan', 'Islam', '-', '085322737007', 'gpus19@gmail.com', 'S1', 'Univeritas Padjadjaran', 'PPGD Basic 1, Introduction of Wound Management', 'perawat/33.jpg', 'Kurang dari 1 tahun', 'Fresh Graduated', 'Fresh Graduated', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, 1, 'MD Nugroho', 'Perum Griya Cikeas Blok C5/4 Gunungputri Bogor', 'Jawa Barat', 'Laki Laki', 'Islam', '-', '085255665757', 'noegrohohs@gmail.com', 'Ners', 'Universitas Ngudi Waluyo', 'ICU, BTCLS', 'perawat/34.jpg', '1 - 5 tahun', 'ICU', 'Mampu memberikan asuhan keperawatan dengan baik sesuai standar keperawatan baik mandiri maupun kolaboratif.', 'Visit Fisioterapi', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, 1, 'M Hery Subandi', 'Jl. Jempiring, Mispalah, Kel. Prapen,   Kec. Praya, Kab. Lombok Tengah, NTB 83511', 'Sleman, DIY', 'Laki Laki', 'Islam', '-', '081529940520', 'm.herysubandi@gmail.com', 'S1', 'Universitas Jenderal A. Yani Yogyakarta', 'PPGD, SIRKUMSISI, ', 'perawat/35.jpg', '1 - 5 tahun', 'Puskesmas Mangkung ', 'Saya orang yang religius, bertanggungjawab, ramah, dan suka bergaul. Dan saya memilikk kepribadian yang baik dibuktikan dengan bekerja terakhir di BNN sebagai konselor lapas, dan suka dengan kegiatan yang bersifat sosial.', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, 1, 'Wisnu Muhamad Patah', 'Kp.Bendungan Rt.001 Rw.011 Ds.Sagara Kec.Cibalong Kab.Garut', 'Tarogong Kidul Garut', 'Laki Laki', 'Islam', '-', '082118169614', 'wisnupatah@gmail.com', 'S1', 'STIKes Karsa Husada Garut', 'BTCLS & K3', 'perawat/36.jpg', '1 - 5 tahun', 'Perawat Puskesmas Pebantu Di Desa dan Di Klinik', 'Saya adalah orang yang siap bekerja untuk melayani setiap pasien,tidak pernah membeda-bedakan pasien dalam setatus ekinomi,sosial,maupun budaya,saya juga orang yang ingin bekerja keras untuk merubah kehidupan saya agar menjadi lebih baik.', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, 1, 'Yovy mahfuza', 'Jalan pahlawan no 60, duren jaya', 'Bekasi timur', 'Laki Laki', 'Islam', '-', '081218048159', 'yovymahfuza@gmail.com', 'S1', 'Stikes binawan', 'Str, btcls, ppgd, serkom', 'perawat/37.jpg', '1 - 5 tahun', 'Rs sentosa', 'Pernah di ruang bedah', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, 1, 'Azhari', 'Mon dua', 'Nagan raya', 'Laki Laki', 'Islam', '-', '085359940447', 'azhariehar@gmail.com', 'S1', 'Sekolah tinggi ilmu kesehatan sumatera utara', 'Tidak ada', 'perawat/38.jpg', 'Kurang dari 1 tahun', 'Praktikum rumah sakit dr pirngadi', 'Tidak ada', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, 1, 'Andi Nur Amifta', 'Jl. Elang II no 107', 'Kota palu', 'Perempuan', 'Islam', '-', '085249708558', 'amiftaamifta28@gmail.com', 'D3', 'Akademi keperawatan pemda donggala', 'Sertifikat gawat darurat dan sertifikat registrasi perawat (STR) ', 'perawat/39.jpg', 'Kurang dari 1 tahun', 'Perawat sekolah', 'Selain perawat saya juga berpengalaman menjadi staff administrasi dan guru', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, 1, 'Resty Ramayanti Suparno', 'Jlan Mesjid Bendungan No 12A Kec. Kramat Jati, Kel. Cawang. Kota Jakarta Timur', 'Jakarta Timur', 'Perempuan', 'Islam', '-', '085956175950', 'resty.suparno@yahoo.com', 'S1 Profesi Nurs', 'Sekolah Tinggi Ilmu Kesehatan Binawan', 'BTCLS dan Hiperkes', 'perawat/40.jpg', '1 - 5 tahun', 'Fresh Graduate', 'Sya orgnya ontime dlam waktu, selalu teliti dlam bkerja, bisa melakukan pemasangan infus, NGT, Cateter, EKG dan yg sdah menjadi kewajiban tugas perawat yg lainnya', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, 1, 'Akhmad Ali', 'Semarang', 'Semarang', 'Laki Laki', 'Islam', '-', '085790258014', 'akhmadali70@gmail.com', 'D3', 'Akper dr soedono madiun', 'Pelatihan ppgd, pelatihan ibs, pelatihan perawatan luka modern', 'perawat/41.jpg', '5 - 10 tahun', 'Puskesmas purwosari, rs petrokimia gresik, rsup dr kariadi', 'Saya sebagai perawat siap memberikan pelayanan keperawatan yang profesional kepada pasien', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, 1, 'Faris Antoni', 'Magetan', 'Madiun', 'Laki Laki', 'Tidak ingin menyebutkan', '-', '089525240930', 'farisantoni64@gmail.com', 'D3', 'Politeknik Kesehatan Kemenkes Surabaya', 'PPGD', 'perawat/42.jpg', '1 - 5 tahun', '5 tahun', 'Fast response for your healthcaring', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, 1, 'Fitri Supriadi', 'Cigondewah Hilir RT 02/05 Cigondewah Hilir Margaasih Kab. Bandung', 'Bandung', 'Perempuan', 'Islam', '-', '08976253276', 'fitrisupriadi0409@gmail.com', 'D3', 'KEPERAWATAN POLTEKKES BANDUNG', 'In House Training: Pelatihan Neonatus Dasar RSUPN Dr. Cipto Mangunkusumo 2016', 'perawat/43.jpg', '1 - 5 tahun', 'Perawat Perinatologi RSCM, Perawat Puskesmas Pagarsih', 'Mampu bekerja perorangan maupun tim', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, 1, 'Dian Wahyuningrum', 'perum griya kondang asri Block b3 no 18 desa kondang jaya Rt:15 Rw:07, KAB. KARAWANG, KARAWANG TIMUR, JAWA BARAT, ID, 41371', 'Karawang', 'Perempuan', 'Islam', '-', '085778860767', 'dianwahyuningrun9@gmail.com', 'D3', 'Subang jawa barat ', 'Btcls', 'perawat/44.jpg', 'Kurang dari 1 tahun', '<2', 'Saya dianwahyuningrum umur 19 thn saya mahasiswa tingkat akhir di akper pemkab  subang saya pernah mengikuti pelatihan btcls terima kasih ', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, 1, 'Christy Yanhi', 'Jalan satria 4 no.26 grogol', 'Jakarta', 'Perempuan', 'Protestan', '-', '081370624844', 'christyyanhi1101@gmail.com', 'D3', 'Akper husada karya jaya', 'BTCLS', 'perawat/45.jpg', '1 - 5 tahun', '2,5 tahun', 'Saat ini masih bekerja d rs', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(285, 1, 'Asep Purnomosidi', 'Jln rhm noeradji sumur pancing karawaci tangerang kota banten ', 'Tangerang', 'Laki Laki', 'Islam', '-', '08978678369', 'aseppurnomosidi@ymail.com', 'S1', 'Universitas Aisyiyah Yogyakarta', 'Ppgd/btcls', 'perawat/46.jpg', '1 - 5 tahun', 'Pernah menjadi perawat bangsal, igd dan sekarang di kamar operasi', 'Saya adalah perawat yg jujur, rajin trampil ', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(286, 1, 'Yoga Jupiter Oksinadi', 'Jln.sultan mahmud badarudin II no 1 palembang', 'Palembang', 'Laki Laki', 'Islam', '-', '082175700996', 'yogajupiteroksinadi@gmail.com', 'D3', 'Akademi keperawatan kesdam II/sriwijaya', 'Seminar keperawatan ', 'perawat/47.jpg', 'Kurang dari 1 tahun', 'Belum ada hanya pengalaman selama praktek lapangan ', 'Saya ingin bergabung menjadi perawat ', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(287, 1, 'Dinda Anisa', 'jln wartawan 2 no 23 A turangga, lengkong, bandung', 'Jawa barat', 'Perempuan', 'Islam', '-', '08977658677', 'dindaannisawiharja6@gmail.com', 'D3', 'Politeknik Piksi Ganesha', 'belum dapat', 'perawat/48.jpg', 'Kurang dari 1 tahun', 'sangat menyenangkan', 'saya senang menjadi perawat, terutama jika pasiennya lucu seperti bayi', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(288, 1, 'Putri Melania Purba', 'jl bunga rampai x no 32 jakarta ', 'semarang ', 'Perempuan', 'Protestan', '-', '085780696439', 'putrimelania@gmail.com', 'S1', 'universitas diponegoro semarang', 'BTCLS ', 'perawat/49.jpg', 'Kurang dari 1 tahun', 'fresh graduate ', 'mudah bergaul, ramah, disiplin dan murah senyum ', 'Visit Perawat', 'Ya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(289, 1, 'Kristin Maranata', 'jl pedongkelan depan', 'jakarta barat', 'Perempuan', 'Protestan', '-', '081281759394', 'kristin.hasibuan@yahoo.com', 'D3', 'poltekkes kemenkes jakarta III', 'BTCL, ACLS', 'perawat/50.jpg', '1 - 5 tahun', 'perawat ICU', 'jujur dan pekerja keras', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(290, 1, 'Margaretha Eden Dinanti', 'Jl. Teluk Aru Utara 71', 'Surabaya', 'Perempuan', 'Protestan', '-', '082132856635', 'margarethaeden08@gmail.com', 'D3', 'STIKES Hang Tuah Surabaya', 'BTCLS', 'perawat/51.jpg', 'Kurang dari 1 tahun', 'Pernah bekerja di RS. William Booth Surabaya', 'Saya orangnya siap bekerja dengan tim, mudah adaptasi, bekerja keras, rajin, jujur, dan disiplin.', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(291, 1, 'Yuswandi', 'Jl.cigondewah kidul kota bandung', 'Tangerang', 'Laki Laki', 'Islam', '-', '085215214287', 'useonedie24@7gmail.com', 'S1', 'Stikes achmad yani cimahi', 'Pelatihan intensive care unit, pelatihan woundcare', 'perawat/52.jpg', '5 - 10 tahun', 'Mempunyai pengalaman di rs siloam selama 5 tahun', 'Sedang melanjutkan kuliah s2 keperawatan medikal bedah', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(292, 1, 'Nunu Nurholik', 'Dusun nanggela, Rt/rw 03/07, Desa linggapura, Kecamatan kawali, Kabupaten ciamis', 'Kawali', 'Laki Laki', 'Islam', '-', '0895378833084', 'hanuun17@gmail.com', 'SMK', 'Smk farmasi pasundan kawali', 'Sertifikat upk,ujikom', 'perawat/53.jpg', 'Kurang dari 1 tahun', 'Sebenarnaya saya seorang freshgraduate smk farmasi', 'Hanya berpengalaman di bidang farmasi khususnya di apotek', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(293, 1, 'Eva Wardah', 'Desa terkesi selatan kec.klambu grobogan jawa tengah', 'Jalan tanjung duren timur no.7', 'Perempuan', 'Islam', '-', '085641889916', 'ewardah9@gmail.com', 'D3', 'Akademi kebidanan al -fathonah jakarta', 'Btcls', 'perawat/54.jpg', '1 - 5 tahun', 'Menolong persalinan asuhan pada bayi baru lahir memeriksa kehamilan', 'Rajin dan ulet', 'Visit Bidan', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `perawats` (`id`, `user_id`, `nama`, `alamat`, `domisili`, `jenis_kelamin`, `agama`, `umur`, `telepon`, `email`, `pendidikan`, `institusi`, `sertifikasi`, `foto`, `mulai_aktif`, `pengalaman`, `deskripsi`, `jenis_perawatan`, `bersedia`, `created_at`, `updated_at`) VALUES
(294, 1, 'Wanda Tia Adela', 'Serukam Desa Pasti Jaya Kecamatan Samalantan Kabupaten Bengkayang kalbar', 'Serukam Bengkayang kalbar', 'Perempuan', 'Protestan', '-', '081344460195', 'wandatia1997adela@gmail.com', 'D3', 'Akper Bethesda Serukam', 'Strategi lulus asesmen dan kredensial bagi tenaga perawat serta sosialisasi sistem informasi keanggotaan (Sim K) ppni wilayah provinsi Kalimantan Barat (2Skp), keterampilan komunikasi dan sof', 'perawat/55.jpg', 'Kurang dari 1 tahun', 'Pengalaman praktek klinik di RS dan puskesmas, selama kuliah dan praktek belajar lapangan', 'Saya kurang bisa memilih dan mengornankan salah satu dari dua pilihan yang sama-sama baik tapi sifatnya bertentangan dilapangan . Misalnya antara keharusan selesai dengan tepat waktu secepat-cepatnya dan keharusan menghasilkan sesuatu yang sebaik-baiknya .', 'Visit Perawat', 'Tidak', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rekenings`
--

CREATE TABLE `rekenings` (
  `id` int(10) UNSIGNED NOT NULL,
  `owner_user_id` int(11) NOT NULL,
  `nama_bank` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_rekening` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rekenings`
--

INSERT INTO `rekenings` (`id`, `owner_user_id`, `nama_bank`, `nomor_rekening`, `nama`, `created_at`, `updated_at`) VALUES
(1, 1, 'BCA', '1630396341', 'Ogy Winenriandhika', '2018-06-20 17:00:00', NULL),
(2, 1, 'BNI', '496534811', 'Ogy Winenriandhika', '2018-06-20 17:00:00', NULL),
(3, 1, 'Mandiri', '1110000130001', 'Ogy Winenriandhika', '2018-06-20 17:00:00', NULL),
(4, 1, 'BTPN/Jenius', '90011659208', 'Ogy Winenriandhika', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rumah_sakits`
--

CREATE TABLE `rumah_sakits` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penanggungjawab` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon_penanggungjawab` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `membership_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rumah_sakits`
--

INSERT INTO `rumah_sakits` (`id`, `user_id`, `nama`, `alamat`, `telepon`, `email`, `penanggungjawab`, `telepon_penanggungjawab`, `membership_status`, `deskripsi`, `foto`, `created_at`, `updated_at`) VALUES
(1, 3, 'RS Hasan Sadikin', 'Bandung Kota, jalannya teuing dimana', '0226621234', 'rshs@mail.com', 'Rivo', '081290756513', '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', 'rs1.jpg', '2018-07-20 17:00:00', NULL),
(2, 4, 'RS Dustira', 'Cimahi Kota, jalannya teuing dimana', '0226621234', 'dustira@mail.com', 'Aink', '081290756513', '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n                Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', 'rs2.jpg', '2018-07-20 17:00:00', NULL),
(3, 5, 'RS Cibabat', 'Cimahi Kota, jalannya teuing dimana', '0226621234', 'cibabat@mail.com', 'Aink Oge', '081290756513', '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.\r\n\r\n                Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.', 'rs3.jpg', '2018-07-20 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hp` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `hp`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@perawatku.id', NULL, '$2y$10$UzHE5yi9NzL7TEt3fYXO1u.VoWcioWfVQaccG2qMxcwdPsVqiiu4G', 'admin', 'Joe0TBGun3LAOnxp2EcKKzJlfclGu7Vw5tzNdQ5HxFu5wVIY9sasN1Sd6BNi', NULL, NULL),
(2, 'iqbal', 'iqbal@example.com', NULL, '$2y$10$UzHE5yi9NzL7TEt3fYXO1u.VoWcioWfVQaccG2qMxcwdPsVqiiu4G', 'user', 'FbHXc3yTAKw8mRH9tPQ4i0NbJ4xpaoWpYDIUZPYTviVBiwgTlprJfwnbDzES', NULL, NULL),
(3, 'RS Hasan Sadikin', 'rshs@mail.com', NULL, '$2y$10$jY6FJN7WSyaAbsUl3fv0nO0XNjaTC6EQWTbAfMAHyALYaeBbdvxB.', 'owner', NULL, NULL, NULL),
(4, 'RS Dustira', 'dustira@mail.com', NULL, '$2y$12$fOXL3ML9yhTSz/AC9bj.YOX1LBYqZjVVW9DorhjGJwRJG0q2S9MPS', 'owner', NULL, NULL, NULL),
(5, 'RS Cibabat', 'cibabat@mail.com', NULL, '$2y$12$DElKnFxv6QSoWbvivzIcSOalcMvC1raPTfEgcuBiqMDkA1yfrQLG6.', 'owner', NULL, NULL, NULL),
(17, 'rivo', 'asd@mail.com', '123', '$2y$10$N6NfM.OLePLIIskSVqUvrekQtrfqjNxHqHNlo5AI/Xc.AxL0R5CSG', 'user', 'rGwQXmWwbR6l19KbCxsSsbtj6wX118XIWSZhvZru6H4LqC3QvYZJaxYa69Gu', '2018-08-13 07:14:18', '2018-08-13 07:14:18');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_ktp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_hp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `nama`, `foto`, `alamat`, `nomor_ktp`, `nomor_hp`, `created_at`, `updated_at`) VALUES
(1, 2, 'Muhammad Iqbal', 'icons/avatar.jpg', 'Graha Bukit Raya I Jl.Bukit Resik VI Blok G8 No. 6', '17129400010', '081290756513', '2018-07-20 17:00:00', NULL),
(26, 17, 'rivo', NULL, '123', '123', '123', '2018-08-13 07:14:18', '2018-08-13 07:14:38'),
(28, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 07:23:35', '2018-08-13 07:23:35'),
(29, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 07:27:09', '2018-08-13 07:27:09'),
(30, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 07:48:27', '2018-08-13 07:48:27'),
(31, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 07:50:09', '2018-08-13 07:50:09'),
(32, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:02:29', '2018-08-13 08:02:29'),
(33, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:02:58', '2018-08-13 08:02:58'),
(34, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:03:06', '2018-08-13 08:03:06'),
(35, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:04:03', '2018-08-13 08:04:03'),
(36, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:07:54', '2018-08-13 08:07:54'),
(37, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:08:46', '2018-08-13 08:08:46'),
(38, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:14:21', '2018-08-13 08:14:21'),
(39, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:15:29', '2018-08-13 08:15:29'),
(40, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:16:27', '2018-08-13 08:16:27'),
(41, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:16:59', '2018-08-13 08:16:59'),
(42, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:17:36', '2018-08-13 08:17:36'),
(43, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:18:19', '2018-08-13 08:18:19'),
(44, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:21:17', '2018-08-13 08:21:17'),
(45, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:30:21', '2018-08-13 08:30:21'),
(46, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:30:49', '2018-08-13 08:30:49'),
(47, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:32:39', '2018-08-13 08:32:39'),
(48, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:33:14', '2018-08-13 08:33:14'),
(49, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 08:33:31', '2018-08-13 08:33:31'),
(50, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 14:29:07', '2018-08-13 14:29:07'),
(51, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 14:34:39', '2018-08-13 14:34:39'),
(52, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-13 14:37:58', '2018-08-13 14:37:58'),
(53, 17, 'rivo', NULL, '123', NULL, '123', '2018-08-21 02:49:45', '2018-08-21 02:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `user_pasiens`
--

CREATE TABLE `user_pasiens` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `tipe_pasien` int(11) NOT NULL,
  `nama_pasien` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat_pasien` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_pasien` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_hp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `berat_badan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tinggi_badan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `umur` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kondisi_medis` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_reviews`
--

CREATE TABLE `user_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `owner_user_id` int(11) NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_reviews`
--

INSERT INTO `user_reviews` (`id`, `user_id`, `booking_id`, `owner_user_id`, `review`, `rating`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 3, 'mantap, bagoos', 5, '2018-07-20 17:00:00', NULL),
(2, 2, 2, 3, 'mantap, bagoos', 5, '2018-07-20 17:00:00', NULL),
(3, 2, 3, 3, 'mantap, bagoos', 5, '2018-07-20 17:00:00', NULL),
(4, 2, 4, 3, 'mantap, bagoos', 5, '2018-07-20 17:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kodeunik` (`kodeunik`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `layanans`
--
ALTER TABLE `layanans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pasiens`
--
ALTER TABLE `pasiens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `perawats`
--
ALTER TABLE `perawats`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `perawats_email_unique` (`email`);

--
-- Indexes for table `rekenings`
--
ALTER TABLE `rekenings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rumah_sakits`
--
ALTER TABLE `rumah_sakits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_pasiens`
--
ALTER TABLE `user_pasiens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_reviews`
--
ALTER TABLE `user_reviews`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_logs`
--
ALTER TABLE `activity_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `layanans`
--
ALTER TABLE `layanans`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `pasiens`
--
ALTER TABLE `pasiens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `perawats`
--
ALTER TABLE `perawats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=295;

--
-- AUTO_INCREMENT for table `rekenings`
--
ALTER TABLE `rekenings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rumah_sakits`
--
ALTER TABLE `rumah_sakits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `user_pasiens`
--
ALTER TABLE `user_pasiens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_reviews`
--
ALTER TABLE `user_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
