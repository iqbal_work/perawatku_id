<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerawatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perawats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('nama');
            $table->string('alamat');
            $table->string('domisili')->nullable();;
            $table->string('jenis_kelamin')->nullable();;
            $table->string('agama')->nullable();;
            $table->string('umur');
            $table->string('telepon');
            $table->string('email')->unique();
            $table->string('pendidikan')->nullable();;
            $table->string('institusi')->nullable();;
            $table->string('sertifikasi');
            $table->string('foto');
            $table->string('mulai_aktif')->nullable();;
            $table->text('pengalaman')->nullable();;
            $table->text('deskripsi');
            $table->string('jenis_perawatan')->nullable();;
            $table->string('bersedia')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perawats');
    }
}
