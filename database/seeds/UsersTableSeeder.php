<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@perawatku.id',
            'password' => '$2y$10$UzHE5yi9NzL7TEt3fYXO1u.VoWcioWfVQaccG2qMxcwdPsVqiiu4G', //admin
            'role' => 'admin'
        ]);

        DB::table('users')->insert([
            'name' => 'iqbal',
            'email' => 'iqbal@example.com',
            'password' => '$2y$10$UzHE5yi9NzL7TEt3fYXO1u.VoWcioWfVQaccG2qMxcwdPsVqiiu4G', //admin
            'role' => 'user'
        ]);

        DB::table('users')->insert([
            'name' => 'RS Hasan Sadikin',
            'email' => 'rshs@mail.com',
            'password' => '$2y$10$jY6FJN7WSyaAbsUl3fv0nO0XNjaTC6EQWTbAfMAHyALYaeBbdvxB.', //rshs123
            'role' => 'owner'
        ]);

        DB::table('users')->insert([
            'name' => 'RS Dustira',
            'email' => 'dustira@mail.com',
            'password' => '$2y$12$fOXL3ML9yhTSz/AC9bj.YOX1LBYqZjVVW9DorhjGJwRJG0q2S9MPS', //dustira
            'role' => 'owner'
        ]);

        DB::table('users')->insert([
            'name' => 'RS Cibabat',
            'email' => 'cibabat@mail.com',
            'password' => '$2y$12$DElKnFxv6QSoWbvivzIcSOalcMvC1raPTfEgcuBiqMDkA1yfrQLG6.', //cibabat
            'role' => 'owner'
        ]);
    }
}
