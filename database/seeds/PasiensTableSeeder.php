<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PasiensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('pasiens')->insert([
            'user_id' => '2',
            'booking_id' => '1',
            'tipe_pasien' => '1',
            'nama_pasien' => 'Iqbal',
            'alamat_pasien' => 'Indonesia',
            'email_pasien' => 'mantap@jos.gandos',
            'nomor_hp' => '081231231',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('pasiens')->insert([
            'user_id' => '2',
            'booking_id' => '2',
            'tipe_pasien' => '2',
            'nama_pasien' => 'John',
            'alamat_pasien' => 'Indonesia',
            'email_pasien' => 'mantap@jos.gandos',
            'nomor_hp' => '081231231',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('pasiens')->insert([
            'user_id' => '2',
            'booking_id' => '3',
            'tipe_pasien' => '1',
            'nama_pasien' => 'Iqbal',
            'alamat_pasien' => 'Indonesia',
            'email_pasien' => 'mantap@jos.gandos',
            'nomor_hp' => '081231231',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('pasiens')->insert([
            'user_id' => '2',
            'booking_id' => '4',
            'tipe_pasien' => '2',
            'nama_pasien' => 'John',
            'alamat_pasien' => 'Indonesia',
            'email_pasien' => 'mantap@jos.gandos',
            'nomor_hp' => '081231231',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);
    }
}
