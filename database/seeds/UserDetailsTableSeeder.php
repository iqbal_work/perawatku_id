<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user_details')->insert([
            'user_id' => '2',
            'nama' => 'Muhammad Iqbal',
            'foto' => 'placeholder_square.jpg',
            'alamat' => 'Graha Bukit Raya I Jl.Bukit Resik VI Blok G8 No. 6',
            'nomor_ktp' => '17129400010',
            'nomor_hp' => '081290756513',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);
    }
}
