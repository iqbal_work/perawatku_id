<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class FeesTableSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
        DB::table('fees')->insert([
            'perawat_id' => '6',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '6',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '6',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '6',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '6',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '6',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '6',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        //-------------------------------------------------------------------------------------------------
        DB::table('fees')->insert([
            'perawat_id' => '7',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '7',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '7',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '7',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '7',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '7',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '7',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        //-------------------------------------------------------------------------------------------//

        DB::table('fees')->insert([
            'perawat_id' => '8',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '8',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '8',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '8',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '8',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '8',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '8',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        //---------------------------------------------------------------------------------------------//

        DB::table('fees')->insert([
            'perawat_id' => '9',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '9',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '9',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '9',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '9',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '9',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '9',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);
        //--------------------------------------------------------------------------------------------//

        DB::table('fees')->insert([
            'perawat_id' => '10',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '10',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '10',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '10',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '10',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '10',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '10',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);
    }
}
