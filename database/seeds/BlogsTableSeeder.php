<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('blogs')->insert([
            'user_id' => '1',
            'judul' => 'Blog 1',
            'gambar' => 'blog1.jpg',
            'meta' => 'Blog ini adalah uji coba pertama ',
            'konten' => 'Blog ini adalah uji coba pertama untuk kemaslahatan umat di bulan ramadhan bro cus meong',
 			'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('blogs')->insert([
            'user_id' => '1',
            'judul' => 'Blog 2',
            'gambar' => 'blog2.jpg',
            'meta' => 'Blog ini adalah uji coba pertama ',
            'konten' => 'Blog ini adalah uji coba pertama untuk kemaslahatan umat di bulan ramadhan bro cus meong',
 			'created_at' => Carbon::now()->format('Y-m-d')
        ]);
    }
}
