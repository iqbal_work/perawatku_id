<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class FeesTableSeeder3 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
        DB::table('fees')->insert([
            'perawat_id' => '11',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '11',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '11',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '11',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '11',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '11',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '11',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        //-------------------------------------------------------------------------------------------------
        DB::table('fees')->insert([
            'perawat_id' => '12',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '12',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '12',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '12',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '12',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '12',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '12',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        //-------------------------------------------------------------------------------------------//

        DB::table('fees')->insert([
            'perawat_id' => '13',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '13',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '13',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '13',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '13',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '13',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '13',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        //---------------------------------------------------------------------------------------------//

        DB::table('fees')->insert([
            'perawat_id' => '14',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '14',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '14',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '14',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '14',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '14',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '14',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);
        //--------------------------------------------------------------------------------------------//

        DB::table('fees')->insert([
            'perawat_id' => '15',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '15',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '15',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '15',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '15',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '15',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '15',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);
    }
}
