<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BookingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('bookings')->insert([
            'kode_transaksi' => '1_1',
            'user_detail_id' => '1',
            'owner_user_id' => '3',
            'perawat_id' => '4',
            'layanan' => '1',
            'tanggal_order' => Carbon::now()->format('Y-m-d')
        ]);
        DB::table('bookings')->insert([
            'kode_transaksi' => '1_2',
            'user_detail_id' => '1',
            'owner_user_id' => '3',
            'perawat_id' => '4',
            'layanan' => '1',
            'tanggal_order' => Carbon::now()->format('Y-m-d')
        ]);
        DB::table('bookings')->insert([
            'kode_transaksi' => '1_3',
            'user_detail_id' => '1',
            'owner_user_id' => '3',
            'perawat_id' => '4',
            'layanan' => '1',
            'tanggal_order' => Carbon::now()->format('Y-m-d')
        ]);
        DB::table('bookings')->insert([
            'kode_transaksi' => '1_4',
            'user_detail_id' => '1',
            'owner_user_id' => '3',
            'perawat_id' => '4',
            'layanan' => '1',
            'tanggal_order' => Carbon::now()->format('Y-m-d')
        ]);
    }
}
