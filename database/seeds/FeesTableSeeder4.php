<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class FeesTableSeeder4 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('fees')->insert([
            'perawat_id' => '16',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '16',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '16',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '16',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '16',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '16',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '16',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '17',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '17',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '17',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '17',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '17',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '17',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '17',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '18',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '18',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '18',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '18',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '18',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '18',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '18',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '19',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '19',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '19',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '19',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '19',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '19',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '19',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '20',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '20',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '20',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '20',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '20',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '20',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '20',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);
    }
}
