<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RekeningsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('rekenings')->insert([
            'owner_user_id' => '3',
            'nama_bank' => 'BCA',
            'nomor_rekening' => '1234567890',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('rekenings')->insert([
            'owner_user_id' => '4',
            'nama_bank' => 'BCA',
            'nomor_rekening' => '1234567890',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('rekenings')->insert([
            'owner_user_id' => '5',
            'nama_bank' => 'BCA',
            'nomor_rekening' => '1234567890',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);
    }
}
