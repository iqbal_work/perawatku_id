<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class FeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('fees')->insert([
            'perawat_id' => '1',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '1',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '1',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '1',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '1',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '1',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '1',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        //-------------------------------------------------------------------------------------------------
        DB::table('fees')->insert([
            'perawat_id' => '2',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '2',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '2',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '2',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '2',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '2',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '2',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        //-------------------------------------------------------------------------------------------//

        DB::table('fees')->insert([
            'perawat_id' => '3',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '3',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '3',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '3',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '3',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '3',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '3',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        //---------------------------------------------------------------------------------------------//

        DB::table('fees')->insert([
            'perawat_id' => '4',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '4',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '4',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '4',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '4',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '4',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '4',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);
        //--------------------------------------------------------------------------------------------//

        DB::table('fees')->insert([
            'perawat_id' => '5',
            'layanan' => 'Home Visit',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '5',
            'layanan' => 'Paket - Homecare 12 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '5',
            'layanan' => 'Paket - Homecare 8 Jam Kerja Per Hari Selama 1 Bulan',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '5',
            'layanan' => 'Paket - Homecare Standby 24 Jam Selama 1 Bulan',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '5',
            'layanan' => 'Pemeriksaan Gula Darah, Kolesterol, Asam Urat dan Konsultasi',
            'biaya' => '150000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '5',
            'layanan' => 'Pemeriksaan Tanda-Tanda Vital (Tekanan Darah, Nadi, Nafas) dan Konsultasi',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('fees')->insert([
            'perawat_id' => '5',
            'layanan' => 'Perawatan Pasien Lansia',
            'biaya' => '50000',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);
    }
}
