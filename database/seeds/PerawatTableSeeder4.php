<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PerawatTableSeeder4 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('perawats')->insert([
            'user_id' => '1',
            'nama' => 'John Doe',
            'alamat' => 'Indoenesia',
            'umur' => '12',
            'telepon' => '0226621234', //admin
            'email' => 'jd16@mail.com',
            'pendidikan' => '123123123213',
            'institusi' => '123444444',
            'sertifikasi' => 'Cisco',
            'foto' => 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png',
            'pengalaman' => '4',
            'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.

				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('perawats')->insert([
            'user_id' => '1',
            'nama' => 'John Doe',
            'alamat' => 'Indoenesia',
            'umur' => '12',
            'telepon' => '0226621234', //admin
            'email' => 'jd17@mail.com',
            'pendidikan' => '123123123213',
            'institusi' => '123444444',
            'sertifikasi' => 'Cisco',
            'foto' => 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png',
            'pengalaman' => '4',
            'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.

				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('perawats')->insert([
            'user_id' => '1',
            'nama' => 'John Doe',
            'alamat' => 'Indoenesia',
            'umur' => '12',
            'telepon' => '0226621234', //admin
            'email' => 'jd18@mail.com',
            'pendidikan' => '123123123213',
            'institusi' => '123444444',
            'sertifikasi' => 'Cisco',
            'foto' => 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png',
            'pengalaman' => '4',
            'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.

				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('perawats')->insert([
            'user_id' => '1',
            'nama' => 'John Doe',
            'alamat' => 'Indoenesia',
            'umur' => '12',
            'telepon' => '0226621234', //admin
            'email' => 'jd19@mail.com',
            'pendidikan' => '123123123213',
            'institusi' => '123444444',
            'sertifikasi' => 'Cisco',
            'foto' => 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png',
            'pengalaman' => '4',
            'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.

				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('perawats')->insert([
            'user_id' => '1',
            'nama' => 'John Doe',
            'alamat' => 'Indoenesia',
            'umur' => '12',
            'telepon' => '0226621234', //admin
            'email' => 'jd20@mail.com',
            'pendidikan' => '123123123213',
            'institusi' => '123444444',
            'sertifikasi' => 'Cisco',
            'foto' => 'perawat/XOPvYVcISETqkK4TdOAAWQSHlCjwvvdFokvf6M8u.png',
            'pengalaman' => '4',
            'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis diam id odio posuere dapibus dignissim nec felis. Nunc ultrices quis nisl placerat facilisis. Nam facilisis ex id turpis accumsan venenatis. Suspendisse convallis velit eget est efficitur, a consectetur neque faucibus. Ut eu risus ullamcorper, egestas purus vel, congue odio. Mauris ac tincidunt ante. Curabitur cursus viverra cursus. Mauris et lacus maximus, sodales ex eu, fringilla nisi. Curabitur tincidunt velit urna, id finibus enim tristique eu. Pellentesque egestas at odio eu fringilla. Sed eu lectus tortor.

				Nulla sit amet lorem ut neque luctus rhoncus. Vestibulum vel nunc dui. Donec rutrum risus eget purus commodo, sed cursus justo commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur finibus maximus metus, a congue neque condimentum ac. Aliquam erat volutpat. Vestibulum vel tristique mauris, consequat suscipit tellus. Ut a erat finibus, rutrum turpis vehicula, venenatis mauris. Phasellus vitae felis a ante semper interdum. Sed non porttitor dolor. Aenean in augue libero.',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);
    }
}
