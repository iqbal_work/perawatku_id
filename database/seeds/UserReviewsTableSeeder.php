<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user_reviews')->insert([
            'user_id' => '2',
            'booking_id' => '1',
            'owner_user_id' => '3',
            'review' => 'mantap, bagoos',
            'rating' => '5',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('user_reviews')->insert([
            'user_id' => '2',
            'booking_id' => '2',
            'owner_user_id' => '3',
            'review' => 'mantap, bagoos',
            'rating' => '5',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('user_reviews')->insert([
            'user_id' => '2',
            'booking_id' => '3',
            'owner_user_id' => '3',
            'review' => 'mantap, bagoos',
            'rating' => '5',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);

        DB::table('user_reviews')->insert([
            'user_id' => '2',
            'booking_id' => '4',
            'owner_user_id' => '3',
            'review' => 'mantap, bagoos',
            'rating' => '5',
            'created_at' => Carbon::now()->format('Y-m-d')
        ]);
    }
}
