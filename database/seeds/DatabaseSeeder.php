<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RumahSakitTableSeeder::class);
        $this->call(BookingsTableSeeder::class);
        $this->call(FeesTableSeeder::class);
        $this->call(FeesTableSeeder2::class);
        $this->call(FeesTableSeeder3::class);
        $this->call(FeesTableSeeder4::class);
        $this->call(PasiensTableSeeder::class);
        $this->call(PerawatsTableSeeder::class);
        $this->call(PerawatTableSeeder2::class);
        $this->call(PerawatTableSeeder3::class);
        $this->call(PerawatTableSeeder4::class);
        $this->call(UserDetailsTableSeeder::class);
        $this->call(UserReviewsTableSeeder::class);
        $this->call(BlogsTableSeeder::class);
        $this->call(RekeningsTableSeeder::class);
    }
}
