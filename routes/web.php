<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
/*Route::post('/', function() {
    echo "<script type='text/javascript'>alert('Berhasil. Terima kasih telah berlangganan email Perawatku.id');
    window.location='/';
    </script>"; 
});*/

//Auth::routes();
//Route::get('/register', 'RegistrationsController@create');
Route::post('/register', 'RegistrationsController@store');
//Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');

Route::post('/subscribe', 'HomeController@subscribe');
Route::post('/subscribe-footer', 'HomeController@subscribeFooter');

/*Route::get('/gabung-perawatku', function () { return view('pages.gabung-perawatku'); });*/
Route::get('/perawat', 'PerawatController@display');
Route::post('/perawat', 'PerawatController@search');

Route::get('/layanan', function () { return view('user.layanan'); });
Route::get('/layanan/tindakan', function () { return view('user.tindakan'); });
Route::get('/layanan/tindakan/pesan', 'BookingController@tindakan');
Route::get('/layanan/perawatan-bayi', function () { return view('user.perawatan-bayi'); });
Route::get('/layanan/perawatan-bayi/pesan', 'BookingController@layananBayi');
Route::get('/layanan/perawatan-luka', function () { return view('user.perawatan-luka'); });
Route::get('/layanan/perawatan-luka/pesan', 'BookingController@layananLuka');
Route::get('/layanan/perawatan-medis', function () { return view('user.perawatan-medis'); });
Route::get('/layanan/perawatan-medis/pesan', 'BookingController@layananMedis');
Route::get('/layanan/perawatan-paliatif', function () { return view('user.perawatan-paliatif'); });
Route::get('/layanan/perawatan-paliatif/pesan', 'BookingController@layananPaliatif');
Route::post('/pemesanan-layanan', 'BookingController@store2');

Route::get('/pemesanan/pembayaran/{booking}', 'BookingController@pembayaran');
Route::get('/pemesanan/konfirmasi/{booking}', 'BookingController@konfirmasi');
Route::get('/pemesanan/invoice/{booking}', 'BookingController@invoice');

Route::get('/rumah-sakit', 'RumahSakitController@display');
Route::get('/rumah-sakit/{rumahsakit}', 'RumahSakitController@show');
/*Route::post('/pemesanan', 'BookingController@store');
Route::get('/pemesanan/konfirmasi/{booking}', 'BookingController@bukti');
Route::get('/pemesanan/pembayaran/{booking}', 'BookingController@konfirmasi');
Route::get('/pemesanan/{perawat}', 'BookingController@create');
Route::get('/pemesanan-perawat/{perawat}', 'BookingController@create2');
Route::get('/pemesanan/bukti-pembayaran/{booking}', 'DashboardController@show');*/

/*Route::get('/pemesanan/pembayaran/{booking}', 'BookingController@bukti');*/
/*Route::get('/pemesanan-layanan', 'BookingController@create3');*/
Route::get('/pemesanan/bukti-pembayaran/{booking}', 'DashboardController@show');

Route::get('/404', function () { return view('pages.404'); });
Route::get('/faq', function () { return view('pages.faq'); });
Route::get('/syarat-ketentuan', function () { return view('pages.tos'); });
Route::get('/kontak',  function () { return view('pages.contact'); });
Route::post('/kirim-pesan', 'HomeController@contact');

Route::get('/profil', 'BookingController@show_user');
Route::get('/profil/ulasan', 'UserReviewController@index');
Route::get('/profil/pengaturan', 'UserDetailController@show');
Route::post('/profil/pengaturan/ubah-password', 'RegistrationsController@update');
Route::post('/profil/pasien/', 'PasienController@store');
Route::patch('/profil/pasien', 'PasienController@update');
Route::get('/profil/pasien', 'PasienController@show');
Route::post('/profil/pengaturan', 'UserDetailController@store');
Route::patch('/profil/pengaturan', 'UserDetailController@update');

Route::middleware('testadmin')->get('/admin/dashboard', 'DashboardController@index');
Route::get('/admin/pemesanan', 'BookingController@index');

// filtering booking
Route::get('/admin/pemesanan/diterima', 'BookingController@diterima');
Route::get('/admin/pemesanan/belum-diproses', 'BookingController@belum_diproses');
Route::get('/admin/pemesanan/ditolak', 'BookingController@ditolak');
Route::get('/admin/pemesanan/cari', 'BookingController@cari');

// accept or decline booking
Route::get('/admin/pemesanan/tolak/{booking}', 'BookingController@booking_ditolak');
Route::get('/admin/pemesanan/terima/{booking}', 'BookingController@booking_diterima');
// nanti ini tolong diganti,harusnya diganti jadi route::post. yang buatnya belum ngerti caranya update pake button yang gampang, cssnya rusak terus
Route::get('/admin/pembayaran/terima/{booking}', 'BookingController@pembayaran_diterima');

Route::get('/admin/pemesanan/detail/{booking}', 'BookingController@show');

Route::get('/admin/perawat/daftar-perawat', 'PerawatController@index');
Route::get('/admin/perawat/detail-perawat/{perawat}', 'PerawatController@show');

Route::get('/admin/perawat/hapus-perawat/{perawat}', 'PerawatController@destroy');
Route::get('/admin/perawat/edit-perawat/{perawat} ', 'PerawatController@edit');
Route::patch('/admin/perawat/edit-perawat/', 'PerawatController@update');

Route::post('/admin/perawat/tambah-fee', 'FeeController@store');
Route::patch('/admin/perawat/tambah-fee', 'FeeController@update');
Route::get('/admin/perawat/delete-fee/{fee} ', 'FeeController@destroy');

Route::get('/admin/perawat/tambah-perawat', function() {return view('admin.tambah-perawat'); });
Route::post('/admin/perawat/tambah-perawat', 'PerawatController@store');
Route::get('/admin/perawat/ulasan-perawat', 'UserReviewController@show');
//Route::get('/admin/perawat/detail-perawat', function () { return view('admin.detail-perawat'); });
Route::get('/admin/rs/profil-rs', 'RumahSakitController@index');
Route::post('/admin/rs/profil-rs', 'RumahSakitController@create');
Route::patch('/admin/rs/profil-rs', 'RumahSakitController@update');
Route::get('/admin/rs/profil-rs/delete/{rekening}', 'RekeningController@delete');
Route::get('/admin/rs/upgrade-akun', function () { return view('admin.upgrade-akun'); });
Route::post('/admin/rs/upgrade-akun', 'RumahSakitController@membership');

Route::get('/admin/rs/ubah-password', function () { return view('admin.ubah-password'); });
Route::post('/admin/rs/ubah-password', 'RegistrationsController@update');

/* -- SUPER ADMIN -- */

Route::get('/superadmin/dashboard', 'SuperadminController@dashboard');
Route::get('/superadmin/pemesanan', 'SuperadminController@pemesanan');

// filtering booking
Route::get('/superadmin/pemesanan/diterima', 'SuperadminController@pemesananditerima');
Route::get('/superadmin/pemesanan/belum-diproses', 'SuperadminController@pemesananbelumdiproses');
Route::get('/superadmin/pemesanan/ditolak', 'SuperadminController@pemesananditolak');
Route::get('/superadmin/pemesanan/cari', 'SuperadminController@cari');

Route::get('/superadmin/pemesanan/detail/{booking}', 'SuperadminController@detailpesanan');
Route::get('/superadmin/perawat/daftar-perawat', 'SuperadminController@listperawat');
Route::get('/superadmin/perawat/detail-perawat/{perawat}', 'SuperadminController@detailperawat');
Route::get('/superadmin/perawat/edit-perawat/{perawat}', 'SuperadminController@editperawat');
Route::get('/superadmin/perawat/delete-perawat/{perawat}', 'SuperadminController@deleteperawat');
Route::patch('/superadmin/perawat/edit-perawat', 'SuperadminController@updateperawat');
Route::get('/superadmin/perawat/ulasan-perawat', 'SuperadminController@ulasanperawat');
Route::get('/superadmin/perawat/tambah-perawat', function () { return view('superadmin.tambah-perawat'); });
Route::post('/superadmin/perawat/tambah-perawat', 'SuperadminController@tambahperawat');
Route::get('/superadmin/ubah-password', function () { return view('superadmin.ubah-password'); });
Route::post('/superadmin/ubah-password', 'RegistrationsController@update');

Route::get('/superadmin/pemesanan/tolak/{booking}', 'SuperadminController@tolakpesanan');
Route::get('/superadmin/pemesanan/terima/{booking}', 'SuperadminController@terimapesanan');
Route::get('/superadmin/pembayaran/terima/{booking}', 'SuperadminController@terimapembayaran');

Route::post('/superadmin/perawat/tambah-fee', 'SuperadminController@createfee');
Route::patch('/superadmin/perawat/tambah-fee', 'SuperadminController@updatefee');
Route::get('/superadmin/perawat/delete-fee/{fee} ', 'SuperadminController@deletefee');

Route::get('/superadmin/blog', 'BlogsController@index');
Route::get('/superadmin/blog/edit/{blog}', 'BlogsController@edit');
Route::patch('/superadmin/blog/edit/{blog}', 'BlogsController@update');
Route::get('/superadmin/blog/add', 'BlogsController@add');
Route::post('/superadmin/blog/add', 'BlogsController@store');
Route::get('/superadmin/blog/delete/{blog}', 'BlogsController@destroy');

Route::get('/superadmin/rumah-sakit', 'RumahSakitController@premiumConfirmation');
Route::get('/superadmin/rumah-sakit/terima/{rumahsakit}', 'RumahSakitController@terimaPremium');
Route::get('/superadmin/rumah-sakit/tolak/{rumahsakit}', 'RumahSakitController@tolakPremium');

// filtering booking
Route::get('/superadmin/rumah-sakit/free-account', 'SuperadminController@rumkitnormal');
Route::get('/superadmin/rumah-sakit/premium-account', 'SuperadminController@rumkitpremium');
Route::get('/superadmin/rumah-sakit/mengajukan-premium', 'SuperadminController@rumkitapproval');

Route::get('/blog', 'BlogsController@read');
Route::get('/blog/{blog}', 'BlogsController@show');

Route::get('/sitemap.xml', 'SitemapController@index');

Route::get('auth/{provider}', 'HomeController@redirectToProvider');
Route::get('auth/{provider}/callback', 'HomeController@handleProviderCallback');