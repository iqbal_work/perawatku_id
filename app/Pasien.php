<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
	protected $fillable = [
        'user_id', 'booking_id', 'tipe_pasien' ,'nama_pasien', 'alamat_pasien', 'email_pasien', 'nomor_hp', 'berat_badan', 'tinggi_badan', 'umur', 'suku', 'agama', 'kondisi_medis'
    ];
    //
	public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function booking()
    {
    	return $this->hasOne(Booking::class);
    }
}
