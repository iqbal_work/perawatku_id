<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
	protected $fillable = [
        'user_id','nama','alamat', 'nomor_hp', 'nomor_ktp'
    ];
    //
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function booking()
    {
    	return $this->hasMany(Booking::class);
    }
}
