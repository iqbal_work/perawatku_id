<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'hp', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function pasien()
    {
        return $this->hasMany(Pasien::class);
    }

    public function userDetail()
    {
        return $this->hasOne(UserDetail::class);
    }

    public function perawat()
    {
        return $this->hasMany(Perawat::class);
    }
}
