<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //
    protected $fillable = [
        'jenis_booking','kode_transaksi', 'user_detail_id', 'owner_user_id', 'perawat_id', 'layanan', 'tindakan', 'tanggal_order', 'biaya', 'kodeunik', 'booking_status', 'payment_status'
    ];

    protected $dates = ['tanggal_order'];

    public function userDetail()
    {
        return $this->belongsTo(UserDetail::class);
    }

    public function rumahSakit()
    {
    	return $this->belongsTo(RumahSakit::class);
    }

    public function perawat()
    {
    	return $this->belongsTo(Perawat::class);
    }

    public function pasien()
    {
        return $this->belongsTo(Pasien::class);
    }
    public function userReview()
    {
        return $this->hasOne(UserReview::class);
    }
}
