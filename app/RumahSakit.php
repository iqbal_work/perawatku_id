<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RumahSakit extends Model
{
    protected $fillable = [
        'nama', 'alamat', 'telepon', 'email', 'penanggungjawab', 'telepon_penanggungjawab', 'deskripsi', 'foto'
    ];

    // public function __construct()
    // {
    //     return Auth::user()->role;
    //     if (Auth::user()->role == 'owner') {
    //         return redirect(url('/404'));
    //     }
    // }

    //
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function booking()
    {
    	return $this->hasMany(Booking::class);
    }

    public function userReview()
    {
        return $this->hasMany(UserReview::class);
    }

     public function activityLog()
    {
        return $this->hasMany(ActivityLog::class);
    }
}
