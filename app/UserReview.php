<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReview extends Model
{
    //
    public function booking()
    {
    	return $this->belongsTo(Booking::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
