<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perawat extends Model
{
    //
    protected $fillable = [
        'user_id', 'nama', 'alamat', 'domisili', 'jenis_kelamin', 'agama', 'umur', 'telepon', 'email', 'pendidikan', 'institusi', 'sertifikasi', 'foto', 'mulai_aktif', 'pengalaman', 'deskripsi', 'jenis_perawatan', 'bersedia'
    ];

    public function user()
    {
    	return $this->hasOne(RumahSakit::class);
    }

    public function booking()
    {
    	return $this->hasMany(Booking::class);
    }

    public function fee()
    {
        return $this->hasMany(Fee::class);
    }
}
