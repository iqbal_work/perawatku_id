<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    //

	protected $fillable = [
        'perawat_id','layanan', 'biaya'
    ];

    public function perawat()
    {
    	return $this->belongsTo(Perawat::class);
    }
}
