<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Perawat;
use App\Pasien;
use App\RumahSakit;
use App\Layanan;
use App\UserReview;
use App\UserDetail;
use App\Booking;
use App\ActivityLog;
use App\Fee;

class SuperadminController extends Controller
{
    //
    public function Dashboard()
    {
    	$perawat = count(Perawat::where('user_id', Auth::id())->get());
    	$review = count(UserReview::where('owner_user_id', Auth::id())->get());
    	$booking = count(Booking::where('owner_user_id', Auth::id())->get());
    	$activity = ActivityLog::where('user_id', Auth::id())->orderBy('id', 'desc')->take(10)->get();

    	$join = Booking::with('userDetail')->where('owner_user_id', Auth::id())->orderBy('id', 'desc')->take(2)->get();
        /*foreach ($join as $key) {
            $key['fee'] = Fee::where('perawat_id', $key->perawat_id)->first()->biaya;
        }*/

        //return $join;
    	return view('superadmin.dashboard', compact('perawat', 'review', 'booking', 'activity', 'join'));
    }

    public function Pemesanan()
    {
    	$join = Booking::with('userDetail')->where('owner_user_id', Auth::id())->orderBy('id', 'desc')->paginate(10);
    	foreach ($join as $key) {
            $key['pasien'] = Pasien::where('booking_id', $key->id)->first();
            $key['layanan'] = Layanan::where('id', $key->layanan)->first();
        }
        $status = 0;
        //return $key;
        //return $join;
    	return view('superadmin.pemesanan', compact('join', 'status'));
    }

    public function PemesananDiterima()
    {
        $join = Booking::with('userDetail', 'perawat')->where('owner_user_id', Auth::id())->orderBy('id', 'desc')->where('booking_status', '1')->paginate(10);
        foreach ($join as $key) {
            $key['pasien'] = Pasien::where('booking_id', $key->id)->first();
        }
        $status = 1;
        return view('superadmin.pemesanan', compact('join', 'status'));
    }

    public function TerimaPesanan(Booking $booking)
    {
        $booking->booking_status = "1";
        $booking->update();
        return redirect(url('/superadmin/pemesanan/'));
    }

    public function TolakPesanan(Booking $booking)
    {
        $booking->booking_status = "2";
        $booking->update();
        return redirect(url('/superadmin/pemesanan/'));
    }

    public function TerimaPembayaran(Booking $booking)
    {
        $booking->payment_status = "1";
        $booking->update();
        return redirect(url('/superadmin/pemesanan/'));
    }

    public function PemesananDitolak()
    {
        $join = Booking::with('userDetail', 'perawat')->where('owner_user_id', Auth::id())->orderBy('id', 'desc')->where('booking_status', '2')->paginate(10);
        foreach ($join as $key) {
            $key['pasien'] = Pasien::where('booking_id', $key->id)->first();
        }
        $status = 2;
        return view('superadmin.pemesanan', compact('join', 'status'));
    }

    public function PemesananBelumDiproses()
    {
        $join = Booking::with('userDetail', 'perawat')->where('owner_user_id', Auth::id())->orderBy('id', 'desc')->where('booking_status', '0')->paginate(10);
        foreach ($join as $key) {
            $key['pasien'] = Pasien::where('booking_id', $key->id)->first();
        }
        $status = 3;
        return view('superadmin.pemesanan', compact('join', 'status'));
    }

    public function DetailPesanan(Booking $booking)
    {
    	$join = Booking::with('userDetail','perawat')->where('id', $booking->id)->first();
        $join['pasien'] = Pasien::where('booking_id', $join->id)->first();
        $join['fee'] = Fee::where('perawat_id', $join->perawat->id)->first();

        //return $join;
        return view('superadmin.pemesanan-detail',compact('join'));
    }

    public function ListPerawat()
    {
        $perawats = Perawat::where('user_id', Auth::id())->orderBy('id', 'desc')->paginate(10);
        return view('superadmin.daftar-perawat', compact('perawats'));
    }

    public function DetailPerawat(Perawat $perawat)
    {
        switch ($perawat->pengalaman) {
            case '1':
                $perawat->pengalaman = 'Belum ada Pengalaman';
                break;
            case '2':
                $perawat->pengalaman = 'Pengalaman kurang dari 5 Tahun';
                break;
            case '3':
                $perawat->pengalaman = 'Pengalaman 5 - 10 Tahun';
                break;
            default:
                $perawat->pengalaman = 'Pengalaman lebih dari 10 Tahun';
                break;
        }
        //return $perawat;

        //return $perawat->id;
        $fee = Fee::where('perawat_id', $perawat->id)->get();
        return view('superadmin.detail-perawat', compact('perawat', 'fee'));
    }

    public function EditPerawat(Perawat $perawat)
    {
        //return $perawat->id;
        $perawats = Perawat::find($perawat->id);

        //return $perawats;
        return view('superadmin.edit-perawat',compact('perawats'));
    }

    public function UpdatePerawat(Request $request)
    {
        //return $request;
        $this->validate(request(), [
            'nama' => 'required',
            'alamat' => 'required',
            'domisili' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'umur' => 'required',
            'telepon' => 'required',
            'email' => 'required',
            'pendidikan' => 'required',
            'institusi' => 'required',
            'sertifikasi' => 'required',            
            'mulai_aktif' => 'required',
            'pengalaman' => 'required',
            'deskripsi' => 'required',
            'jenis_perawatan' => 'required',
            'bersedia' => 'required',

        ]);
        $perawat = Perawat::find($request->id);

        $perawat->nama = $request->get('nama');
        $perawat->alamat = $request->get('alamat');
        $perawat->domisili = $request->get('domisili');
        $perawat->jenis_kelamin = $request->get('jenis_kelamin');
        $perawat->agama = $request->get('agama');
        $perawat->umur = $request->get('umur');
        $perawat->telepon = $request->get('telepon');
        $perawat->email = $request->get('email');
        $perawat->pendidikan = $request->get('pendidikan');
        $perawat->institusi = $request->get('institusi');
        $perawat->sertifikasi = $request->get('sertifikasi');
        $perawat->mulai_aktif = $request->get('mulai_aktif');
        $perawat->pengalaman = $request->get('pengalaman');
        $perawat->deskripsi = $request->get('deskripsi');
        $perawat->jenis_perawatan = $request->get('jenis_perawatan');
        $perawat->bersedia = $request->get('bersedia');
        if ($request->has('foto')) {
            $perawat->foto = $request->file('foto')->store('perawat');
        }
        $perawat->update();

        $activity = 'Update Data Perawat : <b>'.$request->get('nama').'</b>';
        ActivityLog::create([
                'user_id' => Auth::id(),
                'activity' => $activity,
            ]);

        return redirect(url('/superadmin/perawat/detail-perawat/'.$perawat->id ));
    }

    public function DeletePerawat(Perawat $perawat)
    {
        $perawat->delete();

        return redirect(url('/superadmin/perawat/daftar-perawat'));
    }

    public function UlasanPerawat()
    {
        $review = UserReview::where('owner_user_id', Auth::id())->paginate(5);
        
        foreach ($review as $reviews) {
            $reviews['nama_user'] = UserDetail::where('id', Booking::where('id', $reviews->booking_id)->first()->user_detail_id)->first()->nama;
            $reviews['nama_perawat'] = Perawat::where('id', Booking::where('id', $reviews->booking_id)->first()->perawat_id)->first()->nama;
            $reviews['id_perawat'] = Perawat::where('id', Booking::where('id', $reviews->booking_id)->first()->perawat_id)->first()->id;
            $reviews['foto_perawat'] = Perawat::where('id', Booking::where('id', $reviews->booking_id)->first()->perawat_id)->first()->foto;
            $reviews['tanggal_booking'] = Booking::where('id', $reviews->booking_id)->first()->tanggal_order->toDateString();
        }
        //return $review;
        return view('superadmin.ulasan-perawat', compact('review'));
    }

    public function TambahPerawat(Request $request)
    {
        //return $request;
        //validate input - all required
        $this->validate(request(), [
            'foto' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'domisili' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'umur' => 'required',
            'telepon' => 'required',
            'email' => 'required',
            'pendidikan' => 'required',
            'institusi' => 'required',
            'sertifikasi' => 'required',            
            'mulai_aktif' => 'required',
            'pengalaman' => 'required',
            'deskripsi' => 'required',
            'jenis_perawatan' => 'required',
            'bersedia' => 'required'
        ]);

        //return $request->get('email');
        //input ke database
        Perawat::create([
            'user_id' => Auth::id(),
            'nama' => $request->get('nama'),
            'alamat' => $request->get('alamat'),
            'domisili' => $request->get('domisili'),
            'jenis_kelamin' => $request->get('jenis_kelamin'),
            'agama' => $request->get('agama'),
            'umur' => $request->get('umur'),
            'telepon' => $request->get('telepon'),
            'email' => $request->get('email'),
            'pendidikan' => $request->get('pendidikan'),
            'institusi' => $request->get('institusi'),
            'sertifikasi' => $request->get('sertifikasi'),
            'foto' => $request->file('foto')->store('perawat'),
            'mulai_aktif' => $request->get('mulai_aktif'),
            'pengalaman' => $request->get('pengalaman'),
            'deskripsi' => $request->get('deskripsi'),
            'jenis_perawatan' => $request->get('jenis_perawatan'),
            'bersedia' => $request->get('bersedia')
        ]);

        $activity = 'Tambah Perawat Baru : <b>'.$request->get('nama').'</b>';
        ActivityLog::create([
            'user_id' => Auth::id(),
            'activity' => $activity,
        ]);

        return redirect(url('/superadmin/perawat/daftar-perawat'));
    }

    public function CreateFee(Request $request)
    {
        $this->validate(request(), [
            'layanan' => 'required',
            'biaya' => 'required'
        ]);

        $max = count($request->get('layanan'));
        //return $max;

        //return $request->get('tindakan');
        $n = 0;
        foreach ($request->get('layanan') as $key) {
            $layanan[$n] = $key;
            $n++;
        }

        $n = 0;
        foreach ($request->get('biaya') as $key) {
            $biaya[$n] = $key;
            $n++;
        }

        for ($i=0; $i < $max ; $i++) { 
            Fee::create([
                'perawat_id' => $request->get('perawat_id'),
                'layanan' => $layanan[$i],
                'biaya' => $biaya[$i]
            ]);
        }

        return redirect(url('/superadmin/perawat/detail-perawat/'.$request->get('perawat_id')));
    }

    public function UpdateFee(Request $request)
    {
        $this->validate(request(), [
            'layanan' => 'required',
            'biaya' => 'required'
        ]);

        $max = count($request->get('layanan'));
        //return $max;

        //return $request->get('tindakan');
        $n = 0;
        foreach ($request->get('layanan') as $key) {
            $layanan[$n] = $key;
            $n++;
        }

        $n = 0;
        foreach ($request->get('biaya') as $key) {
            $biaya[$n] = $key;
            $n++;
        }

        $n = 0;
        foreach ($request->get('id_fee') as $key) {
            $id[$n] = $key;
            $n++;
        }

        //return $request->get('perawat_id');

        for ($i=0; $i < $max ; $i++) { 
            $fee = Fee::find($id[$i]);
            $fee->layanan = $layanan[$i];
            $fee->biaya = $biaya[$i];
            $fee->update();
        }

        return redirect(url('/superadmin/perawat/detail-perawat/'.$request->get('perawat_id')));
    }

    public function DeleteFee(Fee $fee)
    {
        $fees = Fee::find($fee->id);
        $id = $fees->perawat_id;
        //return $fees;
        $fees->delete();


        return redirect(url('/superadmin/perawat/detail-perawat/'.$id));
    }

    public function CreateBooking(Request $request)
    {
        $this->validate(request(), [
            'layanan' => 'required',
            'order_date' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'email' => 'required',
            'nomor_hp' => 'required'
        ]);
    }

    public function RumkitNormal()
    {
        $rumahsakit = RumahSakit::where('membership_status', 0)->paginate(10);
        $status = 1;
        //return $rumahsakit;

        return view('superadmin.rumah-sakit', compact('rumahsakit', 'status'));
    }

    public function RumkitApproval()
    {
        $rumahsakit = RumahSakit::where('membership_status', 1)->paginate(10);
        $status = 2;
        //return $rumahsakit;

        return view('superadmin.rumah-sakit', compact('rumahsakit', 'status'));
    }

    public function RumkitPremium()
    {
        $rumahsakit = RumahSakit::where('membership_status', 2)->paginate(10);
        $status = 3;
        //return $rumahsakit;

        return view('superadmin.rumah-sakit', compact('rumahsakit', 'status'));
    }

    public function Cari(Request $request)
    {
        $status = 0;
        //return $request;
        if (!empty(UserDetail::where('nama', 'like', '%'.$request->get('search').'%')->first())) {
            $user = UserDetail::where('nama', 'like', '%'.$request->get('search').'%')->first()->id;
        }
        else{
            $user = "";
        }

        if (!empty(Perawat::where('nama', 'like', '%'.$request->get('search').'%')->first())) {
            $perawat = Perawat::where('nama', 'like', '%'.$request->get('search').'%')->first()->id;
        }
        else{
            $perawat = "";
        }

        //return $perawat;
        
        //return $user;
        $join = Booking::with('userDetail', 'perawat')->where('kode_transaksi', 'like', '%'.$request->get('search').'%')
                    ->orWhere('user_detail_id', $user)
                    ->orWhere('perawat_id', $perawat)
                    ->where('owner_user_id', Auth::id())->orderBy('id', 'desc')->paginate(10);
        //return $join;
        foreach ($join as $key) {
            $key['pasien'] = Pasien::where('booking_id', $key->id)->first();
            $key['rumah_sakit'] = RumahSakit::where('user_id', $key->owner_user_id)->first();
        }
        return view('superadmin.pemesanan', compact('join', 'status'));
    }
}
