<?php

namespace App\Http\Controllers;

use App\Fee;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class FeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

        
    public function index()
    {
        //
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - start - 
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }
        else {
            if (Auth::user()->role != 'owner') { 
                return redirect(url('/404'));
            }
        }
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - end -
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - start - 
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }
        else {
            if (Auth::user()->role != 'owner') { 
                return redirect(url('/404'));
            }
        }
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - end -
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //return $request;
        $this->validate(request(), [
            'layanan' => 'required',
            'biaya' => 'required'
        ]);

        $max = count($request->get('layanan'));
        //return $max;

        //return $request->get('tindakan');
        $n = 0;
        foreach ($request->get('layanan') as $key) {
            $layanan[$n] = $key;
            $n++;
        }

        $n = 0;
        foreach ($request->get('biaya') as $key) {
            $biaya[$n] = $key;
            $n++;
        }

        for ($i=0; $i < $max ; $i++) { 
            Fee::create([
                'perawat_id' => $request->get('perawat_id'),
                'layanan' => $layanan[$i],
                'biaya' => $biaya[$i]
            ]);
        }

        return redirect(url('/admin/perawat/detail-perawat/'.$request->get('perawat_id')));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function show(Fee $fee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function edit(Fee $fee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fee $fee)
    {
        //return $request;
        $this->validate(request(), [
            'layanan' => 'required',
            'biaya' => 'required'
        ]);

        $max = count($request->get('layanan'));
        //return $max;

        //return $request->get('tindakan');
        $n = 0;
        foreach ($request->get('layanan') as $key) {
            $layanan[$n] = $key;
            $n++;
        }

        $n = 0;
        foreach ($request->get('biaya') as $key) {
            $biaya[$n] = $key;
            $n++;
        }

        $n = 0;
        foreach ($request->get('id_fee') as $key) {
            $id[$n] = $key;
            $n++;
        }

        //return $request->get('perawat_id');

        for ($i=0; $i < $max ; $i++) { 
            $fee = Fee::find($id[$i]);
            $fee->layanan = $layanan[$i];
            $fee->biaya = $biaya[$i];
            $fee->update();
        }

        return redirect(url('/admin/perawat/detail-perawat/'.$request->get('perawat_id')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fee $fee)
    {
        if (Auth::user()->role != 'owner') {
            return redirect(url('/404'));
        }
        //
        $fees = Fee::find($fee->id);
        $id = $fees->perawat_id;
        //return $fees;
        $fees->delete();


        return redirect(url('/admin/perawat/detail-perawat/'.$id));
    }
}
