<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Booking;
use App\RumahSakit;
use App\User;
use App\Layanan;
use App\Perawat;
use App\Pasien;
use App\UserReview;
use App\ActivityLog;
use App\UserDetail;

class DashboardController extends Controller
{
    //
    // public function __construct()
    // {
    //     return Auth::user()->role;
    //     if (Auth::user()->role == 'owner') {
    //         return redirect(url('/404'));
    //     }
    // }

    public function index()
    {	
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - start - 
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }
        else {
            if (Auth::user()->role != 'owner') { 
                return redirect(url('/404'));
            }
        }
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - end -
        

    	$rs = count(RumahSakit::where('user_id', Auth::id())->get());
    	$rsname = RumahSakit::where('user_id', Auth::id())->first()->nama;
        //return $rsname;
    	//return RumahSakit::first();
    	$perawat = count(Perawat::where('user_id', Auth::id())->get());
    	$ulasan = count(UserReview::where('owner_user_id', Auth::id())->get());
    	$pemesanan = count(Booking::where('owner_user_id', Auth::id())->get());
        //return $pemesanan;

        $join = Booking::with('userDetail')->where('owner_user_id', Auth::id())->orderBy('id', 'desc')->take(2)->get();
    	//$join = Booking::with('userDetail')->orderBy('id', 'desc')->take(2)->get();
        //return $join;
        // foreach ($join as $key) {
        //     echo $key->biaya;
        //     //$key['biaya'] = Layanan::where('tindakan', $key->tindakan)->first()->biaya;
        // }

        //return $join;

        $activity = ActivityLog::where('user_id', Auth::id())->orderBy('id', 'desc')->take(10)->get();
        //return $join;
    	return view('admin.dashboard', compact('join', 'rs', 'rsname', 'perawat', 'ulasan', 'pemesanan', 'activity'));
    }

    public function show(Booking $booking)
    {
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - start - 
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }

        $id = UserDetail::where('id', $booking->user_detail_id)->first()->user_id;
        if (Auth::id() == $id || Auth::user() != NULL) {
            // else {
            //     if (Auth::user()->role != 'owner' || Auth::user()->role != 'user') { 
            //         return redirect(url('/404'));
            //     }
            // }
            //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - end -

            //ambil rumah sakit, user detail, perawat
            $join = Booking::with('userDetail', 'perawat')->where('id', $booking->id)->first();
            $join['fee'] = Fee::where('id', $join->layanan)->first()->biaya;
            $join['rumahsakit'] = RumahSakit::where('user_id', $join->owner_user_id)->first();
            $join['layanan'] = Fee::where('id', $join->layanan)->first()->layanan;
            $user = User::where('id', $join->userDetail->user_id)->first();
            //return $user;
            //return $join;
            return view('pages.invoice', compact('join', 'user'));
        }
        else
        {
            return view('pages.404');
        }
        
    }
}
