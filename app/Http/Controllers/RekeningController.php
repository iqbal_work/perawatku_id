<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Rekening;

class RekeningController extends Controller
{
    //
    public function delete(Rekening $rekening)
    {
    	if (Auth::user()->role != 'owner') {
            return redirect(url('/404'));
        }

        $perawats = Rekening::find($rekening->id);
        $perawats->delete();
    }
}
