<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use App\UserDetail;

class UserDetailController extends Controller
{
	public function show()
	{
		$user = UserDetail::where('user_id', Auth::id())->first();
        //return $user;
		return view('user.profil-pengaturan', compact('user'));
	}
    //
    public function store(Request $request)
    {
        //return $request;
        $this->validate(request(), [
            'foto' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'nomor_ktp' => 'required',
            'nomor_hp' => 'required',
        ]);

        UserDetail::create([
            'user_id' => Auth::id(),
            'nama' => $request->get('nama'),
            'alamat' => $request->get('alamat'),
            'nomor_ktp' => $request->get('nomor_ktp'),
            'nomor_hp' => $request->get('nomor_hp'),
            'foto' => $request->file('foto')->store('perawat')
        ]);

        return redirect(url('/profil/pengaturan'));
    }
    public function update(Request $request)
    {
        //return $request;
        //$rsid = UserDetail::select('id')->where('user_id', Auth::id())->first();
        $userdetail = UserDetail::find(UserDetail::select('id')->where('user_id', Auth::id())->first())->first();
        //return $userdetail;
        $userdetail['nama'] = $request->get('nama');
        $userdetail['alamat'] = $request->get('alamat');
        $userdetail['nomor_hp'] = $request->get('nomor_hp');
        $userdetail['nomor_ktp'] = $request->get('nomor_ktp');

        //cek kalau image kosong
        if ($request->has('foto')) {
            $userdetail['foto'] = $request->file('foto')->store('users');
        }
        //return $rs;
        $userdetail->update();
    	return redirect(url('/profil/pengaturan'));
    }
}
