<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\ActivityLog;
use App\RumahSakit;
use App\UserDetail;
/**
* 
*/
class RegistrationsController extends Controller
{
	public function create()
	{
		return view('registrations.create');
	}

	public function store()
	{
		//validate
		$this->validate(request(), [
			'name' => 'required',
			'email' => 'required|unique:users|email',
			'email' => 'required|email',
			'hp' => 'required',
			'password' => 'required|confirmed'
		]);
		//create save
		$user = User::create([
			'name' => request('name'),
			'email' => request('email'),
			'password' => bcrypt(request('password')),
            'hp' => request('hp')
		]);

		$userid = User::where('email', request('email'))->first()->id;
		//return $userid;
		$userdetail = UserDetail::create([
			'user_id' => $userid,
			'nama' => request('name'),
			'nomor_hp' => request('hp')
		]);



		//sign user in
		auth()->login($user);

		//redirect homepage
		return redirect()->home();
	}

	public function update(Request $request)
	{
		$this->validate(request(), [
			'old-pass' => 'required',
			'password' => 'required|confirmed'
		]);
		if ($request->password != $request->password_confirmation) {
			return back()->withErrors([
				'message' => 'The password confirmation does not match.'
			]);
		}

		if (bcrypt($request->get('old-pass') == Auth::user()->password))
		{
			$user = User::find(Auth::user()->id);

			$user->password = bcrypt($request->get('password'));
			$user->update();
			ActivityLog::create([
	            'user_id' => Auth::id(),
	            'activity' => 'Ganti Password'
	        ]);
	        if (Auth::user()->role == 'admin') {
	        	return redirect(url('/superadmin/dashboard'));
	        }
	        elseif(Auth::user()->role == 'owner'){
	        	return redirect(url('/admin/dashboard'));
	        }
	        else{
	        	return redirect(url('/profil'));
	        }
		}
	}
}

?>