<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

/**
* 
*/
class SessionsController extends Controller
{

	public function __construct()
	{
		$this->middleware('guest', ['except' => 'destroy']);
	}

	public function create()
	{
		return view('sessions.create');
	}

	public function destroy()
	{
		Auth::logout();
		Session::flush();

		return redirect('/');
	}

	public function store()
	{
		//auth user
		if (!auth()->attempt(request(['email', 'password']))){
			return back()->withErrors([
				'message' => 'email or password is incorrect'
			]);
		}
		//sign in
		//redirect homepage
		if (Auth::user()->role == 'user')
		{

			return back();
		} 
		elseif (Auth::user()->role == 'owner')
		{
			$path = '/admin/dashboard';
		} 
		elseif (Auth::user()->role == 'admin')
		{
			$path = '/superadmin/blog';
		}
		return redirect($path);

	}
}

?>