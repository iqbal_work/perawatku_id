<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class SitemapController extends Controller
{
    public function index()
    {
        $blogs = Blog::latest()->get();

        return response()->view('pages.sitemap', [
            'blogs' => $blogs,
        ])->header('Content-Type', 'text/xml');


    }
}
