<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Booking;
use App\RumahSakit;
use App\User;
use App\UserDetail;
use App\Fee;
use App\Perawat;
use App\Pasien;
use App\Rekening;
use App\ActivityLog;
use App\Layanan;
use Mail;

class BookingController extends Controller
{
    //

    public function create(Perawat $perawat)
    {
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }

        if (Auth::user()->role != 'user') {
            return redirect(url('/404'));
        }

        $user_detail = UserDetail::where('user_id', Auth::id())->first();
        $user_detail['email'] = User::where('id', Auth::id())->first()->email;
        $fee = Fee::where('perawat_id', $perawat->id)->get();
        $pemesanan = 0;

        //return $fee;
        //return $perawat;
        //return $user_detail;
        return view('user.pemesanan', compact('user_detail', 'fee', 'perawat', 'harga', 'pemesanan'));
    }

    public function create2(Perawat $perawat)
    {
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }

        if (Auth::user()->role != 'user') {
            return redirect(url('/404'));
        }

        $user_detail = UserDetail::where('user_id', Auth::id())->first();
        $user_detail['email'] = User::where('id', Auth::id())->first()->email;
        $fee = Fee::where('perawat_id', $perawat->id)->get();
        $pemesanan = 1;

        //return $fee;
        //return $perawat;
        //return $user_detail;
        return view('user.pemesanan', compact('user_detail', 'fee', 'perawat', 'harga', 'pemesanan'));
    }

    public function layananBayi()
    {
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }

        if (Auth::user()->role != 'user') {
            return redirect(url('/404'));
        }

        $user = User::where('id', Auth::id())->first();
        $user_detail = UserDetail::where('user_id', Auth::id())->first();
        $user_detail['email'] = User::where('id', Auth::id())->first()->email;
        $layanans = Layanan::where('kategori', "bayi")->get();
        $pemesanan = 1;
        foreach ($layanans as $layan) {
            $layanan[$layan->id] = $layan;
        }
        $layananjs = json_encode($layanan);

        //return $fee;
        //return $perawat;
        //return $user_detail;
        return view('user.pemesanan', compact('user','user_detail','pemesanan', 'layanan', 'layananjs'));
    }

    public function layananLuka()
    {
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }

        if (Auth::user()->role != 'user') {
            return redirect(url('/404'));
        }

        $user = User::where('id', Auth::id())->first();
        $user_detail = UserDetail::where('user_id', Auth::id())->first();
        $user_detail['email'] = User::where('id', Auth::id())->first()->email;
        $layanans = Layanan::where('kategori', "luka")->get();
        $pemesanan = 0;
        foreach ($layanans as $layan) {
            $layanan[$layan->id] = $layan;
        }
        $layananjs = json_encode($layanan);

        //return $fee;
        //return $perawat;
        //return $user_detail;
        return view('user.pemesanan', compact('user','user_detail','pemesanan', 'layanan', 'layananjs'));
    }

    public function layananMedis()
    {
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }

        if (Auth::user()->role != 'user') {
            return redirect(url('/404'));
        }

        $user = User::where('id', Auth::id())->first();
        $user_detail = UserDetail::where('user_id', Auth::id())->first();
        $user_detail['email'] = User::where('id', Auth::id())->first()->email;
        $layanans = Layanan::where('kategori', "medis")->get();
        $pemesanan = 2;
        foreach ($layanans as $layan) {
            $layanan[$layan->id] = $layan;
        }
        $layananjs = json_encode($layanan);
        //return $fee;
        //return $perawat;
        //return $user_detail;
        return view('user.pemesanan', compact('user','user_detail','pemesanan', 'layanan', 'layananjs'));
    }

    public function layananPaliatif()
    {
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }

        if (Auth::user()->role != 'user') {
            return redirect(url('/404'));
        }

        $user = User::where('id', Auth::id())->first();
        $user_detail = UserDetail::where('user_id', Auth::id())->first();
        $user_detail['email'] = User::where('id', Auth::id())->first()->email;
        $layanans = Layanan::where('kategori', "paliatif")->get();
        $pemesanan = 3;
        foreach ($layanans as $layan) {
            $layanan[$layan->id] = $layan;
        }
        $layananjs = json_encode($layanan);
        //return $layanans;
        //return $fee;
        //return $perawat;
        //return $user_detail;
        return view('user.pemesanan', compact('user','user_detail','pemesanan', 'layanan', 'layananjs'));
    }

    public function tindakan()
    {
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }

        if (Auth::user()->role != 'user') {
            return redirect(url('/404'));
        }

        $user = User::where('id', Auth::id())->first();
        $user_detail = UserDetail::where('user_id', Auth::id())->first();
        $user_detail['email'] = User::where('id', Auth::id())->first()->email;
        $layanans = Layanan::whereNotNull('tindakan')->get();
        $pemesanan = 4;
        foreach ($layanans as $layan) {
            $layanan[$layan->id] = $layan;
        }
        $layananjs = json_encode($layanan);
        //return $layanan;
        //return $layanans;
        //return $fee;
        //return $perawat;
        //return $user_detail;
        return view('user.pemesanan', compact('user','user_detail','pemesanan', 'layanan', 'layananjs'));
    }

    public function store(Request $request)
    {
        //return $request;
        $this->validate(request(), [
            'layanan' => 'required',
            'order_date' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'email' => 'required',
            'hp' => 'required'
        ]);

        if($request->get('pasien') == "sendiri"){
            $request['nama-pasien'] = $request->get('nama');
            $request['alamat-pasien'] = $request->get('alamat');
            $request['email-pasien'] = $request->get('email');
            $request['telepon-pasien'] = $request->get('hp');
        }
        else{
            if ($request->get('pasien') == 'bayi') {
                $this->validate(request(), [
                    'nama-bayi' => 'required',
                    'alamat-bayi' => 'required',
                    'bb-bayi' => 'required',
                    'tb-bayi' => 'required',
                    'umur' => 'required',
                    'suku' => 'required',
                    'agama' => 'required',
                    'kondisi-medis' => 'required',
                ]); 
            }
            else {
                $this->validate(request(), [
                    'nama-pasien' => 'required',
                    'alamat-pasien' => 'required',
                    'email-pasien' => 'required',
                    'telepon-pasien' => 'required'
                ]);
            }
        }
        if(empty(Booking::orderBy('id', 'desc')->first()->id)){
            $kode_transaksi = 0; 
        }else{
            $kode_transaksi = Booking::orderBy('id', 'desc')->first()->id+1;
        }
        $request['kode_transaksi'] = $request->perawat_id."_".$kode_transaksi;

        $request['owner_user_id'] =  Perawat::where('id', $request->perawat_id)->first()->user_id; 
        $request['user_id'] = UserDetail::where('id', $request->user_detail_id)->first()->user_id;
        $order_date = Carbon::parse($request->get('order_date'))->toDateString();
        //return $order_date;
        //return $request->get('owner_user_id');

        Booking::create([
            'kode_transaksi' => $request->get('kode_transaksi'),
            'user_detail_id' => $request->get('user_detail_id'),
            'owner_user_id' => $request->get('owner_user_id'),
            'perawat_id' => $request->get('perawat_id'),
            'layanan' => $pemesanan,
            'biaya' => $request->get('layanan'),
            'tanggal_order' => $order_date
        ]);

        $bookingid = Booking::orderBy('id', 'desc')->first()->id;

        switch ($request->get('pasien')) {
            case 'bayi':
                $jenis_pasien = 3;
                break;
            case 'manula':
                $jenis_pasien = 4;
                break;
            case 'orang-lain':
                $jenis_pasien = 2;
                break;
            default:
                $jenis_pasien = 1;
                break;
        }

        //return $request;

        if ($jenis_pasien == 3) {
            Pasien::create([
                'user_id' => $request->get('user_id'),
                'booking_id' => $bookingid,
                'tipe_pasien' => $jenis_pasien,
                'nama_pasien' => $request->get('nama-bayi'),
                'alamat_pasien' => $request->get('alamat-bayi'),
                'berat_badan' => $request->get('bb-bayi'),
                'tinggi_badan' => $request->get('tb-bayi'),
                'umur' => $request->get('umur'),
                'suku' => $request->get('suku'),
                'agama' => $request->get('agama'),
                'kondisi_medis' => $request->get('kondisi-medis')
            ]);
        }
        else {
            Pasien::create([
                'user_id' => $request->get('user_id'),
                'booking_id' => $bookingid,
                'tipe_pasien' => $jenis_pasien,
                'nama_pasien' => $request->get('nama-pasien'),
                'alamat_pasien' => $request->get('alamat-pasien'),
                'email_pasien' => $request->get('email-pasien'),
                'hp' => $request->get('telepon-pasien')
            ]);
        }
        

        ActivityLog::create([
            'user_id' => $request->get('owner_user_id'),
            'activity' => 'Booking Baru dari '.$request->get('nama').'</br>'.Perawat::where('id', $request->get('perawat_id'))->first()->nama.' - '.Fee::where('id', $request->get('layanan'))->first()->layanan
        ]);

        return redirect(url('/pemesanan/konfirmasi'));
    }

    public function store2(Request $request)
    {
        //return $request;
        $this->validate(request(), [
            'layanan',
            'tindakan',
            'order_date' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'email' => 'required',
            'hp' => 'required',
            'biaya'
        ]);

        // UserDetail::create([
        //     'user_id' => Auth::id(),
        //     'nama' => request('nama'),
        //     'alamat' => request('alamat'),
        //     'nomor_hp' => request('hp')
        // ]);

        if($request->get('pasien') == "sendiri"){
            $request['nama-pasien'] = $request->get('nama');
            $request['alamat-pasien'] = $request->get('alamat');
            $request['email-pasien'] = $request->get('email');
            $request['telepon-pasien'] = $request->get('hp');
        }
        else{
            if ($request->get('pasien') == 'bayi') {
                $this->validate(request(), [
                    'nama-bayi' => 'required',
                    'alamat-bayi' => 'required',
                    'bb-bayi' => 'required',
                    'tb-bayi' => 'required',
                    'umur' => 'required',
                    'suku' => 'required',
                    'agama' => 'required',
                    'kondisi-medis' => 'required',
                ]);
            }
            else {
                $this->validate(request(), [
                    'nama-pasien' => 'required',
                    'alamat-pasien' => 'required',
                    'email-pasien' => 'required',
                    'telepon-pasien' => 'required'
                ]);
            }
        }
        if(empty(Booking::orderBy('id', 'desc')->first()->id)){
            $kode_transaksi = 0;
        }else{
            $kode_transaksi = str_random(5);
        }
        $request['kode_transaksi'] = "prwt-".$kode_transaksi;
        $kodeunik = rand (001, 999);
        $request['kodeunik'] = $kodeunik;

        /*$request['owner_user_id'] =  Perawat::where('id', $request->perawat_id)->first()->user_id;*/
        $request['user_id'] = Auth::id();
        $order_date = Carbon::parse($request->get('order_date'))->toDateString();
        //return $order_date;
        //return $request->get('owner_user_id');

        Booking::create([
            'kode_transaksi' => $request->get('kode_transaksi'),
            'user_detail_id' => $request->get('user_id'),
            'owner_user_id' => 1,
            'layanan' => $request->get('jasaasli'),
            'tindakan' => $request->get('tindakanasli'),
            'tanggal_order' => $order_date,
            'biaya' => $request->get('biaya'),
            'kodeunik' => $request->get('kodeunik')
        ]);

        $bookingid = Booking::orderBy('id', 'desc')->first()->id;
        $kode_transaksi = $request['kode_transaksi'];

        switch ($request->get('pasien')) {
            case 'bayi':
                $jenis_pasien = 3;
                break;
            case 'manula':
                $jenis_pasien = 4;
                break;
            case 'orang-lain':
                $jenis_pasien = 2;
                break;
            default:
                $jenis_pasien = 1;
                break;
        }

        //return $request;

        if ($jenis_pasien == 3) {
            Pasien::create([
                'user_id' => $request->get('user_id'),
                'booking_id' => $bookingid,
                'tipe_pasien' => $jenis_pasien,
                'nama_pasien' => $request->get('nama-bayi'),
                'alamat_pasien' => $request->get('alamat-bayi'),
                'berat_badan' => $request->get('bb-bayi'),
                'tinggi_badan' => $request->get('tb-bayi'),
                'umur' => $request->get('umur'),
                'suku' => $request->get('suku'),
                'agama' => $request->get('agama'),
                'kondisi_medis' => $request->get('kondisi-medis')
            ]);
        }
        else {
            Pasien::create([
                'user_id' => $request->get('user_id'),
                'booking_id' => $bookingid,
                'tipe_pasien' => $jenis_pasien,
                'nama_pasien' => $request->get('nama-pasien'),
                'alamat_pasien' => $request->get('alamat-pasien'),
                'email_pasien' => $request->get('email-pasien'),
                'nomor_hp' => $request->get('telepon-pasien')
            ]);
        }

        ActivityLog::create([
            'user_id' => 3,
            'activity' => 'Booking Baru dari '.$request->get('nama').'</br> Layanan: '.$request->get('layananasli').' dan Tindakan: '.$request->get('tindakanasli'),
        ]);

        return redirect(url('/pemesanan/pembayaran/'.$kode_transaksi));
    }

    public function index()
    {
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - start - 
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }
        else {
            if (Auth::user()->role != 'owner') { 
                return redirect(url('/404'));
            }
        }
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - end -

    	$rsid = RumahSakit::where('user_id', Auth::id())->first()->id;
    	//return $rsid->id;

    	$booking = Booking::where('owner_user_id', Auth::id())->get();
    	
    	$join = Booking::with('userDetail', 'perawat')->where('owner_user_id', Auth::id())->orderBy('id', 'desc')->paginate(10);
    	foreach ($join as $key) {
            $key['pasien'] = Pasien::where('booking_id', $key->id)->first();
            $key['rumah_sakit'] = RumahSakit::where('user_id', $key->owner_user_id)->first();
        }
        $status = 0;
        //return $key;
        //return $join;
    	return view('admin.pemesanan',compact('join', 'status'));
    }

    public function Cari(Request $request)
    {
        $status = 0;
        //return $request;
        if (!empty(UserDetail::where('nama', 'like', '%'.$request->get('search').'%')->first())) {
            $user = UserDetail::where('nama', 'like', '%'.$request->get('search').'%')->first()->id;
        }
        else{
            $user = "";
        }

        if (!empty(Perawat::where('nama', 'like', '%'.$request->get('search').'%')->first())) {
            $perawat = Perawat::where('nama', 'like', '%'.$request->get('search').'%')->first()->id;
        }
        else{
            $perawat = "";
        }

        //return $perawat;
        
        //return $user;
        $join = Booking::with('userDetail', 'perawat')->where('kode_transaksi', 'like', '%'.$request->get('search').'%')
                    ->orWhere('user_detail_id', $user)
                    ->orWhere('perawat_id', $perawat)
                    ->where('owner_user_id', Auth::id())->orderBy('id', 'desc')->paginate(10);
        //return $join;
        foreach ($join as $key) {
            $key['pasien'] = Pasien::where('booking_id', $key->id)->first();
            $key['rumah_sakit'] = RumahSakit::where('user_id', $key->owner_user_id)->first();
        }
        return view('admin.pemesanan', compact('join', 'status'));
    }

    public function show_user(){
        if (empty(UserDetail::where('user_id', Auth::id())->first()->nomor_ktp)) {
            return redirect(url('profil/pengaturan'));
        }
        $user_detail_id = UserDetail::where('user_id', Auth::id())->first()->user_id;

        $booking = Booking::with('perawat')->where('user_detail_id', $user_detail_id)->orderBy('id', 'desc')->paginate(10);
        foreach ($booking as $key) {
            $key['rumahsakit'] = RumahSakit::where('user_id', $key->owner_user_id)->first();
        }

        foreach ($booking as $key) {
            switch ($key->booking_status) {
                case '1':
                    $key['booking_status'] = "Sedang Diproses";
                    break;
                case '2':
                    $key['booking_status'] = "Ditolak";
                    break;
                default:
                    $key['booking_status'] = "Belum Diproses";
                    break;
            }
            /*$key['layanan'] = Fee::where('id', $key['layanan'])->first()->layanan;*/;
        }

        //return $booking;
        return view('user.profil', compact('booking'));
    }

    public function show(Booking $booking)
    {
        if (Auth::user()->role != 'owner') {
            return redirect(url('/404'));
        }

        $join = Booking::with('userDetail','perawat')->where('id', $booking->id)->first();
        $join['pasien'] = Pasien::where('booking_id', $join->id)->first();
        $join['fee'] = Fee::where('perawat_id', $join->perawat->id)->first();

        //return $join;
        return view('admin.pemesanan-detail',compact('join'));
    }

    public function booking_ditolak(Booking $booking)
    {
        if (Auth::user()->role != 'owner') {
            return redirect(url('/404'));
        }

        $booking->booking_status = "2";
        $booking->update();
        return redirect(url('/admin/pemesanan/'));
    }

    public function booking_diterima(Booking $booking)
    {
        if (Auth::user()->role != 'owner') {
            return redirect(url('/404'));
        }

        $booking->booking_status = "1";
        $booking->update();
        return redirect(url('/admin/pemesanan/'));
    }

    public function diterima()
    {
        if (Auth::user()->role != 'owner') {
            return redirect(url('/404'));
        }

        $rsid = RumahSakit::where('user_id', Auth::id())->first()->id;

        $booking = Booking::where('owner_user_id', Auth::id())->get();
        
        $join = Booking::with('userDetail', 'perawat')->orderBy('id', 'desc')->where('booking_status', '1')->get();
        foreach ($join as $key) {
            $key['pasien'] = Pasien::where('booking_id', $key->id)->first();
            $key['rumah_sakit'] = RumahSakit::where('user_id', $key->owner_user_id)->first()->nama;
        }
        $status = 1;
        return view('admin.pemesanan', compact('join', 'status'));
    }

    public function ditolak()
    {
        if (Auth::user()->role != 'owner') {
            return redirect(url('/404'));
        }

        $rsid = RumahSakit::where('user_id', Auth::id())->first()->id;

        $booking = Booking::where('owner_user_id', Auth::id())->get();
        
        $join = Booking::with('userDetail', 'perawat')->orderBy('id', 'desc')->where('booking_status', '2')->get();
        foreach ($join as $key) {
            $key['pasien'] = Pasien::where('booking_id', $key->id)->first();
            $key['rumah_sakit'] = RumahSakit::where('user_id', $key->owner_user_id)->first()->nama;
        }
        $status = 2;
        return view('admin.pemesanan', compact('join', 'status'));
    }

    public function belum_diproses()
    {   
        if (Auth::user()->role != 'owner') {
            return redirect(url('/404'));
        }

        $rsid = RumahSakit::where('user_id', Auth::id())->first()->id;

        $booking = Booking::where('owner_user_id', Auth::id())->get();
        
        $join = Booking::with('userDetail', 'perawat')->orderBy('id', 'desc')->where('booking_status', '0')->get();
        foreach ($join as $key) {
            $key['pasien'] = Pasien::where('booking_id', $key->id)->first();
            $key['rumah_sakit'] = RumahSakit::where('user_id', $key->owner_user_id)->first()->nama;
        }
        $status = 3;
        return view('admin.pemesanan', compact('join', 'status'));
    }

    public function pembayaran($booking)
    {
        $book = Booking::where('kode_transaksi', $booking)->first();
        $userid = UserDetail::where('user_id', $book->user_detail_id)->first()->user_id;

        if (Auth::id() == $userid) {
            //return $book;
            $book['user_detail'] = UserDetail::where('user_id', $book->user_detail_id)->first();
            $book['user_detail']['email'] = User::where('id', $book->user_detail_id)->first()->email;
            $book['pasien'] = Pasien::where('booking_id', $book->id)->first();
            $book['bank'] = Rekening::where('owner_user_id', $book->owner_user_id)->get();
            $total = $book->biaya + $book->kodeunik;
            return view('user.pembayaran', compact('book','total'));
        }
        else
        {
            return view('pages.404');
        }
        
    }

    public function konfirmasi($booking)
    {
        //return $request;
        $book = Booking::where('kode_transaksi', $booking)->first();
        $book->booking_status = "1";
        $book->payment_status = "1";
        $book->update();

        $userid = UserDetail::where('user_id', $book->user_detail_id)->first()->user_id;

        if (Auth::id() == $userid) {
            //return $book;
            $book['user_detail'] = UserDetail::where('user_id', $book->user_detail_id)->first();
            $book['user'] = User::where('id', $book->user_detail_id)->first();
            $book['pasien'] = Pasien::where('booking_id', $book->id)->first();
            $total = $book->biaya + $book->kodeunik;

            Mail::send('mails.pesanan',
                array(
                    'nama' => $book->pasien->nama_pasien, 'alamat' => $book->pasien->alamat_pasien, 'email' => $book->user->email, 'nomor_hp' => $book->user->hp, 'kunjungan' => $book->tanggal_order->format('d-m-Y'), 'total' => $total, 'kode' => $book->kode_transaksi, 'layanan' => $book->layanan, 'tindakan' => $book->tindakan, 'kodeunik' => $book->kodeunik
                ), function($message) use ($book)
                {
                    $message->from('info@perawatku.id');
                    $message->to('info@perawatku.id', 'Admin')->subject('Pesanan baru: '.$book->kode_transaksi);
                });

            return view('user.konfirmasi', compact('book','total'));
        }
    }

    public function pembayaran_diterima(Booking $booking)
    {
        $booking->payment_status = "1";
        $booking->update();
        return redirect(url('/admin/pemesanan/'));
    }

    public function invoice($booking)
    {
        //return $request;
        $booking = Booking::where('kode_transaksi', $booking)->first();
        $userid = UserDetail::where('user_id', $booking->user_detail_id)->first()->user_id;

        if (Auth::id() == $userid) {
            //return $book;
            $user_detail = UserDetail::where('user_id', $booking->user_detail_id)->first();
            $pasien = Pasien::where('booking_id', $booking->id)->first();
            $bank = Rekening::where('owner_user_id', $booking->owner_user_id)->get();
            $booking->booking_status = "1";
            $booking->payment_status = "2";
            $total = $booking->biaya + $booking->kodeunik;
            $booking->update();

            return view('pages.invoice', compact('booking', 'pasien', 'user_detail', 'total', 'bank'));
        }
        else
        {
            return view('pages.404');
        }
    }
}
