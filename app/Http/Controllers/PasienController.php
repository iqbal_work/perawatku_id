<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Pasien;
use App\UserDetail;

class PasienController extends Controller
{
    //
	public function show()
	{
		if (empty(UserDetail::where('user_id', Auth::id())->first()->nomor_ktp)) {
            return redirect(url('profil/pengaturan'));
        }

		$bayi = Pasien::where('user_id', Auth::id())->where('booking_id', 0)->where('tipe_pasien', 3)->first();
		$pasien = Pasien::where('user_id', Auth::id())->where('booking_id', 0)->where('tipe_pasien', 2)->first();
 		return view('user.profil-pasien', compact('pasien', 'bayi'));
	}

    public function store(Request $request)
    {
    	if ($request->get('nama-bayi') != null) {
    		$this->validate(request(), [
	            'nama-bayi' => 'required',
	            'alamat-bayi' => 'required',
	            'bb-bayi' => 'required',
	            'tb-bayi' => 'required',
	            'umur' => 'required',
	            'suku' => 'required',
	            'agama' => 'required',
	            'kondisi-medis' => 'required',
	        ]); 

    		Pasien::create([
	            'user_id' => Auth::id(),
	            'booking_id' => 0,
	            'tipe_pasien' => 3,
	            'nama_pasien' => $request->get('nama-bayi'),
	            'alamat_pasien' => $request->get('alamat-bayi'),
	            'berat_badan' => $request->get('bb-bayi'),
	            'tinggi_badan' => $request->get('tb-bayi'),
	            'umur' => $request->get('umur'),
	            'suku' => $request->get('suku'),
	            'agama' => $request->get('agama'),
	            'kondisi_medis' => $request->get('kondisi-medis')
	        ]);

	        return redirect(url('/profil/pasien'));
    	}
    	else{
    		$this->validate(request(), [
	            'nama-pasien' => 'required',
	            'alamat-pasien' => 'required',
	            'email-pasien' => 'required',
	            'telepon-pasien' => 'required'
	        ]);

	        Pasien::create([
	            'user_id' => Auth::id(),
	            'booking_id' => 0,
	            'tipe_pasien' => 2,
	            'nama_pasien' => $request->get('nama-pasien'),
	            'alamat_pasien' => $request->get('alamat-pasien'),
	            'email_pasien' => $request->get('email-pasien'),
	            'nomor_hp' => $request->get('telepon-pasien')
	        ]);

	        return redirect(url('/profil/pasien'));
    	}
	}

	public function update(Request $request)
    {
    	if ($request->get('nama-bayi') != null) {
    		$this->validate(request(), [
	            'nama-bayi' => 'required',
	            'alamat-bayi' => 'required',
	            'bb-bayi' => 'required',
	            'tb-bayi' => 'required',
	            'umur' => 'required',
	            'suku' => 'required',
	            'agama' => 'required',
	            'kondisi-medis' => 'required',
	        ]); 

    		$pasien = Pasien::where('id', $request->get('id'))->first();
    		$pasien['nama_pasien'] = $request->get('nama-bayi');
    		$pasien['alamat_pasien'] = $request->get('alamat-bayi');
    		$pasien['berat_badan'] = $request->get('bb-bayi');
    		$pasien['tinggi_badan'] = $request->get('tb-bayi');
    		$pasien['umur'] = $request->get('umur');
    		$pasien['suku'] = $request->get('suku');
    		$pasien['agama'] = $request->get('agama');
    		$pasien['kondisi_medis'] = $request->get('kondisi-medis');

    		$pasien->update();

	    	return redirect(url('/profil/pasien'));
    	}
    	else{
    		$this->validate(request(), [
	            'nama-pasien' => 'required',
	            'alamat-pasien' => 'required',
	            'email-pasien' => 'required',
	            'telepon-pasien' => 'required'
	        ]);

    		$pasien = Pasien::where('id', $request->get('id'))->first();
    		$pasien['nama_pasien'] = $request->get('nama-pasien');
    		$pasien['alamat_pasien'] = $request->get('alamat-pasien');
    		$pasien['email'] = $request->get('email-pasien');
    		$pasien['nomor_hp'] = $request->get('telepon-pasien');

    		$pasien->update();

	        return redirect(url('/profil/pasien'));
    	}
	}
}
