<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Perawat;
use App\RumahSakit;
use App\Fee;
use App\ActivityLog;

class PerawatController extends Controller
{
    //
    public function index()
    {
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - start - 
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        } else {
            if (Auth::user()->role != 'owner') {
                return redirect(url('/404'));
            }
        }
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - end -
        //$rsid = RumahSakit::where('user_id', Auth::id())->first()->id;
        $perawats = Perawat::where('user_id', Auth::id())->orderBy('id', 'desc')->paginate(10);

        //return $perawats;
        return view('admin.daftar-perawat', compact('perawats'));
    }

    public function display()
    {
        $perawats = Perawat::with('fee')->where('user_id', 1)->orderBy('id', 'desc')->paginate(16);

        foreach ($perawats as $key) {
            switch ($key->mulai_aktif) {
                case '1':
                    $key->mulai_aktif = 'Belum ada Pengalaman';
                    break;
                case '2':
                    $key->mulai_aktif = 'Pengalaman kurang dari 5 Tahun';
                    break;
                case '3':
                    $key->mulai_aktif = 'Pengalaman 5 - 10 Tahun';
                    break;
                default:
                    $key->mulai_aktif = 'Pengalaman lebih dari 10 Tahun';
                    break;
            }
        }

        //return $perawats;

        return view('user.perawat', compact('perawats'));
    }

    public function show(Perawat $perawat)
    {
        if (Auth::user()->role != 'owner') {
            return redirect(url('/404'));
        }

        switch ($perawat->mulai_aktif) {
            case '1':
                $perawat->mulai_aktif = 'Belum ada Pengalaman';
                break;
            case '2':
                $perawat->mulai_aktif = 'Pengalaman kurang dari 5 Tahun';
                break;
            case '3':
                $perawat->mulai_aktif = 'Pengalaman 5 - 10 Tahun';
                break;
            default:
                $perawat->mulai_aktif = 'Pengalaman lebih dari 10 Tahun';
                break;
        }
        //return $perawat;

        //return $perawat->id;
        $fee = Fee::where('perawat_id', $perawat->id)->get();
        //return $fee;
        //return ($perawat);
        return view('admin.detail-perawat', compact('perawat', 'fee'));
    }

    public function store(Request $request)
    {
        return $request;
        //return $request->file('foto')->store('perawat');
        //validate input - all required
        $this->validate(request(), [
            'foto' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'domisili' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'umur' => 'required',
            'telepon' => 'required',
            'email' => 'required',
            'pendidikan' => 'required',
            'institusi' => 'required',
            'sertifikasi' => 'required',
            'mulai_aktif' => 'required',
            'pengalaman' => 'required',
            'deskripsi' => 'required',
            'jenis_perawatan' => 'required',
            'bersedia' => 'required'
        ]);


        //ambil id rs
        $id = Auth::id();
        //return $id;
        //input ke database
        Perawat::create([
            'user_id' => Auth::id(),
            'nama' => $request->get('nama'),
            'alamat' => $request->get('alamat'),
            'domisili' => $request->get('domisili'),
            'jenis_kelamin' => $request->get('jenis_kelamin'),
            'agama' => $request->get('agama'),
            'umur' => $request->get('umur'),
            'telepon' => $request->get('telepon'),
            'email' => $request->get('email'),
            'pendidikan' => $request->get('pendidikan'),
            'institusi' => $request->get('institusi'),
            'sertifikasi' => $request->get('sertifikasi'),
            'foto' => $request->file('foto')->store('perawat'),
            'mulai_aktif' => $request->get('mulai_aktif'),
            'pengalaman' => $request->get('pengalaman'),
            'deskripsi' => $request->get('deskripsi'),
            'jenis_perawatan' => $request->get('jenis_perawatan'),
            'bersedia' => $request->get('bersedia')
        ]);

        $activity = 'Tambah Perawat Baru : <b>' . $request->get('nama') . '</b>';
        ActivityLog::create([
            'user_id' => Auth::id(),
            'activity' => $activity,
        ]);

        return redirect(url('/admin/dashboard'));
    }

    public function edit(Perawat $perawat)
    {
        if (Auth::user()->role != 'owner') {
            return redirect(url('/404'));
        }
        //return $perawat->id;
        $perawats = Perawat::find($perawat->id);

        //return $perawats;
        return view('admin.edit-perawat', compact('perawats'));
    }

    public function update(Request $request)
    {
        $this->validate(request(), [
            'nama' => 'required',
            'alamat' => 'required',
            'domisili' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'umur' => 'required',
            'telepon' => 'required',
            'email' => 'required',
            'pendidikan' => 'required',
            'institusi' => 'required',
            'sertifikasi' => 'required',
            'mulai_aktif' => 'required',
            'pengalaman' => 'required',
            'deskripsi' => 'required',
            'jenis_perawatan' => 'required',
            'bersedia' => 'required',

        ]);
        $perawat = Perawat::find($request->id);

        $perawat->nama = $request->get('nama');
        $perawat->alamat = $request->get('alamat');
        $perawat->domisili = $request->get('domisili');
        $perawat->jenis_kelamin = $request->get('jenis_kelamin');
        $perawat->agama = $request->get('agama');
        $perawat->umur = $request->get('umur');
        $perawat->telepon = $request->get('telepon');
        $perawat->email = $request->get('email');
        $perawat->pendidikan = $request->get('pendidikan');
        $perawat->institusi = $request->get('institusi');
        $perawat->sertifikasi = $request->get('sertifikasi');
        $perawat->mulai_aktif = $request->get('mulai_aktif');
        $perawat->pengalaman = $request->get('pengalaman');
        $perawat->deskripsi = $request->get('deskripsi');
        $perawat->jenis_perawatan = $request->get('jenis_perawatan');
        $perawat->bersedia = $request->get('bersedia');
        if ($request->has('foto')) {
            $perawat->foto = $request->file('foto')->store('perawat');
        }
        $perawat->update();

        $activity = 'Update Data Perawat : <b>' . $request->get('nama') . '</b>';
        ActivityLog::create([
            'user_id' => Auth::id(),
            'activity' => $activity,
        ]);

        return redirect(url('/admin/perawat/detail-perawat/' . $perawat->id));
        /*$rs['nama'] = $request->get('nama');
        $rs['alamat'] = $request->get('alamat');
        $rs['telepon'] = $request->get('telepon');
        $rs['email'] = $request->get('email');
        $rs['penanggungjawab'] = $request->get('penanggungjawab');
        $rs['telepon_penanggungjawab'] = $request->get('telepon_penanggungjawab');
        $rs['deskripsi'] = $request->get('deskripsi');

        //cek kalau image kosong
        if ($request->has('image')) {
            $rs['foto'] = $request->file('image')->store('public');
        }
        //return $rs;
        $rs->update();*/
    }

    public function destroy(Perawat $perawat)
    {
        if (Auth::user()->role != 'owner') {
            return redirect(url('/404'));
        }

        $perawats = Perawat::find($perawat->id);
        $perawats->delete();

        return redirect(url('/admin/perawat/daftar-perawat'));
    }

    public function search(Request $request)
    {
        //return $request;
        if ($request->get('kota') == null && $request->get('layanan') == null) {
            return redirect(url('/perawat'));
        } elseif ($request->get('kota') != null && $request->get('layanan') == null) {
            $perawats = Perawat::with('fee')->where('alamat', 'like', '%' . $request->get('kota') . '%')->where('user_id', 1)->paginate(15);

            foreach ($perawats as $key) {
                switch ($key->mulai_aktif) {
                    case '1':
                        $key->mulai_aktif = 'Belum ada Pengalaman';
                        break;
                    case '2':
                        $key->mulai_aktif = 'Pengalaman kurang dari 5 Tahun';
                        break;
                    case '3':
                        $key->mulai_aktif = 'Pengalaman 5 - 10 Tahun';
                        break;
                    default:
                        $key->mulai_aktif = 'Pengalaman lebih dari 10 Tahun';
                        break;
                }
            }
            return view('user.perawat', compact('perawats'));
        } elseif ($request->get('kota') == null && $request->get('layanan') != null) {
            $layanan = $request->get('layanan');
            //return $layanan;
            $perawats = Perawat::with('fee')->where('user_id', 1)->whereHas('fee', function ($query) use ($layanan) {
                $query->where('layanan', $layanan);
            })->paginate(15);
            //$perawats = Perawat::with('fee')->where('user_id', 1)->paginate(15);
            $n = 1;
            //return $perawats;
            if (!empty($perawats)) {
                foreach ($perawats as $key) {
                    if ($key->fee[$n]['biaya'] == 0) {
                        unset($key->fee[$n]);
                    }
                    $n++;

                    switch ($key->pengalaman) {
                        case '1':
                            $key->pengalaman = 'Belum ada Pengalaman';
                            break;
                        case '2':
                            $key->pengalaman = 'Pengalaman kurang dari 5 Tahun';
                            break;
                        case '3':
                            $key->pengalaman = 'Pengalaman 5 - 10 Tahun';
                            break;
                        default:
                            $key->pengalaman = 'Pengalaman lebih dari 10 Tahun';
                            break;
                    }
                    // switch ($key->mulai_aktif) {
                    //     case '1':
                    //         $key->mulai_aktif = 'Belum ada Pengalaman';
                    //         break;
                    //     case '2':
                    //         $key->mulai_aktif = 'Pengalaman kurang dari 5 Tahun';
                    //         break;
                    //     case '3':
                    //         $key->mulai_aktif = 'Pengalaman 5 - 10 Tahun';
                    //         break;
                    //     default:
                    //         $key->mulai_aktif = 'Pengalaman lebih dari 10 Tahun';
                    //         break;
                    // }
                }
                return view('user.perawat', compact('perawats'));
            } else {
                $layanan = $request->get('layanan');
                //return $layanan;
                $perawats = Perawat::with('fee')->where('user_id', 1)->where('alamat', 'like', '%' . $request->get('kota') . '%')->whereHas('fee', function ($query) use ($layanan) {
                    $query->where('layanan', $layanan);
                })->paginate(15);
                //$perawats = Perawat::with('fee')->where('alamat', 'like', '%'.$request->get('kota').'%')->where('user_id', 1)->paginate(15);
                $n = 1;
                //return $perawats;
                if (!empty($perawats)) {
                    foreach ($perawats as $key) {
                        if ($key->fee[$n]['biaya'] == 0) {
                            unset($key->fee[$n]);
                        }
                        $n++;

                        switch ($key->pengalaman) {
                            case '1':
                                $key->pengalaman = 'Belum ada Pengalaman';
                                break;
                            case '2':
                                $key->pengalaman = 'Pengalaman kurang dari 5 Tahun';
                                break;
                            case '3':
                                $key->pengalaman = 'Pengalaman 5 - 10 Tahun';
                                break;
                            default:
                                $key->pengalaman = 'Pengalaman lebih dari 10 Tahun';
                                break;
                        }
                        // switch ($key->mulai_aktif) {
                        //     case '1':
                        //         $key->mulai_aktif = 'Belum ada Pengalaman';
                        //         break;
                        //     case '2':
                        //         $key->mulai_aktif = 'Pengalaman kurang dari 5 Tahun';
                        //         break;
                        //     case '3':
                        //         $key->mulai_aktif = 'Pengalaman 5 - 10 Tahun';
                        //         break;
                        //     default:
                        //         $key->mulai_aktif = 'Pengalaman lebih dari 10 Tahun';
                        //         break;
                        // }
                    }

                    return view('user.perawat', compact('perawats'));
                }
                //return $request;
            }
        }
    }
}
