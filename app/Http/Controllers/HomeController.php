<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Blog;
use App\Subscriber;
use Mail;
use Socialite;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listBlog = Blog::limit(3)->orderBy('created_at', 'desc')->get();

        return view('user.home')->with('blog', $listBlog);
    }

    public function subscribe(Request $request) 
    {
        $this->validate($request, [
        'email' => 'required',
        ]);

        Subscriber::create($request->all());

        Mail::send('mails.subscribe',
            array(
                'email' => $request->get('email'),
            ), function($message)
            {
                $message->from('info@perawatku.id');
                $message->to('info@perawatku.id', 'Admin')->subject('Subscribe Nomor HP');
            });
  
        /*return Redirect::to('/')->with('message', 'Terima kasih sudah men-subscribe Perawatku!');*/
        return back()->with('success', 'Terima kasih sudah men-subscribe Perawatku!');
    }

    public function subscribeFooter(Request $request) 
    {
        $this->validate($request, [
        'email' => 'required|email',
        ]);

        Subscriber::create($request->all());

        Mail::send('mails.subscribe',
            array(
                'email' => $request->get('email'),
            ), function($message)
            {
                $message->from('info@perawatku.id');
                $message->to('info@perawatku.id', 'Admin')->subject('Subscribe Email');
            });
  
        /*return Redirect::to('/')->with('message', 'Terima kasih sudah men-subscribe Perawatku!');*/
        return back()->with('success', 'Terima kasih sudah men-subscribe Perawatku!');
    }

    public function contact(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'email' => 'required',
            'hp' => 'required',
            'subjek' => 'required',
            'pesan' => 'required',
        ]);

        Mail::send('mails.contact',
            array(
                'nama' => $request->get('nama'),
                'email' => $request->get('email'),
                'hp' => $request->get('hp'),
                'subjek' => $request->get('subjek'),
                'pesan' => $request->get('pesan'),
            ),
            function($message)
            {
                $message->from('info@perawatku.id');
                $message->to('info@perawatku.id', 'Admin')->subject('Ada yang menghubungi Perawatku via Contact Form');
            });

        return back()->with('success', 'Terima kasih sudah mengirim pesan kepada Perawatku!');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);
    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();


    }
}
