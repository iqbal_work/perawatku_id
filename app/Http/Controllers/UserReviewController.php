<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserReview;
use App\Booking;
use App\Perawat;
use App\RumahSakit;
use App\UserDetail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UserReviewController extends Controller
{
    //
    public function index()
    {	
        if (empty(UserDetail::where('user_id', Auth::id())->first()->nomor_ktp)) {
            return redirect(url('profil/pengaturan'));
        }

    	$review = UserReview::with('booking')->where('owner_user_id', Auth::id())->paginate(10);
    	foreach ($review as $reviews) {
    		$reviews['perawat'] = Perawat::where('id', $reviews->booking->perawat_id)->first();
    		$reviews['rumahsakit'] = RumahSakit::where('id', $reviews->booking->rumah_sakit_id)->first();
    	}
    	return view('user.profil-ulasan', compact('review'));
    }

    public function indexadmin()
    {   
        $review = UserReview::with('booking')->paginate(10);
        return view('admin.ulasan-perawat', compact('review'));
    }

    public function show()
    {
        $review = UserReview::where('owner_user_id', Auth::id())->paginate(5);
        
        foreach ($review as $reviews) {
            $reviews['nama_user'] = UserDetail::where('id', Booking::where('id', $reviews->booking_id)->first()->user_detail_id)->first()->nama;
            $reviews['nama_perawat'] = Perawat::where('id', Booking::where('id', $reviews->booking_id)->first()->perawat_id)->first()->nama;
            $reviews['id_perawat'] = Perawat::where('id', Booking::where('id', $reviews->booking_id)->first()->perawat_id)->first()->id;
            $reviews['foto_perawat'] = Perawat::where('id', Booking::where('id', $reviews->booking_id)->first()->perawat_id)->first()->foto;
            $reviews['tanggal_booking'] = Booking::where('id', $reviews->booking_id)->first()->tanggal_order->toDateString();
        }
        //return $review;
        return view('admin.ulasan-perawat', compact('review'));
    }
}
