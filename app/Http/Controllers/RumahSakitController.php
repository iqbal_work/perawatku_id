<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use App\RumahSakit;
use App\Perawat;
use App\Rekening;
use App\Fee;
use App\ActivityLog;

class RumahSakitController extends Controller
{
    //
    public function display()
    {
        $rumahsakit = RumahSakit::paginate(10);
        //return $rumahsakit;
        foreach ($rumahsakit as $key) {
            $key['perawat'] = Perawat::where('user_id', $key->user_id)->get();
        }
        $perawat = Perawat::all();
        //return $perawat;
        //return $rumahsakit;

        return view('user.rs', compact('rumahsakit', 'perawat'));
    }

    public function show($rumahsakit)
    {
        $name = str_replace('_', ' ', $rumahsakit);

        $rumahsakit = RumahSakit::where('nama', $name)->first();
        //return $rumahsakit;

        $rumahsakit['perawat'] = Perawat::where('user_id', $rumahsakit->user_id)->get();

        foreach ($rumahsakit->perawat as $perawat) {
            switch ($perawat->pengalaman) {
                case '1':
                    $perawat->pengalaman = 'Belum ada Pengalaman';
                    break;
                case '2':
                    $perawat->pengalaman = 'Pengalaman kurang dari 5 Tahun';
                    break;
                case '3':
                    $perawat->pengalaman = 'Pengalaman 5 - 10 Tahun';
                    break;
                default:
                    $perawat->pengalaman = 'Pengalaman lebih dari 10 Tahun';
                    break;
            }

            $perawat['fee'] = Fee::where('perawat_id', $perawat->id)->get();
        }

        //return $rumahsakit;        

        return view('user.rs-detail', compact('rumahsakit'));
    }

    public function index()
    {
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - start - 
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }
        else {
            if (Auth::user()->role != 'owner') { 
                return redirect(url('/404'));
            }
        }
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - end -


    	//ambil id user
    	$user_id = Auth::id();
        $rsid = RumahSakit::select('id')->where('user_id', $user_id)->first();

    	//search rumah sakit where userid = id user
    	$rumahsakits = RumahSakit::where('user_id', $user_id)->first();
        $rekenings = Rekening::where('owner_user_id', Auth::id())->get();
    	//return ($rumahsakits);
        //return $rekenings;

    	//redirect to profil rs page
    	return view('admin.profil-rs', compact('rumahsakits', 'rekenings'));
    }
    public function update(Request $request)
    {
        //return $request;
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - start - 
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }
        else {
            if (Auth::user()->role != 'owner') { 
                return redirect(url('/404'));
            }
        }
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - end -
        
        //ambil id user
        $user_id = Auth::id();

        //search rumah sakit where userid = id user
        $rsid = RumahSakit::select('id')->where('user_id', $user_id)->first();

        //return $rsid;
        $rs = RumahSakit::find($rsid)->first();
        //return $rs;

        //assign values


        //$rs->update($request->all());
        //return ($request);

        $rs['nama'] = $request->get('nama');
        $rs['alamat'] = $request->get('alamat');
        $rs['telepon'] = $request->get('telepon');
        $rs['email'] = $request->get('email');
        $rs['penanggungjawab'] = $request->get('penanggungjawab');
        $rs['telepon_penanggungjawab'] = $request->get('telepon_penanggungjawab');
        $rs['deskripsi'] = $request->get('deskripsi');

        //cek kalau image kosong
        if ($request->has('image')) {
            $rs['foto'] = $request->file('image')->store('rumah-sakit');
        }
        //return $rs;
        $rs->update();

        ActivityLog::create([
            'user_id' => Auth::id(),
            'activity' => 'Update Profil Rumah Sakit'
        ]);

        if (!empty($request->get('bank'))) {
            if (!empty($request->get('rekening'))) {
                $n = 0;
                foreach ($request->get('bank') as $key) {
                    if ($key != null) {
                        $bank[$n] = $key;
                        $n++;
                    }
                }
                //return $bank;

                $n = 0;
                foreach ($request->get('rekening') as $key) {
                    if ($key != null) {
                        $rekening[$n] = $key;
                        $n++;
                    }
                }

                 $max = count($bank);

                for ($i=0; $i < $max; $i++) { 
                    if ($rekening[$i] != null || $bank[$i] != null) {
                        Rekening::updateOrCreate(
                        [
                            'owner_user_id' => Auth::id(),
                            'nama_bank' => $bank[$i],
                            'nomor_rekening' => $rekening[$i],
                        ],
                        [
                            'owner_user_id' => Auth::id(),
                            'nama_bank' => $bank[$i],
                            'nomor_rekening' => $rekening[$i]
                        ]);
                    }
                }
            }
        }

        return redirect('./admin/rs/profil-rs');
    }

    public function create()
    {
        $this->validate(request(), [
            'user_id' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'telepon' => 'required',
            'email' => 'required',
            'penanggungjawab' => 'required',
            'telepon_penanggungjawab' => 'required',
            'deskripsi' => 'required',
            'foto' => 'required'
        ]);

        return $request->get('bank');

        //ambil id rs
        $id = Auth::id();
        //return $id;

        //input ke database
        RumahSakit::create([
            'user_id' => $id,
            'nama' => $request->get('nama'),
            'alamat' => $request->get('alamat'),
            'telepon' => $request->get('telepon'),
            'email' => $request->get('email'),
            'penanggungjawab' => $request->get('penanggungjawab'),
            'telepon_penanggungjawab' => $request->get('telepon_penanggungjawab'),
            'foto' => $request->file('foto')->store('perawat'),
            'deskripsi' => $request->get('deskripsi')
        ]);

        $rsid = RumahSakit::where('user_id', Auth::id())->first()->id;

        ActivityLog::create([
            'user_id' => Auth::id(),
            'activity' => 'Update Profil Rumah Sakit'
        ]);



        if (!empty($request->get('bank'))) {
            if (!empty($request->get('rekening'))) {
                $max = $request->get('bank');

                $n = 0;
                foreach ($request->get('bank') as $key) {
                    $bank[$n] = $key;
                    $n++;
                }

                $n = 0;
                foreach ($request->get('rekening') as $key) {
                    $rekening[$n] = $key;
                    $n++;
                }

                for ($i=0; $i < $max; $i++) { 
                    Rekening::updateOrCreate(
                    [
                        'owner_user_id' => Auth::id(),
                        'nama_bank' => $bank[$i],
                        'nomor_rekening' => $rekening[$i],
                    ],
                    [
                        'owner_user_id' => Auth::id(),
                        'nama_bank' => $bank[$i],
                        'nomor_rekening' => $rekening[$i]
                    ]);
                }
            }
        }

        return redirect(url('/admin/dashboard'));
    }

    public function membership()
    {
        $rs = RumahSakit::where('user_id', Auth::id())->first();
        $rs->membership_status = 1;

        $rs->update();

        return redirect(url('/admin/rs/upgrade-akun'));
    }

    public function premiumConfirmation()
    {
        $rumahsakit = RumahSakit::paginate(10);
        $status = 0;
        //return $rumahsakit;

        return view('superadmin.rumah-sakit', compact('rumahsakit', 'status'));
    }

    public function tolakPremium(RumahSakit $rumahsakit)
    {
        $rumahsakit['membership_status'] = 0;
        $rumahsakit->update();

        ActivityLog::create([
            'user_id' => $rumahsakit->user_id,
            'activity' => 'Pengajuan Premium Account ditolak'
        ]);

        return redirect(url('/superadmin/rumah-sakit'));
    }

    public function terimaPremium(RumahSakit $rumahsakit)
    {
        $rumahsakit['membership_status'] = 2;
        $rumahsakit->update();

        ActivityLog::create([
            'user_id' => $rumahsakit->user_id,
            'activity' => 'Pengajuan Premium Account diterima'
        ]);

        return redirect(url('/superadmin/rumah-sakit'));

    }
}
