<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Blog;

class BlogsController extends Controller
{
	public function read()
	{
		$blog = Blog::orderBy('created_at', 'desc')->paginate(9);
        return view('user.blog', compact('blog'));
	}

    public function show($blog)
    {
        $judul = str_replace("-", " ", $blog);
        //return $judul;
        $blogs = Blog::where('judul', $judul)->first();
        $listBlog = Blog::limit(5)->orderBy('created_at', 'desc')->get();

        return view('user.blog-detail', compact('blogs', 'listBlog'));
    }

    public function index()
    {
    	//terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - start - 
        if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }
        else {
            if (Auth::user()->role != 'admin') { 
                return redirect(url('/404'));
            }
        }
        //terribly sorry for this bad middleware implementation, back-end guy too dumb to know how to implement middleware here - end -

        $blog = Blog::orderBy('created_at', 'desc')->paginate(10);
        //return $blog;
        return view('superadmin.blog', compact('blog'));
    }

    public function edit(Blog $blog)
    {
    	if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }
        else {
            if (Auth::user()->role != 'admin') { 
                return redirect(url('/404'));
            }
        }
        //return $blog;
        return view('superadmin.blog-edit', compact('blog'));
    }

    public function update(Request $request)
    {
    	$blogs = Blog::find($request->id);
    	$blogs->user_id = Auth::id();
        $blogs->judul = $request->get('judul');
        $blogs->meta = $request->get('meta');
        $blogs->konten = $request->get('konten');
        if ($request->has('gambar')) {
            $blogs->gambar = $request->file('gambar')->store('blogs');
        }

        //return asset('/storage/'.$blogs->gambar);

        //return $blogs;
        $blogs->update();
        return redirect(url('/superadmin/blog'));
    }

    public function store(Request $request)
    {
    	$this->validate(request(),[
    		'judul' => 'required',
    		'meta' => 'required',
    		'konten' => 'required'
    	]);

    	$blogs = new Blog;

        $blogs->judul = rtrim($request->get('judul'), "? ");
        $blogs->meta = $request->get('meta');
        $blogs->konten = $request->get('konten');
        $blogs->user_id = Auth::id();
        if ($request->has('gambar')) {
            $blogs->gambar = $request->file('gambar')->store('blogs');
        }else{
        	$blogs->gambar = 'images/icons/placeholder.jpg';
        }
        $blogs->save();

        //return $blog;

    	return redirect(url('/superadmin/blog'));
    }

    public function add()
    {
    	if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }
        else {
            if (Auth::user()->role != 'admin') { 
                return redirect(url('/404'));
            }
        }

    	return view('superadmin.blog-add');
    }

    public function destroy(Blog $blog)
    {
    	if (Auth::user() === NULL) {
            return redirect(url('/404'));
        }
        else {
            if (Auth::user()->role != 'admin') { 
                return redirect(url('/404'));
            }
        }

        $blogs = Blog::find($blog->id);
        $blogs = Blog::where('id', $blog)->first();
        $blogs->delete();

        return redirect(url('/superadmin/blog'));
    }
}
