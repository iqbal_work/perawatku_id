<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layanan extends Model
{
    protected $fillable = [
        'kategori', 'jasa', 'tindakan', 'biaya'
    ];
}
