<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    //
    protected $fillable = [
        'user_id', 'judul', 'gambar', 'meta', 'konten'
    ];

    protected $dates = ['created_at'];
}
