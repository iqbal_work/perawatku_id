<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
    //
    protected $fillable = [
        'owner_user_id','nama_bank', 'nomor_rekening', 'nama'
    ];
}
