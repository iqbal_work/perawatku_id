<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RumahSakit;

class ActivityLog extends Model
{
    //
    protected $fillable = [
        'user_id', 'activity'
    ];

     public function rumahSakit()
    {
    	return $this->belongsTo(RumahSakit::class);
    }
}
