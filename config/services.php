<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id' => env('512878425243-hkhec0e4820t3r3ial0u4cme1jk5d3g9.apps.googleusercontent.com'),
        'client_secret' => env('TK1g9epAEv2foCymVIN6f2v8'),
        'redirect' => 'https://perawatku.id/signin-google',
    ],

    'facebook' => [
        'client_id' => env('249891952526757'),
        'client_secret' => env('c83fd41089c02372a8e3623887e50b0e'),
        'redirect' => 'https://perawatku.id/signin-facebook',
    ],

];
